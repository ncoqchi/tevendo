@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="text-center mb-5">
                    <h2>Iniciar Sesión</h2>
                </div>
                <div class="tab-pane fade show active" id="result2" role="tabpanel">
                    <div class="example-modal">
                        <div class="card box-shadow">
                            <div class="card-body tab-content py-4">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email" class="font-weight-medium">{{ __('Correo Electrónico') }}</label>
                                        <div class="input-group-overlay form-group">
                                            <div class="input-group-prepend-overlay">
                                                <span class="input-group-text"><i class="czi-mail text-muted"></i></span></div>
                                            <input class="form-control prepended-form-control @error('email') is-invalid @enderror"
                                                   name="email" value="{{ old('email') }}" type="email"  placeholder="tucorreo@ejemplo.com"
                                                   autocomplete="email" required autofocus>
                                        </div>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="password" class="font-weight-medium">{{ __('Contraseña') }}</label>
                                        <div class="password-toggle">
                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password" required autocomplete="current-password">
                                            <label class="password-toggle-btn">
                                                <input id="idViewPassword" class="custom-control-input" onclick="cambiarEstado()" type="checkbox">
                                                <i class="czi-eye password-toggle-indicator"></i>
                                                <span class="sr-only">Ver Contraseña</span>
                                            </label>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group d-flex flex-wrap justify-content-between">
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input class="custom-control-input" type="checkbox" name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="remember">
                                                {{ __('Recordar') }}
                                            </label>

                                        </div>
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link text-dark text-muted" href="{{ route('password.request') }}">
                                                {{ __('¿Olvidaste tu contraseña?') }}
                                            </a>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-block btn-shadow font-weight-medium text-light mb-3"
                                    style="background-image: linear-gradient(to right, #ff0000, #ff0323, #ff1239, #ff214b, #ff2f5c, #fd356a, #fa3d77, #f64584, #f14a91, #ea519d, #e157a9, #d85eb3);">
                                        <i class="czi-sign-in mr-2 ml-n21"></i>
                                        {{ __('Iniciar Sesión') }}
                                    </button>
                                </form>
                            </div>
{{--                            <div class="card-footer bg-secondary">--}}
{{--                                Se--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripting')
    <script type="text/javascript" src="{{asset('js/login.js')}}"></script>
@endsection
