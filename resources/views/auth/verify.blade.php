@extends('layouts.app')

@section('content')
{{--<div style="position: relative">--}}
{{--    <video autoplay muted loop style="width: 100%">--}}
{{--        <source src="{{asset('video/stars.mp4')}}" type="video/mp4">--}}
{{--    </video>--}}
{{--    <label style="position: absolute; top: 0; left: 10px; color: white">Antes de continuar, consulte su correo electrónico para ver si existe un enlace de verificación.</label>--}}
{{--    <div class="my-3" style="position: absolute; top: 50%; left: 50%; color: white">--}}
{{--        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">--}}
{{--            @csrf--}}
{{--            <button type="submit" class="btn btn-link p-0 m-0 align-baseline font-weight-medium font-size-lg">{{ __('Haga clic aquí para solicitar otro') }}</button>.--}}
{{--        </form>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="text-center mb-5">
                <h3>Debe Verificar su dirección de Correo Electrónico</h3>
            </div>
            <div class="card box-shadow">
{{--                <div class="card-header bg-primary text-light">{{ __('Debe Verificar Su dirección de Correo Electrónico') }}</div>--}}

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.') }}
                        </div>
                    @endif
                    {{ __('Antes de continuar, consulte su correo electrónico para ver si existe un enlace de verificación.') }}
                    {{ __('Si no recibió el correo electrónico') }},
                    <div class="my-3">
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline font-weight-medium font-size-lg">{{ __('Haga clic aquí para solicitar otro') }}</button>.
                        </form>
                    </div>

                    <div class="p-5">
                        <img src="{{asset('img/gmail_and_outlook.jpg')}}" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
