@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="text-center mb-4">
                <h5 class="">¿Olvidaste tu contraseña?</h5>
            </div>
            <div class="card box-shadow">
{{--                <div class="card-header bg-primary text-light">--}}
{{--                    {{ __('Restablecer la contraseña') }}--}}
{{--                </div>--}}
                <div class="card-body mb-4" style="border-top: #3A83B9 solid 4px">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="border-bottom px-1 pt-1 pb-3 mb-3 d-flex justify-content-between">
                        <div style="width: 80px">
                            <img src="{{asset('img/contrasenas/candado.png')}}" alt="">
                        </div>
                        <div class="text-justify text-muted">
                            Ingrese su dirección de correo electrónico y le enviaremos un enlace para restablecer su contraseña
                        </div>
                    </div>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="d-block font-weight-medium">
                                {{ __('Correo Electrónico') }}
                            </label>
                            <div class="">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}" placeholder="micorreo@ejemplo.com"
                                       required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <div class="">
                                <button type="submit" class="btn w-100 text-light" style="background-color: #47BB7F;">
                                    {{ __('Reestablecer Contraseña') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer d-flex justify-content-between align-items-center p-4" style="border-top: 0; background-color: #FAFAF9">
                    <div>¿Aún no tienes una cuenta?</div>
                    <div><button class="btn btn-light">Crear una Cuenta</button></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
