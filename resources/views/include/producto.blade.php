    <div class="row pt-2">
        @if(sizeof($productos)==0)
            <p class="text-primary font-weight-bold pl-3">No se encontraron productos</p>
        @endif
        @foreach($productos as $producto)
            <div class="col-lg-2 col-sm-3 col-6 px-2 mb-1 mb-sm-4">
                <div class="card product-card p-2">
                    @if($producto->precio > $producto->precioRebaja)
                        <span class="badge badge-shadow" style="background-color:red;color: white">
                            -{{number_format(((($producto->precio - $producto->precioRebaja) / $producto->precio)* 100),0, '.', ',')}}%</span>
                    @endif
                    <a class="card-img-top d-block text-center" href="{{ route('producto.detalle', $producto->id) }}">
                        <img class="rounded" loading="lazy" src="{{ $producto->imagen1  }}"  alt="Producto" style="height: 200px">
                    </a>
                    <div class="card-body p-0 px-sm-2 pt-sm-1">
                        <a class="d-block font-size-sm cursormano text-dark font-weight-medium"
                           href="{{ route('productos.categoria', $producto->categoria_id) }}">{{$producto->categoria->nombre}}
                        </a>
                        <div class="font-size-sm">
                            @if( strlen($producto->nombre) >= 19 )
                                <a href="{{ route('producto.detalle', $producto->id) }}" class="cursormano text-dark">{{ucwords(strtolower(substr($producto->nombre, 0, 16)))}}..</a>
                            @else
                                <a href="{{ route('producto.detalle', $producto->id) }}" class="cursormano text-dark">{{ucwords(strtolower($producto->nombre))}}</a>
                            @endif
                        </div>
                        <div class="mt-2">
                            @if($producto->precio > $producto->precioRebaja)
                            <div class="d-flex justify-content-between">
                                <div class="font-weight-medium">
                                    Online
                                </div>
                                <div class="">
                                    <span class="text-primary font-weight-medium">S/&nbsp;{{number_format($producto->precioRebaja,2, '.', ',')}}</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="font-weight-normal">
                                    Antes
                                </div>
                                <div class="">
                                    <span class="text-muted"><del>S/&nbsp;{{number_format($producto->precio,2, '.', ',')}}</del></span>
                                </div>
                            </div>
                            @else
                            <div class="d-flex justify-content-between" >
                                <div class="font-weight-medium">
                                    Online
                                </div>
                                <div class="">
                                    <span class="text-primary font-weight-medium">S/&nbsp;{{number_format($producto->precio,2, '.', ',')}}</span>
                                </div>
                            </div>
                                <div>
                                    &nbsp;
                                </div>
                            @endif

                            <div class="d-flex justify-content-center mb-1">
                                <div class="star-rating">
                                    @for($i = 1; $i <= 5; $i++)
                                        @if($i > $producto->rating)
                                            <i class="font-size-md sr-star czi-star-filled" style="color: #aeb4be73"></i>
                                        @else
                                            <i class="font-size-md sr-star czi-star-filled active"></i>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 px-sm-1 py-1">
                        <button class="btn btn-outline-primary btn-sm btn-block mb-2 py-1 px-0 p-sm-2"
                                type="button" onclick="agregarCarrito({{$producto}})">
                            <i class="czi-cart font-size-sm font-weight-bold mr-1"></i>&nbsp;&nbsp;Agregar
                        </button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
