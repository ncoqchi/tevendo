@extends('layouts.home')
@section('head')

@endsection

@section('contenido')

<section class="container pt-4 mb-4 mb-sm-5">

    <div style="width: 100%;" class="px-2 py-1">
        <h2 style="text-shadow: 0px 0px 10px rgba(0,0,0,0.3);font-family: 'Roboto', Sans-serif; font-weight: 600;">
            <span style="justify-content: center; order: initial; display: flex; box-sizing: border-box;margin: 0;border: 0;vertical-align: baseline; font-size: 100%;">
                <span style="width: 200px;height: 5px;background-color: red;box-sizing: border-box;"></span>
            </span>
            <div class="text-center text-dark mt-2">
                <span><em>{{$categoria->nombre}}</em></span>
            </div>
        </h2>
    </div>

    @if (pathinfo($categoria->imagen, PATHINFO_EXTENSION) == 'mp4')

     <div class="d-flex justify-content-center mb-3">
         <video autoplay muted loop height="450">
             <source src="{{asset($categoria->imagen)}}" type="video/mp4">
         </video>
     </div>

    @else
        <div class="text-center mb-3 mb-sm-4">
            <img loading="lazy" class="rounded" style="box-shadow:  10px 10px 26px 0px rgba(222,222,222,1); width: 600px" src="{{asset($categoria->imagen)}}" alt="Imagen de Categoria">
        </div>
    @endif



    @include('include.producto')
    <div class="pt-3" style="margin:0 auto; width: 180px">
        {{ $productos->links() }}
    </div>
</section>

@endsection
