@extends('layouts.home')

@section('contenido')

{{--    Para PC--}}
    <section class="mb-4 d-none d-sm-block">
        <div class="fade show active" id="result5">
            <div class="cz-carousel cz-dots-enabled">
                <div class="cz-carousel-inner" data-carousel-options="{
                    &quot;mode&quot;: &quot;gallery&quot;,
                    &quot;autoplay&quot;: true,
                    &quot;autoplayTimeout&quot;: 2000,
                    &quot;speed&quot;: 3000
                  }">
                    @foreach($sliders as $slide)
                    <img class="cursormano"  src="{{asset($slide->imagen)}}" style="height: 650px"
                         alt="Image" onclick="javascript:window.location='{{$slide->enlace}}'"/>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


{{--    Para celulares--}}
    <section class="mb-4 d-block d-sm-none">
        <div class="fade show active" id="result5">
            <div class="cz-carousel cz-dots-enabled">
                <div class="cz-carousel-inner" data-carousel-options="{
                    &quot;mode&quot;: &quot;gallery&quot;,
                    &quot;autoplay&quot;: true,
                    &quot;autoplayTimeout&quot;: 2000,
                    &quot;speed&quot;: 3000}">
                    @foreach($sliders as $slide)
                        <img class="cursormano"  src="{{asset($slide->imagen2)}}" alt="Image" style="height: 600px"
                             onclick="javascript:window.location='{{$slide->enlace}}'"/>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- Category (Women)-->
    <section class="container pt-4 mb-4">
        <div class="mb-3">
            <h1 style="text-shadow: 0px 0px 10px rgba(0,0,0,0.3);font-family: 'Roboto', Sans-serif; font-weight: bold; font-size: 40px">
                <span style="justify-content: center; order: initial; display: flex; box-sizing: border-box;margin: 0;border: 0;vertical-align: baseline; font-size: 100%;">
                    <span style="width: 120px;height: 5px;background-color: red;box-sizing: border-box;"></span>
                </span>
                <div class="text-center text-dark mt-1">
                    <span><em>¡Categorías!</em></span>
                </div>
            </h1>
        </div>
    </section>

    <section class="container pt-4 mb-4 mb-sm-5">
            @foreach($categorias as $categoria)
              @if( sizeof($categoria->productos) > 0 )
{{--                  @if( $loop->index % 2 == 0)--}}
{{--                      POSICIONES PAR--}}
                      <section class="pt-lg-3 mb-4" style="max-width: 1256px;position: relative;margin: 0px auto;">
                          <div class="row" >
                              <!-- TITULO NOMBRE E IMAGEN CATEGORIA-->
                              <div class="col-md-6">
                                  <div class="d-flex flex-column h-100 overflow-hidden rounded" style="background-color: white;">
                                      <div class="" style="position: relative">
                                          @if (pathinfo($categoria->imagen, PATHINFO_EXTENSION) == 'mp4')

                                              <video autoplay muted loop height="450">
                                                  <source src="{{asset($categoria->imagen)}}" type="video/mp4">
                                              </video>

                                          @else
                                              <img loading="lazy" class="d-block w-100" style="height: 450px" src="{{asset($categoria->imagen)}}" alt="">
                                          @endif
                                          <div class="d-flex justify-content-between p-3 w-100" style="background-color:  rgb(0 0 0 / 41%); position: absolute;top:10px;left: 10px">
                                              <div>
                                                  <span class="" style="color: white; font-size: 36px">{{$categoria->nombre}}</span>
                                                  <span class="d-block">
                                                      <a class="btn-link font-size-lg" style="color: white"
                                                         href="{{route('productos.categoria',$categoria->id)}}">Ver Categoría<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>
                                                      </a>
                                                  </span>
                                              </div>
                                              <div class="cz-custom-controls order-md-1" id="for-women-{{$categoria->id}}">
                                                  <button type="button"><i class="czi-arrow-left"></i></button>
                                                  <button type="button"><i class="czi-arrow-right"></i></button>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="text-center mt-4 d-none d-sm-block">
                                          <a class="btn btn-primary font-size-md" href="{{route('productos.categoria',$categoria->id)}}">
                                              Ver Categoría<i class="czi-arrow-right ml-2"></i>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              <!-- PRODUCTOS CARROUSEL-->
                              <div class="col-md-6 pt-4 pt-md-0">
                                  <div class="cz-carousel">
                                      <div class="cz-carousel-inner" data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#for-women-{{$categoria->id}}&quot;}">
                                          <!-- PRODUCTO GRUPOS DE 3-->
                                          {{$cantidadCarousel = sizeof($categoria->productos)}}
                                          @for ($i = 0; $i < ($cantidadCarousel / 2); $i++)
                                              <div>
                                                  <div class="row mx-n2">
                                                      <!-- PRODUCTO 3-->
                                                      @foreach($categoria->productos->slice($i*2, 2) as $pro)
                                                          <div class="col-6 col-sm-6 px-0 px-2 mb-sm-4">
                                                              <div class="card product-card p-3">
                                                                  @if($pro->precio > $pro->precioRebaja)
                                                                      <span class="badge badge-shadow py-1"
                                                                            style="background-color:red;color: white">
                                                                            - {{round(((($pro->precio - $pro->precioRebaja) / $pro->precio)* 100),0)}}%
                                                                        </span>
                                                                  @endif
                                                                  <a class="card-img-top d-block overflow-hidden text-center" href="{{route('producto.detalle', $pro->id)}}">
                                                                      <img class="rounded-sm" loading="lazy"  src="{{asset($pro->imagen1)}}" alt="Product">
                                                                  </a>
                                                                  <div class="card-body py-2 px-2">
                                                                      <a style="" class="d-block pb-1 nc-title-cate" href="{{ route('productos.categoria', $categoria->id) }}">
                                                                          {{ $categoria->nombre }}
                                                                      </a>
                                                                      <div class="pb-0" >
{{--                                                                          @if( strlen($pro->nombre) >= 25 )--}}
{{--                                                                              <a href="{{ route('producto.detalle', $pro->id) }}" class="cursormano text-dark">{{ucwords(strtolower(substr($pro->nombre, 0, 23)))}}..</a>--}}
{{--                                                                          @else--}}
                                                                              <a href="{{ route('producto.detalle', $pro->id) }}" class="cursormano text-dark">{{ucwords(strtolower($pro->nombre))}}</a>
{{--                                                                          @endif--}}
                                                                      </div>
                                                                      <div class="d-flex flex-column mt-2">
                                                                          <div class="product-price d-flex flex-nowrap justify-content-between">
                                                                              <div class="nc-text-bold">Online</div>
                                                                              <div>
                                                                                  <span class="text-primary font-weight-medium">
                                                                                      S/ {{number_format($pro->precioRebaja, 2, '.', ',')}}
                                                                                  </span>
                                                                              </div>
                                                                          </div>

                                                                          @if($pro->precioRebaja < $pro->precio)
                                                                          <div class="product-price d-flex flex-nowrap justify-content-between">
                                                                              <div class="nc-text-normal">Antes</div>
                                                                              <div>
                                                                                  <span class="font-size-sm">
                                                                                  <del>S/ {{number_format($pro->precio, 2, '.', ',')}}</del>
                                                                              </span>
                                                                              </div>
                                                                          </div>
                                                                          @else
                                                                              <div>&nbsp;</div>
                                                                          @endif

                                                                          <div class="star-rating mt-1 d-flex justify-content-center">
                                                                              <div>
                                                                                  @for ($j = 1; $j <= 5; $j++)
                                                                                      @if($j <= $pro->rating)
                                                                                          <i class="font-size-md sr-star czi-star-filled active"></i>
                                                                                      @else
                                                                                          <i class="font-size-md sr-star czi-star"></i>
                                                                                      @endif
                                                                                  @endfor
                                                                              </div>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      @endforeach
                                                  </div>
                                              </div>
                                          @endfor
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="py-4 text-center d-block d-sm-none">
                              <a class="btn btn-primary font-size-md" href="{{route('productos.categoria',$categoria->id)}}">
                                  Ver Productos<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>
                              </a>
                          </div>
                      </section>

                    @endif
                    @endforeach

    </section>



{{--              @else--}}
{{--              POSICIONES IMPAR--}}
{{--              <section class="container pt-lg-3 mb-4 mb-sm-5">--}}
{{--                  <div class="row">--}}
{{--                      <div class="col-md-4 d-block d-sm-none">--}}
{{--                          <div class="d-flex flex-column h-100 overflow-hidden rounded" style="background-color: #f6f8fb;">--}}
{{--                              <div class="d-flex justify-content-between p-3">--}}
{{--                                  <div>--}}
{{--                                      <span class="font-size-xl font-weight-bold">{{$categoria->nombre}}</span>--}}
{{--                                      <span class="d-block d-sm-none ml-2">--}}
{{--                                          <a class="btn-link font-weight-medium font-size-md" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                                 Ver Productos<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>--}}
{{--                                          </a>--}}
{{--                                      </span>--}}
{{--                                  </div>--}}
{{--                                  <div class="cz-custom-controls order-md-1" id="for-women-{{$categoria->id}}">--}}
{{--                                      <button type="button"><i class="czi-arrow-left"></i></button>--}}
{{--                                      <button type="button"><i class="czi-arrow-right"></i></button>--}}
{{--                                  </div>--}}
{{--                              </div>--}}
{{--                              <a class="d-none d-md-block" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                  <img loading="lazy" class="d-block w-100" src="{{asset($categoria->imagen)}}" style="height: 300px" alt="">--}}
{{--                              </a>--}}
{{--                              <div class="px-3 py-4 text-center d-none d-sm-block">--}}
{{--                                  <a class="btn btn-primary font-size-md" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                      Ver Productos<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>--}}
{{--                                  </a>--}}
{{--                              </div>--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                      <!-- Product grid (carousel)-->--}}
{{--                      <div class="col-md-8 pt-4 pt-md-0">--}}
{{--                          <div class="cz-carousel">--}}
{{--                              <div class="cz-carousel-inner" data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#for-women-{{$categoria->id}}&quot;}">--}}
{{--                                  <!-- Carousel item-->--}}
{{--                                  {{$cantidadCarousel = sizeof($categoria->productos)}}--}}
{{--                                  @for ($i = 0; $i < ($cantidadCarousel / 2); $i++)--}}
{{--                                      <div>--}}
{{--                                          <div class="row mx-n2">--}}
{{--                                              <!-- producto item-->--}}
{{--                                              @foreach($categoria->productos->slice($i*2, 2) as $pro)--}}
{{--                                                  <div class="col-6 px-0 px-sm-2 mb-sm-4">--}}
{{--                                                      <div class="card product-card">--}}
{{--                                                          @if($pro->precio > $pro->precioRebaja)--}}
{{--                                                              <span class="badge badge-shadow font-weight-bold py-1"--}}
{{--                                                                    style="background-color:red;color: white">--}}
{{--                                                                    - {{round(((($pro->precio - $pro->precioRebaja) / $pro->precio)* 100),0)}}%--}}
{{--                                                              </span>--}}
{{--                                                          @endif--}}
{{--                                                          <a class="card-img-top d-block overflow-hidden p-1 text-center" href="{{route('producto.detalle', $pro->id)}}">--}}
{{--                                                              <img class="rounded-sm w-100" loading="lazy" src="{{asset(explode(',', $pro->imagen)[0])}}" alt="Product">--}}
{{--                                                          </a>--}}
{{--                                                          <div class="card-body py-2">--}}
{{--                                                              <a class="product-meta d-block pb-1" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                                                  {{ $categoria->nombre }}--}}
{{--                                                              </a>--}}
{{--                                                              <h3 class="product-title font-size-lg pb-0">--}}
{{--                                                                  <a href="{{route('producto.detalle', $pro->id)}}">{{$pro->nombre}}</a>--}}
{{--                                                              </h3>--}}
{{--                                                              <div class="d-flex flex-column">--}}
{{--                                                                  <div class="product-price">--}}
{{--                                                                      <span class="text-primary font-weight-medium">S/. {{number_format($pro->precioRebaja, 2, '.', ',')}}</span>--}}
{{--                                                                  </div>--}}
{{--                                                                  <div class="star-rating">--}}
{{--                                                                      @for ($j = 1; $j <= 5; $j++)--}}
{{--                                                                          @if($j <= $pro->rating)--}}
{{--                                                                              <i class="sr-star czi-star-filled active"></i>--}}
{{--                                                                          @else--}}
{{--                                                                              <i class="sr-star czi-star"></i>--}}
{{--                                                                          @endif--}}
{{--                                                                      @endfor--}}
{{--                                                                  </div>--}}
{{--                                                              </div>--}}
{{--                                                          </div>--}}
{{--                                                      </div>--}}
{{--                                                  </div>--}}
{{--                                              @endforeach--}}
{{--                                          </div>--}}
{{--                                      </div>--}}
{{--                                  @endfor--}}
{{--                              </div>--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                      <!-- Banner with controls-->--}}
{{--                      <div class="col-md-4 d-none d-sm-block ">--}}
{{--                          <div class="d-flex flex-column h-100 overflow-hidden rounded" style="background-color: #f6f8fb;">--}}
{{--                              <div class="d-flex justify-content-between p-3">--}}
{{--                                  <div>--}}
{{--                                      <span class="font-size-xl font-weight-bold">{{$categoria->nombre}}</span>--}}
{{--                                      <span class="d-block d-sm-none ml-2">--}}
{{--                                          <a class="btn-link font-weight-medium font-size-md" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                                 Ver Productos<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>--}}
{{--                                          </a>--}}
{{--                                      </span>--}}
{{--                                  </div>--}}
{{--                                  <div class="cz-custom-controls order-md-1" id="for-women-{{$categoria->id}}">--}}
{{--                                      <button type="button"><i class="czi-arrow-left"></i></button>--}}
{{--                                      <button type="button"><i class="czi-arrow-right"></i></button>--}}
{{--                                  </div>--}}
{{--                              </div>--}}
{{--                              <a class="d-none d-md-block" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                  <img loading="lazy" class="d-block w-100" src="{{asset($categoria->imagen)}}" style="height: 300px" alt="">--}}
{{--                              </a>--}}
{{--                              <div class="px-3 py-4 text-center d-none d-sm-block">--}}
{{--                                  <a class="btn btn-primary font-size-md" href="{{route('productos.categoria',$categoria->id)}}">--}}
{{--                                      Ver Productos<i class="czi-arrow-right font-size-xs align-middle ml-1"></i>--}}
{{--                                  </a>--}}
{{--                              </div>--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                  </div>--}}
{{--              </section>--}}
{{--          @endif--}}




    <section class="container pt-4 mb-4 mb-sm-5">
        <div class="mb-3">
            <h1 style="text-shadow: 0px 0px 10px rgba(0,0,0,0.3);font-family: 'Roboto', Sans-serif; font-weight: 600;">
                <span style="justify-content: center; order: initial; display: flex; box-sizing: border-box;margin: 0;border: 0;vertical-align: baseline; font-size: 100%;">
                    <span style="width: 120px;height: 5px;background-color: red;box-sizing: border-box;"></span>
                </span>
                <div class="text-center text-dark mt-1">
                    <span><em>¡En Oferta!</em></span>
                </div>
            </h1>
        </div>
        <productos></productos>
    </section>

{{--    <div class="text-center pt-1 my-5">--}}
{{--        <a class="btn btn-accent px-4" href="{{ route('tienda') }}">Ver Toda La Tienda</a>--}}
{{--    </div>--}}
@endsection


@section('scripting')
     <script src="/js/app_290820200648am.js"></script>
@endsection

