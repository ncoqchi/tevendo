@extends('layouts.home')

@section('head')
    <link rel="stylesheet" href="{{asset('js/sweet/sweetalert2.css')}}">
@endsection

@section('contenido')
    <!-- Page Title (Shop)-->
    <compra-principal></compra-principal>
    <!-- Page Content-->
@endsection
@section('scripting')
    <script src="{{asset('js/app_290820200648am.js')}}"></script>
@endsection
