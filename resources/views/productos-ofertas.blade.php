@extends('layouts.home')
@section('contenido')
{{--    <div class="pt-1" style="background-image: radial-gradient(circle, #ff6734, #ff582c, #fe4724, #fe311e, #fd0019);">--}}
   <div class="pt-1">
        <div class="container d-lg-flex justify-content-between py-1 py-sm-2">
            <div class="order-lg-2 mb-1 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-primary flex-lg-nowrap justify-content-center justify-content-lg-start">
                        <li class="breadcrumb-item">
                            <a class="text-nowrap text-dark" href="index.php"><i class="czi-home"></i>Principal</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap text-dark">
                            <a href="#" class="text-primary font-weight-bold">Ofertas</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left pt-1" >
                <h2 class="font-weight-bold text-primary mb-0"><em>Todas Nuestras Ofertas</em></h2>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4">

            <tienda :enoferta="'on'"></tienda>

    </div>
@endsection

@section('scripting')
    <script src="{{asset('js/app_290820200648am.js')}}"></script>
@endsection
