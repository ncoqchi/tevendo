@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="d-sm-flex flex-wrap justify-content-between align-items-center pb-2">
                <h2 class="h3 py-2 mr-2 text-center text-sm-left">Actualizar Tipo de Envío</h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            <form action="{{ route('admin.actualizar.tipoenvio')}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-product-name">Nombre</label>
                    <input class="form-control" type="text" id="unp-product-name" name="nombre" value="{{$tipoenvio->nombre}}">
                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label class="font-weight-medium" for="unp-standard-price">Precio</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">S/.</span>
                            </div>
                            <input class="form-control" type="text" id="unp-standard-price" name="precio" value="{{$tipoenvio->precio}}">
                        </div>
                    </div>
                </div>

                <button class="btn btn-info btn-block" type="submit"><i class="czi-cloud-upload font-size-lg mr-2"></i>Actualizar</button>
            </form>
        </div>
    </section>

@endsection
