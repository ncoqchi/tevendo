@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-9 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="d-sm-flex flex-wrap justify-content-between align-items-center pb-2">
                <h2 class="h3 py-2 mr-2 text-center text-sm-left">Lista de Tipos de Envío</h2>
            </div>
            <div class="table-responsive mb-3">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Fecha Creación</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tiposenvios as $tipoenvio)
                        <tr>
                            <th scope="row">{{$tipoenvio->id}}</th>
                            <td>{{$tipoenvio->nombre}}</td>
                            <td>S/. {{number_format($tipoenvio->precio,2, '.', ',')}}</td>
                            @if($tipoenvio->created_at == null)
                                <td>-</td>
                            @else
                                <td>{{$tipoenvio->created_at->format('d-m-Y H:i:s')}}</td>
                            @endif
                            <td>
                                <a class="btn btn-success" style="padding: 3px" href="{{ route('admin.editar.tipoenvio', $tipoenvio->id) }}">Editar</a>
                                <form class="d-inline" method="POST" action="{{route('admin.eliminar.tipoenvio', $tipoenvio->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" style="padding: 3px" type="submit">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
