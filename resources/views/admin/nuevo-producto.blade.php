@extends('layouts.admin')
@section('seccion')
    <section class="mb-3 px-3 px-sm-10">
        <div class="mb-2">
            <!-- Title-->
            <div class="text-center pt-3 pb-1">
                <h3 class="mr-2"><strong><em>Nuevo Producto</em></strong></h3>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <form action="{{ route('admin.registrar.producto')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label class="font-weight-medium" for="unp-product-name">Nombre<span class="text-primary font-weight-bold"> *</span></label>
                    <input required class="form-control  @error('nombre') is-invalid @enderror" type="text" id="unp-product-name"
                           name="nombre" placeholder="Ingrese un Nombre" value="{{ old('nombre') }}">
                </div>

                <div class="form-group">
                    <label class="font-weight-medium" for="unp-product-description">Descripción</label>
                    <textarea class="form-control ckeditor" rows="3" id="unp-product-description" value="{{old('descripcion')}}"
                              name="descripcion" placeholder="Ingrese una Descripción"></textarea>
{{--                    <div class="bg-secondary p-3 font-size-ms rounded-bottom">--}}
{{--                        <span--}}
{{--                            class="d-inline-block font-weight-medium mr-2 my-1">Soporta los siguientes caracteres:</span>--}}
{{--                        <em class="d-inline-block border-right pr-2 mr-2 my-1">*Cursiva*</em>--}}
{{--                        <strong class="d-inline-block border-right pr-2 mr-2 my-1">**Negrita**</strong>--}}
{{--                        <span class="d-inline-block border-right pr-2 mr-2 my-1">- Item de Lista</span>--}}
{{--                        <span class="d-inline-block border-right pr-2 mr-2 my-1">##Cabecera##</span>--}}
{{--                        <span class="d-inline-block">--- Regla Horizontal</span>--}}
{{--                    </div>--}}
                </div>

                <div class="row">
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-standard-stock">Stock (Uni)<span class="text-primary font-weight-bold"> *</span></label>
                        <input required class="form-control  @error('stock') is-invalid @enderror" min="1" type="number"
                               id="unp-standard-stocke" name="stock" value="{{ old('name','1') }}">
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-standard-price">Precio Normal S/.<span class="text-primary font-weight-bold"> *</span></label>
                        <input required class="form-control @error('precio') is-invalid @enderror" type="number"
                               id="unp-standard-price" name="precio" value="{{ old('precio','0.00') }}" onClick="this.select();">
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium"></label>
                        <div class="custom-control custom-switch mt-3">
                            <input class="custom-control-input" name="flagRebaja" type="checkbox"
                                   id="customSwitch2" {{(old("flagRebaja",false) == true ? "checked":"")}}>
                            <label class="custom-control-label" for="customSwitch2">Aplicar Rebaja</label>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-extended-priceRebaja">Precio Rebaja S/.</label>
                        <input class="form-control @error('precioRebaja') is-invalid @enderror" disabled type="number"
                               value="{{ old('precioRebaja','0.00') }}" id="unp-extended-priceRebaja" onClick="this.select();"
                               name="precioRebaja">

                    </div>
                </div>

                <div class="form-group">
                    <label class="font-weight-medium" for="unp-category">Categoría<span class="text-primary font-weight-bold"> *</span></label>
                    <select required class="custom-select mr-2 @error('categoria') is-invalid @enderror" id="unp-category" name="categoria">
                        <option value="">- SELECCIONAR -</option>
                        @foreach($categorias as $categoria)
                            @if (old('categoria') == $categoria->id)
                                <option value="{{$categoria->id}}" selected>{{$categoria->nombre}}</option>
                            @else
                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>



                <div class="form-group">
                    <div class="mb-3">
                        <label class="font-weight-medium">Imagenes para el Producto</label>

                        <span class="d-block alert alert-info">Tamanio recomendado para las imagenes : 1000px X 1250px</span>
                    </div>

                    <div class="row">
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 1 <span class="text-primary font-weight-bold"> *</span></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 1 para este producto</span>
                                    <input class="cz-file-drop-input" required type="file" name="imagen1">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 2</label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 2 para este producto</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen2">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 3</label>
                                <div class="cz-file-drop-area p-1">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 3 para este producto</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen3">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 4</label>
                                <div class="cz-file-drop-area p-1">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 4 para este producto</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen4">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 5</label>
                                <div class="cz-file-drop-area p-1">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 5 para este producto</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen5">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="form-group">
                                <label class="font-weight-medium">Imagen 6</label>
                                <div class="cz-file-drop-area p-1">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 6 para este producto</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen6">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-2">
                    <button class="btn btn-info" type="submit"><i class="czi-check font-size-lg mr-2"></i>Guardar</button>
                    <a class="btn btn-primary" href="{{route('admin.productos')}}"><i class="czi-navigation font-size-lg mr-2"></i>Ver Productos</a>
                </div>

            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#customSwitch2').click(function (e) {
                var flag = $('#unp-extended-priceRebaja').prop('disabled');

                $('#unp-extended-priceRebaja').prop('disabled', !flag);
            });


            $("input[type='text,number']").click(function () {
                $(this).select();
            });
        });
    </script>

    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>


@endsection
