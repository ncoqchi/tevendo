@extends('layouts.admin')
@section('seccion')
    <section class="mb-3 px-3 px-sm-5 px-lg-10">
        <div class="mb-2">
            <div class="mt-3">
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <div class="card border-0 box-shadow">
                <div class="card-header px-2 py-0 text-center" style="background-color: #ff4648">
                    <span class="text-light mb-0 font-size-xl"><em>Nuevo Slider</em></span>
                </div>
                <div class="card-body">
                    <form class="mb-4" action="/admin/sliders" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="font-weight-medium" for="unp-category">Enlazar por:</label>
                            <div class="custom-control custom-radio custom-control-inline ml-2">
                                <input class="custom-control-input" type="radio" id="ex-radio-1" value="1" name="radio"  {{ (old('radio','1') == "1") ? 'checked' : ""}}>
                                <label class="custom-control-label" for="ex-radio-1">Categoria</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-2">
                                <input class="custom-control-input" type="radio" id="ex-radio-2" value="2" name="radio" {{ (old('radio') == "2") ? 'checked' : ""}}>
                                <label class="custom-control-label" for="ex-radio-2">Producto</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="font-weight-medium" for="unp-category">Seleccione una Categoria:</label>
                                <select class="custom-select mr-2 @error('categoria') is-invalid @enderror" id="unp-category" name="categoria">
                                    <option value="">- SELECCIONAR -</option>
                                    @foreach($categorias as $categoria)
                                        @if (old('categoria') == $categoria->id)
                                            <option value="{{$categoria->id}}" selected>{{$categoria->nombre}}</option>
                                        @else
                                            <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="font-weight-medium" for="unp-producto">Seleccione un Producto:</label>
                                <select class="custom-select mr-2 @error('producto') is-invalid @enderror" id="unp-producto" name="producto">
                                    <option value="">- SELECCIONAR -</option>
                                    @foreach($productos as $producto)
                                        @if (old('producto') == $producto->id)
                                            <option value="{{$producto->id}}" selected>{{$producto->nombre}}</option>
                                        @else
                                            <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="font-weight-medium" for="">Foto para Laptops y PC:</label>
                                <div class="mt-2">
                                    <span class="alert alert-info font-size-xs">Dimesiones de Imagen aceptable: 1920px X 650px</span>
                                </div>
                                <div class="cz-file-drop-area mt-3">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message">Arrastra y suelta una imagen para PC</span>
                                    <input required class="cz-file-drop-input" type="file" name="imagenParaPc" >
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="font-weight-medium" for="">Foto para Celulares:</label>
                                <div class="mt-2">
                                    <span class="alert alert-info font-size-xs">Dimesiones de Imagen aceptable: 600px X 600px</span>
                                </div>
                                <div class="cz-file-drop-area mt-3">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message">Arrastra y suelta una imagen para Celular</span>
                                    <input required class="cz-file-drop-input" type="file" name="imagenParaCelular">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-info" type="submit"><i class="czi-check font-size-lg mr-2"></i>Guardar</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card border-0 box-shadow mt-5">
                <div class="card-header px-2 py-0 text-center" style="background-color: #ff4648">
                    <span class="text-light mb-0 font-size-xl"><em>Lista de Sliders</em></span>
                </div>
                <div class="card-body">
{{--                    <div class="table-responsive font-size-md text-nowrap">--}}
{{--                        <table class="table table-hover table-striped mb-0">--}}
                    <div class="table-responsive font-size-md text-nowrap mb-5">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Enlace</th>
                                <th>Link</th>
                                <th>Imagen</th>
                                <th>Fecha Modificación</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sliders as $slide)
                                <tr>
                                    <td class="font-weight-bold">
                                        @if($slide->categoria_id == 0)
                                            ENLACE A PRODUCTO
                                        @else
                                            ENLACE A CATEGORIA
                                        @endif
                                    </td>
                                    <td>{{$slide->enlace}}</td>
                                    <td class="p-2"><img class="rounded" src="{{asset( $slide->imagen)}}" width="200px" style="max-width: 200px" alt=""></td>
                                    @if($slide->updated_at == null)
                                        <td>-</td>
                                    @else
                                        <td>{{$slide->created_at->format('d-m-Y H:i:s')}}</td>
                                    @endif
                                    <td>
                                        <a class="btn btn-accent" style="padding: 3px" href="/admin/sliders/{{$slide->id}}">Editar</a>
                                        <button class="btn btn-primary eliminar-slider" style="padding: 3px"  data="{{$slide->id}}">Eliminar</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('#ex-radio-1').click(function () {
                // $('#unp-category').removeAttr('disabled');
                // $('#unp-producto').attr('disabled',true);
                $('#unp-producto').val('');
            });

            $('#ex-radio-2').click(function () {
                // $('#unp-producto').removeAttr('disabled');
                // $('#unp-category').attr('disabled',true);
                $('#unp-category').val('');
            });

            $('.eliminar-slider').click(function (e) {
                // console.log("DATA ", $(this).attr('data'));
                let idSlide = $(this).attr('data');
                Swal.fire({
                    title: 'Slider',
                    text: `¿Deseas eliminar el Slider?`,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    console.log("Result :", result);
                    if (result.value) {
                        data = { _method: 'delete' , "_token": "{{ csrf_token() }}"};
                        url = '/admin/sliders/'+idSlide;
                        request = $.post(url, data);
                        request.done(function(res){
                            console.log("RESPUESTA ", res);
                            Swal.fire({
                                icon: 'info',
                                title: 'Slider',
                                text: 'Se eliminó el Slider con exito!'
                            }).then((result) =>{
                                location.reload();
                            });
                        });
                    }
                });
            });

        });
    </script>

@endsection
