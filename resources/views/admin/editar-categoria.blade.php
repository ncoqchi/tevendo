@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 pl-lg-0 pr-xl-5">

            <div class="pb-2 text-center">
                <h2 class="text-accent font-weight-bold"><em>Actualizar Categoría</em></h2>
            </div>

            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>

            <form action="{{ route('admin.actualizar.categoria')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div>
                    <input class="form-control" type="hidden" id="id-categoria" name="id" value="{{$categoria->id}}">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-product-name">Nombre:</label>
                    <input class="form-control" type="text" id="unp-product-name" name="nombre" value="{{$categoria->nombre}}">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label class="font-weight-medium" for="unp-standard-price">Imagen Actual:</label>
                        <div class="img-thumbnail rounded">
                            <img src="{{asset($categoria->imagen)}}" alt="{{$categoria->nombre}}" title="{{$categoria->nombre}}">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label class="font-weight-medium" for="unp-standard-price">Imagen Nueva</label>
                        <div class="cz-file-drop-area">
                            <div class="cz-file-drop-icon czi-cloud-upload"></div>
                            <span class="cz-file-drop-message">Arrastra y suelta una imagen para actualizar</span>
                            <input class="cz-file-drop-input" type="file" name="foto">
                            <button class="cz-file-drop-btn btn btn-success btn-sm mb-2" type="button">Seleccionar Archivo</button>
                        </div>
                    </div>

                </div>

                <div class="text-center mt-3">
                    <button class="btn btn-success mt-3" type="submit"><i class="czi-cloud-upload font-size-lg mr-2"></i>Actualizar</button>
                    <button class="btn btn-primary mt-3" ><i class="czi-cloud-upload font-size-lg mr-2"></i>Ver Categorías</button>

                </div>

                </form>

        </div>
    </section>

@endsection
