@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3">
            <div class="text-center pt-3 pb-1">
                <h4 class="font-weight-bold mb-0"><em>Ventas Por Mes Por Año</em></h4>
            </div>

            <div class="d-flex flex-nowrap justify-content-between mb-2">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.ventas.poranio')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="select-input">Seleccione El Año:</label>
                        <select class="form-control custom-select" id="select-input" name="p" onchange="this.form.submit()">
                            <option value="2020" @if (request()->get('p') == '2020') selected @endif>2020</option>
                            <option value="2021" @if (request()->get('p') == '2021') selected @endif>2021</option>
                            <option value="2022" @if (request()->get('p') == '2022') selected @endif>2022</option>
                            <option value="2023" @if (request()->get('p') == '2023') selected @endif>2023</option>
                            <option value="2024" @if (request()->get('p') == '2024') selected @endif>2024</option>
                            <option value="2025" @if (request()->get('p') == '2025') selected @endif>2025</option>
                            <option value="2026" @if (request()->get('p') == '2026') selected @endif>2026</option>
                            <option value="2027" @if (request()->get('p') == '2027') selected @endif>2027</option>
                            <option value="2028" @if (request()->get('p') == '2028') selected @endif>2028</option>
                            <option value="2029" @if (request()->get('p') == '2029') selected @endif>2029</option>
                            <option value="2030" @if (request()->get('p') == '2030') selected @endif>2030</option>
                        </select>
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('admin.ventas.poranio')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>

            <div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="table-responsive mb-3 text-nowrap">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Mes</th>
                                    <th style="text-align: right">Total Ingresos</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ingresos as $ingreso)
                                    <tr>
                                        <td class="font-weight-bold text-accent">{{$ingreso->mes}}</td>
                                        <td class="text-right">S/ {{number_format($ingreso->totalingreso, 2, '.', ',')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div style="width: 600px">
                            <div class="ct-chart ct-perfect-fourth cz-bar-chart-0"
                                 data-bar-chart='{
                 "labels": ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
                 "series": [[
                 @foreach($ingresos as $ingreso)
                   @if($loop->index==11)
                      {{$ingreso->totalingreso}}
                   @else
                      {{$ingreso->totalingreso}},
                   @endif
                 @endforeach
                 ]]}'
                                 data-series-color='{"colors": ["#ff0000"]}'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>



@endsection

@section('scripts')

@endsection
