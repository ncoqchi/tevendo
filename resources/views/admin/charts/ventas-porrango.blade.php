@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3">
            <div class="text-center mt-3 mb-2">
                <h4 class="font-weight-bold mb-0"><em>Ventas Por Rango de Fechas</em></h4>
            </div>

            <div class="mb-3">
                <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.ventas.porrangos')}}">
                    <label class="d-none d-sm-inline mt-sm-0 mr-2" for="select-input">Desde: </label>
                    <input required class="form-control" type="date" name="desde" id="date-input" value="@if(request()->get('desde')){{request()->get('desde')}}@else{{date("Y-m-d")}}@endif" >
                    <label class="d-none d-sm-inline mt-sm-0 ml-3" for="select-input">Hasta: </label>
                    <input required class="form-control" type="date" name="hasta" id="date-input-hasta" value="@if(request()->get('hasta')){{request()->get('hasta')}}@else{{date("Y-m-d")}}@endif">
                    <button class="btn btn-primary ml-2" type="submit">Consultar</button>
                </form>
            </div>

            <div>
                <div class="table-responsive mb-3 text-nowrap">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Pedido</th>
                                <th>Nombres</th>
                                <th>Celular</th>
                                <th>Correo</th>
                                <th>Detalle de Compra</th>
                                <th>Categoría</th>
                                <th class="text-right">SubTotal</th>
                                <th class="text-right">Cantidad</th>
                                <th class="text-right">Total</th>
                                <th>Fecha Compra</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ingresos as $ingreso)
                            <tr>
                                <td class="font-weight-bold text-accent">{{$ingreso->id}}</td>
                                <td>{{$ingreso->nombres}}</td>
                                <td>{{$ingreso->celular}}</td>
                                <td>{{$ingreso->correo}}</td>
                                <td>{{$ingreso->detalle}}</td>
                                <td>{{$ingreso->categoria}}</td>
                                <td class="text-right">S/ {{number_format($ingreso->subtotal, 2, '.', ',')}}</td>
                                <td class="text-right">{{$ingreso->cantidad}}</td>
                                <td class="text-right">S/ {{number_format($ingreso->total, 2, '.', ',')}}</td>
                                <td>{{$ingreso->fechacompra}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>



@endsection

@section('scripts')

@endsection
