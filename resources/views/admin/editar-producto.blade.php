@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3 px-sm-10">
            <!-- Title-->
            <div class="text-center mt-2 mb-3">
                <h2 class="mr-2 text-accent"><strong><em>Actualizar Producto</em></strong></h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <form action="{{ route('admin.actualizar.producto',$producto->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="font-weight-medium" for="unp-product-name">Nombre<span class="text-primary font-weight-bold"> *</span></label>
                    <input required class="form-control  @error('nombre') is-invalid @enderror" type="text" id="unp-product-name"
                           name="nombre" placeholder="Ingrese un Nombre" value="{{ old('nombre', $producto->nombre) }}">
                </div>

                <div class="form-group">
                    <label class="font-weight-medium" for="unp-product-description">Descripción</label>
                    <textarea class="form-control ckeditor" rows="3" id="unp-product-description" name="descripcion" placeholder="Ingrese una Descripción">{{old('descripcion', $producto->descripcion)}}</textarea>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-standard-stock">Stock (Uni)</label>
                        <input class="form-control disabled" type="text" disabled
                               id="unp-standard-stocke" name="stock" value="{{$producto->stock}}">
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-standard-price">Precio Normal S/.<span class="text-primary font-weight-bold"> *</span></label>
                        <input required class="form-control @error('precio') is-invalid @enderror" type="number"
                               id="unp-standard-price" name="precio" value="{{ old('precio',$producto->precio) }}" onClick="this.select();">
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium"></label>
                        <div class="custom-control custom-switch mt-3">
                            <input class="custom-control-input" name="flagRebaja" type="checkbox"
                                   id="customSwitch2" {{(old("flagRebaja",false) == true ? "checked":"")}}>
                            <label class="custom-control-label" for="customSwitch2">Aplicar Rebaja</label>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="font-weight-medium" for="unp-extended-priceRebaja">Precio Rebaja S/.</label>
                        <input class="form-control @error('precioRebaja') is-invalid @enderror" disabled type="number"
                               value="{{ old('precioRebaja', $producto->precioRebaja) }}" id="unp-extended-priceRebaja" onClick="this.select();"
                               name="precioRebaja">
                    </div>
                </div>

                <div class="form-group">
                    <label class="font-weight-medium" for="unp-category">Categoría<span class="text-primary font-weight-bold"> *</span></label>
                    <select required class="custom-select mr-2 @error('categoria') is-invalid @enderror" id="unp-category" name="categoria">
                        <option value="">- SELECCIONAR -</option>
                        @foreach($categorias as $categoria)
                            @if (old('categoria',$producto->categoria_id) == $categoria->id)
                                <option value="{{$categoria->id}}" selected>{{$categoria->nombre}}</option>
                            @else
                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <div class="mb-3">
                        <label class="font-weight-medium">Imagenes para el Producto</label>
                        <span class="d-block alert alert-info">Tamanio recomendado para las imagenes : 300px X 380px</span>
                    </div>

                    <div class="row">
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-primary" for="unp-standard-price"><em>Imagen 1 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen1)}}" style="height: 150px" title="Imagen 1">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-primary"><em>Imagen 1 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 1 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen1">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGEN 2--}}
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-info" for="unp-standard-price"><em>Imagen 2 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen2)}}" style="height: 150px" title="Imagen 2">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-info"><em>Imagen 2 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 1 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen2">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGEN 3--}}
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-warning" for="unp-standard-price"><em>Imagen 3 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen3)}}" style="height: 150px" title="Imagen 3">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-warning"><em>Imagen 3 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 3 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen3">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGEN 4--}}
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-dark" for="unp-standard-price"><em>Imagen 4 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen4)}}" style="height: 150px" title="Imagen 4">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-dark"><em>Imagen 4 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 4 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen4">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGEN 5--}}
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-success" for="unp-standard-price"><em>Imagen 5 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen5)}}" style="height: 150px" title="Imagen 5">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-success"><em>Imagen 5 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 5 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen5">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGEN 6--}}
                        <div class="col-6 col-sm-3">
                            <label class="font-weight-medium text-accent" for="unp-standard-price"><em>Imagen 6 Actual</em></label>
                            <div class="img-thumbnail rounded text-center">
                                <img src="{{asset($producto->imagen6)}}" style="height: 150px" title="Imagen 6">
                            </div>
                        </div>

                        <div class="col-6 col-sm-3">
                            <div class="form-group">
                                <label class="font-weight-medium text-accent"><em>Imagen 6 Nueva</em></label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message ">Arrastra y suelta la imagen 6 para actualizarla</span>
                                    <input class="cz-file-drop-input" type="file" name="imagen6">
                                    <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Archivo</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="text-center mt-2">
                    <button class="btn btn-success" type="submit"><i class="czi-check font-size-lg mr-2"></i>Actualizar</button>
                    <a class="btn btn-danger" href="{{route('admin.productos')}}"><i class="czi-navigation font-size-lg mr-2"></i>Ver Productos</a>
                </div>

            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#customSwitch2').click(function (e) {
                var flag = $('#unp-extended-priceRebaja').prop('disabled');

                $('#unp-extended-priceRebaja').prop('disabled', !flag);
            });


            $("input[type='text,number']").click(function () {
                $(this).select();
            });
        });
    </script>


    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>


@endsection
