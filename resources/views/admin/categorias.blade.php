@extends('layouts.admin')
@section('seccion')
    <section class="mb-3 px-3 px-sm-5 px-lg-10">
        <div class="mb-2">
            <!-- Title-->
            <div class="text-center pt-3 pb-1">
                <h3 class="py-2 mr-2 text-center font-weight-bold"><em>Lista de Categorías</em></h3>
            </div>
            <div class="mb-3">
                <a href="{{route('admin.nueva.categoria')}}" class="btn btn-sm btn-primary"><i class="czi-add mr-2"></i>Nueva Categoría</a>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            <div class="table-responsive mb-3 text-nowrap">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th>Ultima Modificación</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categorias as $categoria)
                        <tr>
                            <td>{{$categoria->nombre}}</td>
                            <td><img class="rounded" src="{{asset($categoria->imagen)}}" width="150px" style="max-width: 200px" alt=""> </td>
                            @if($categoria->updated_at == null)
                                <td>-</td>
                            @else
                            <td class="p-2">{{$categoria->updated_at->format('d-m-Y H:i:s')}}</td>
                            @endif
                            <td>
                                <a class="btn btn-sm btn-info" href="{{ route('admin.editar.categoria', $categoria->id) }}"><i class="czi-edit"></i> Editar</a>
                                <form class="d-inline" method="POST" action="{{route('admin.eliminar.categoria', $categoria->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-primary"  type="submit"><i class="czi-trash"></i> Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="d-flex justify-content-sm-center">
                    {{$categorias->appends(request()->query())->links()}}
                </div>
            </div>

        </div>
    </section>

@endsection
