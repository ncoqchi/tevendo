@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="d-sm-flex flex-wrap justify-content-between align-items-center pb-2">
                <h2 class="h3 py-2 mr-2 text-center text-sm-left">Nuevo Tipo Envío</h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            <form action="{{ route('admin.registrar.tipoenvio')}}" method="POST">
                @csrf
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-product-name">Nombre</label>
                    <input class="form-control" type="text" id="unp-product-name" name="nombre">
                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label class="font-weight-medium" for="unp-standard-price">Precio</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">S/.</span>
                            </div>
                            <input class="form-control" type="text" id="unp-standard-price" name="precio">
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btn-block" type="submit"><i class="czi-cloud-upload font-size-lg mr-2"></i>Guardar</button>
            </form>
        </div>
    </section>

@endsection
