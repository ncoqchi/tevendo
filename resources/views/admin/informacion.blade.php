@extends('layouts.admin')
@section('seccion')
    <section class="mb-3 px-3 px-sm-5 px-lg-10">
        <div class="mb-2">
            <div class="text-center pt-3 pb-1">
{{--                style="text-shadow: 1px 2px 3px #969bf5;"--}}
                <h3 class="py-2 mr-2 font-weight-bold" ><em>Actualizar Información General</em></h3>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            </div>

            <form action="{{ route('admin.actualizar.informacion')}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label class="font-weight-medium" for="unp-category">Banco</label>
                    <select required class="custom-select mr-2" id="unp-category" name="banco" >
                        <option @if($info->banco == 'BBVA') selected @endif value="BBVA">BBVA</option>
                        <option @if($info->banco == 'BCP') selected @endif value="BCP">BCP</option>
                        <option @if($info->banco == 'INTERBANK') selected @endif  value="INTERBANK">INTERBANK</option>
                        <option @if($info->banco == 'SCOTIABANK') selected @endif value="SCOTIABANK">SCOTIABANK</option>
                    </select>
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-numero">Número De Cuenta:</label>
                    <input class="form-control mb-2" required type="text" id="unp-numero" name="cuenta" value="{{$info->cuenta}}" placeholder="Ingrese un Numero de Cuenta">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-cci">CCI:</label>
                    <input class="form-control mb-2" required type="text" id="unp-cci" name="cci" value="{{$info->cci}}" placeholder="Ingrese codigo interbancario">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-whastapp">Whastapp:</label>
                    <input class="form-control mb-2" required type="text" id="unp-whastapp" name="whastapp" value="{{$info->whastapp}}" placeholder="Ingrese un numero de whastapp">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-whastapp">Facebook:</label>
                    <input class="form-control mb-2" required type="text" id="unp-whastapp" name="facebook" value="{{$info->facebook}}" placeholder="Ingrese su URL de Facebook">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-whastapp">Instagram:</label>
                    <input class="form-control mb-2" required type="text" id="unp-whastapp" name="instagram" value="{{$info->instagram}}" placeholder="Ingrese su URL de Instagram">
                </div>

                <div class="text-center mt-2" >
                    <button class="btn btn-info" type="submit"><i class="czi-check font-size-lg mr-2"></i>Actualizar</button>
                </div>
            </form>

        </div>
    </section>

@endsection
