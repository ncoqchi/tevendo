@extends('layouts.admin')
@section('seccion')
    <div class="mb-3">
        <div class="mb-2">
            <!-- Title-->
            <div class="text-center pt-3 pb-1">
                <h3 class="py-1 font-weight-bold"><em>Adjuntar Voucher al Pedido</em></h3>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i>
                        </div>
                        {{session('mensaje')}}
                    </div>
                    <div class="mb-3">
                        <a class="btn btn-primary" href="{{route('admin.pedidos.depositados')}}">Ir a Pedidos Con Depósito Confirmado</a>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            @if($recibo)
            <div class="row mt-2 mb-5">
                <div class="col-sm-5">
                    <div class="card border-0 box-shadow p-3">
                        <div class="text-center">
                            <h4 class="mb-3">Pedido
                                <span class="font-weight-bold text-primary" style="font-family: monospace; font-size: 2rem">
                                <u>PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</u>
                            </span>
                            </h4>
                        </div>
                        <form method="POST" action="{{route('admin.pedidos.adjuntar.voucher.enviar', $recibo->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3 pr-4">
                                <div class="col-3 font-weight-medium">Usuario:</div>
                                <div class="col-7 pl-0">
                                    <span class="">{{$recibo->user->name}}</span>
                                    <img class="rounded d-block" width="50" src="{{asset($recibo->user->avatar)}}" alt="{{$recibo->user->name}}"/>
                                </div>
                            </div>
                            <div class="row mb-3 pr-4">
                                <div class="col-3 font-weight-medium">Estado:</div>
                                <div class="col-7 pl-0">
                                    @if($recibo->estado == 1)
                                        <span class="badge badge-warning font-size-lg">Pendiente</span>
                                    @elseif($recibo->estado == 2)
                                        <span class="badge badge-info font-size-lg">Costo Envío Asignado</span>
                                    @elseif($recibo->estado == 3)
                                        <span class="badge badge-success font-size-lg">Enviado</span>
                                    @elseif($recibo->estado == 4)
                                        <span class="badge badge-danger font-size-lg">Entregado</span>
                                    @elseif($recibo->estado == 6)
                                        <span class="badge badge-info font-size-lg">Voucher Adjuntado - Pago Confirmado</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3 pr-4">
                                <div class="col-3 font-weight-medium" >Tipo de Envío</div>
                                <div class="col-7 pl-0">
                                    @if($recibo->tipoenvio == 1)
                                        <span class="text-success font-weight-bold"><i class="czi-hand-wash opacity-60"></i> Contra Entrega</span>
                                    @else
                                        <span class="text-primary font-weight-bold"><i class="czi-card opacity-60"></i> Depósito en Cuenta</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-4 pr-4">
                                <div class="col-3 font-weight-medium">Comentarios:</div>
                                <div class="col-7 pl-0"><span>{{$recibo->comentarios}}</span></div>
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-3 font-weight-medium" >SubTotal S/</label>
                                <input class="form-control col-3 text-right" disabled type="text" name="subtotal"
                                       value="{{number_format($recibo->subtotal, 2, '.', '')}}">
                                <label class="col-3 font-weight-medium" >IGV S/</label>
                                <input class="form-control col-3 text-right" disabled type="text" name="igv"
                                       value="{{number_format($recibo->igv, 2, '.', '')}}">
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-3 font-weight-medium" for="id-costo-envio" >Costo Envío S/</label>
                                <input disabled class="form-control col-3 text-right font-weight-bold" id="id-costo-envio"
                                       type="number" name="costoenvio" value="{{number_format($recibo->costoenvio, 2, '.', '')}}">
                                <label class="col-3 font-weight-medium" for="id-total" >Total S/</label>
                                <input class="form-control col-3 text-right text-primary font-weight-bold font-size-lg"
                                       id="id-total" disabled type="text" name="totalactual"
                                       value="{{number_format($recibo->total, 2, '.', ',')}}">
                            </div>

                            @if($recibo->estado == 6)
                                <div class="row pb-2 ml-2">
                                    <div class="form-group w-100">
                                        <label class="font-weight-medium" for="unp-standard-price">Voucher:</label>
                                        <div class="img-thumbnail rounded">
                                            <img class="w-100" src="{{asset($recibo->imagen_voucher)}}" alt="{{$recibo->id}}">
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row pb-1 ml-2">
                                    <div class="form-group w-100">
                                        <label class="font-weight-medium">Voucher<span class="text-primary font-weight-bold"> *</span></label>
                                        <div class="cz-file-drop-area">
                                            <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                            <span class="cz-file-drop-message ">Arrastra y suelta la imagen del Voucher</span>
                                            <input class="cz-file-drop-input" required type="file" name="imagen">
                                            <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Imagen</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($recibo->estado == 2 && $recibo->tipoenvio==2)
                                <div class="text-center mb-3">
                                    <button type="submit" class="btn btn-info mt-1">
                                        <i class="czi-navigation font-size-lg mr-2"></i>Registrar Voucher</button>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Datos de Envío</u></h4></div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Cliente</div>
                                    <div class="font-weight-bold text-accent font-size-lg">{{ucwords($recibo->envio->nombres)}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium ">Correo Electrónico</div>
                                    <div class="font-weight-bold text-accent font-size-lg">{{$recibo->envio->correo_electronico}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Celular</div>
                                    <div class="">{{$recibo->envio->celular}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Ubigeo</div>
                                    <div class="">{{$recibo->envio->distrito_id}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Dirección</div>
                                    <div class="">{{$recibo->envio->direccion}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Referencia</div>
                                    <div class="">{{ $recibo->envio->referencia}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Productos</u></h4></div>
                                @foreach($detalle as $pro)
                                    <div class="mb-2">
                                        <div class="media d-block d-flex">
                                            <div class="w-25">
                                                <a class="d-inline-block mr-2" href="#">
                                                    <img class="rounded" style="height: 80px" src="{{asset(explode(',',$pro->imagen)[0]) }}" alt="Product">
                                                </a>
                                            </div>
                                            <div class="w-75 media-body pt-1">
                                                <div class="font-size-xs mb-1"><a class="text-dark font-weight-medium" href="">{{$pro->categoria}}</a></div>
                                                <div class="font-size-xs"><a href="" class="text-dark">{{ucwords( strtolower($pro->detalle)  )}}</a></div>
                                                <div class="font-size-xs">S/ {{number_format($pro->subtotal, 2, '.', '')}} x {{$pro->cantidad}} = <span class="font-weight-medium text-primary">S/ {{number_format($pro->total, 2, '.', '')}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="container py-5 mb-lg-3">
                <div class="row justify-content-center pt-lg-4 text-center">
                    <div class="col-sm-9">
                        <h1 class="display-404">N° de pedido no encontrado</h1>
                        <h4 class="mb-4 text-info"><em>Este Pedido ya no se encuentra en un estado para Adjuntar Voucher</em></h4>
                        <p class="font-size-lg mb-4"><a href="{{route('admin.pedidos.costoenvio')}}">Regresar</a></p>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function(){
            // alert("Hola");
            $('#id-costo-envio').keyup(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).change(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).focusout(function(e) {
                console.log("Focus Out:", e);
            });


            $('#id-btn-pedido-pendiente').click(function (e) {
                var costoEnvio = parseFloat($('#id-costo-envio').val()).toFixed(2);
                var idPedido = $(this).attr('data');
                console.log(idPedido);
                var pedido = (idPedido +"").padStart(8,"0");

                Swal.fire({
                    icon:'question',
                    title: `Pedido PED-${pedido}`,
                    html: `¿Deseas asignar <span class="text-primary font-weight-bold font-size-xl"><em>S/ ${costoEnvio}</em></span>, como costo de envío para este Pedido?`,
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar Costo de Envío',
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return fetch(`/admin/pedido-pendiente/actualizar-costoenvio/${idPedido}/${costoEnvio}`)
                            .then(response => {
                                console.log("Return Fetch " , response);
                                if (!response) {
                                    console.log("Return Fetch Error");
                                    throw new Error(response)
                                } else {
                                    console.log("ELSE", response);
                                    if (response.url.substring( response.url.lastIndexOf('/'), response.url.length ) == '/login') {
                                        throw new Error("Su sesión ha caducado, necesita loguearse");
                                    } else {
                                        console.log("Then");
                                        return response.json();
                                    }
                                }
                            })
                            .catch(error => {
                                console.log("Errorcito ", error);
                                Swal.showValidationMessage (
                                    `${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado Fetch :", result);
                    if (result.isConfirmed) {
                        Swal.fire({
                            icon:'success',
                            title: `Pedido #PED-${pedido}`,
                            html: `El costo de envío actualizado fue enviado al correo de ${result.value.nombres.toUpperCase()}:
                                    <a href="" style="color:blue">${result.value.correo}</a>`,
                        }).then(res => {
                            window.location.href = '/admin/pedidos-pendientes'
                        });
                    }
                });
            });
        });
    </script>

@endsection

