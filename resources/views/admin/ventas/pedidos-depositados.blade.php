@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3">
            <div class="d-flex flex-wrap justify-content-center align-items-center py-3">
                <h3 class="mr-2 text-center font-weight-bold mb-0"><em>Pedidos con Depósito Confirmado</em></h3>
            </div>

            <div>
                <span class="alert alert-info p-0"><em>Debe alistar el pedido que desee enviar y luego hacer click sobre 'Confirmar Envío' para notificar al cliente el envío de sus productos</em> </span>
            </div>
            <div>
                <span class="alert alert-info p-0"><em>Caso Contrario puede anular o cancelar el pedido</em></span>
            </div>

            <div class="d-flex flex-nowrap justify-content-between mb-1">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.pedidos.depositados')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="id-num-pedido">Buscar por Nº de pedido:</label>
                        <input id="id-num-pedido" type="text" class="form-control" placeholder="Ingrese N° de pedido" name="num_pedido"
                               value="{{request()->get('num_pedido')}}">
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('admin.pedidos.depositados')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>

{{--            <div>--}}
{{--                <span class="alert alert-info">Para Visualizar el detalle, hacer click sobre el N de pedido</span>--}}
{{--            </div>--}}

            <div class="table-responsive font-size-md text-nowrap">
                <table class="table table-hover table-striped mb-0">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Cliente</th>
                            <th>Fecha Modificación</th>
                            <th>Tipo de Envío</th>
                            <th style="text-align: right">Costo Envío</th>
                            <th>Estado</th>
                            <th style="text-align: right">Total</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($recibos as $recibo)
                        <tr>
                            <td class="py-2">
                                <a class="font-weight-bold font-size-sm text-dark" href="#"
                                   onclick="verModal('{{$recibo->id}}')" data-toggle="modal">PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</a>
                            </td>
                            <td class="">{{ucwords($recibo->envio->nombres)}} / {{$recibo->envio->correo_electronico}}</td>
                            <td class="py-2">{{date_format($recibo->created_at, 'd-m-Y H:i:s')}}</td>
                            <td class="py-2">
                                @if($recibo->tipoenvio == 1)
                                    <span class="text-success font-weight-medium"><i class="czi-dollar font-weight-medium"></i> Contra Entrega</span>
                                @else
                                    <span class="text-primary font-weight-medium">
                                        <i class="czi-card font-weight-medium"></i> Depósito en Cuenta
                                    </span>
                                @endif
                            </td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->costoenvio, 2, '.', ',')}}</td>
                            <td class="py-2">
                                @if($recibo->estado == 1)
                                    <span class="badge badge-warning m-0">Pendiente</span>
                                @elseif($recibo->estado == 2)
                                    <span class="badge badge-info m-0">Costo de Envío Asignado</span>
                                @elseif($recibo->estado == 3)
                                    <span class="badge badge-success m-0">Enviado</span>
                                @elseif($recibo->estado == 4)
                                    <span class="badge badge-danger m-0">Entregado</span>
                                @elseif($recibo->estado == 6)
                                    <span class="badge badge-accent m-0">Voucher Adjuntado - Pago Confirmado</span>
                                @endif
                            </td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->total, 2, '.', ',')}}</td>
                            <td style="text-align: center">
                                <button data="{{$recibo->id}}" cliente="{{$recibo->envio->nombres}}"
                                        correo="{{$recibo->envio->correo_electronico}}" style="padding: 3px 8px"
                                        class="id-btn-confirmar-envio btn btn-success">Confirmar Envío
                                </button>
                                {{--onclick="window.location='{{route("admin.editar.productopendiente",$recibo->id)}}'"--}}
                                <button style="padding: 3px 8px" class="btn btn-dark id-cancelar-pedido"
                                        data="{{$recibo->id}}" cliente="{{$recibo->envio->nombres}}"
                                        correo="{{$recibo->envio->correo_electronico}}">Cancelar Pedido</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="pb-4">
            <div class="">
                {{$recibos->appends(request()->query())->links()}}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.id-cancelar-pedido').click(function (e) {
                var idPedido = $(this).attr('data');
                var cliente = $(this).attr('cliente');
                var correo = $(this).attr('correo');
                console.log(idPedido);
                var pedido = (idPedido +"").padStart(8,"0");

                Swal.fire({
                    icon:'question',
                    title: `Pedido PED-${pedido}`,
                    input: 'text',
                    inputPlaceholder:'Ingrese un Motivo',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    html: `¿Deseas <span class="text-primary font-weight-bold">CANCELAR</span> el pedido de <span class="font-weight-bold">${cliente}</span>?
                                    <div>Se enviará una notificación  a: <a class="d-block" href="" style="color:blue">${correo}</a></div>`,
                    showCancelButton: true,
                    confirmButtonText: 'Cancelar Pedido',
                    cancelButtonText: 'Salir',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (motivo) => {
                        if(motivo == ''){
                            motivo='-';
                        }
                        return fetch(`/admin/pedidos/cancelar/${idPedido}/${motivo}`)
                            .then(response => {
                                console.log("Ajax " , response);
                                if (!response) {
                                    throw new Error(response)
                                }
                                return response.json();
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado :", result);
                    if (result.isConfirmed) {
                        Swal.fire({
                            icon:'success',
                            title: `Pedido PED-${pedido}`,
                            html: `Se <span class="text-primary font-weight-bold">CANCELÓ</span> el pedido de ${result.value.nombres.toUpperCase()}, se envió una correo a
                                    <a href="" style="color:blue">${result.value.correo}</a>`,
                            // text: `El pedido de ${result.value.nombres.toUpperCase()} con celular:${result.value.celular},  ha sido enviado a: ${result.value.correo}`
                            // imageUrl: result
                        }).then(res => {
                            document.location.reload();
                        });
                    }
                });
            });

            $('.id-btn-confirmar-envio').click(function (e) {
                var idPedido = $(this).attr('data');
                var cliente = $(this).attr('cliente');
                var correo = $(this).attr('correo');
                console.log(idPedido);
                var pedido = (idPedido + "").padStart(8, "0");

                Swal.fire({
                    icon: 'question',
                    title: `Pedido PED-${pedido}`,
                    html: `¿Deseas confirmar el Envío del Pedido de <span class="font-weight-bold">${cliente}</span>?
                            <div>Se enviará una notificación a:<a class="d-block" href="" style="color:blue">${correo}</a></div>`,
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar Envío',
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return fetch(`/admin/pedidos-con-costo-envio/actualizar/${idPedido}`)
                            .then(response => {
                                console.log("Ajax ", response);
                                if (!response) {
                                    throw new Error(response)
                                }
                                return response.json();
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado :", result);
                    if (result.isConfirmed) {
                        Swal.fire({
                            icon: 'success',
                            title: `Pedido PED-${pedido}`,
                            html: `El pedido de <span class="text-primary font-weight-bold">${result.value.nombres.toUpperCase()}</span> a sido enviado a su dirección, se le notificará al correo:
                                <a href="" style="color:blue">${result.value.correo}</a>
                              `,
                            // text: `El pedido de ${result.value.nombres.toUpperCase()} con celular:${result.value.celular},  ha sido enviado a: ${result.value.correo}`
                            // imageUrl: result
                        }).then(res => {
                            document.location.reload();
                        });
                    }
                });
            });
        });
    </script>
@endsection
