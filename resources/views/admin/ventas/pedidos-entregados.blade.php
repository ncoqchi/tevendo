@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <!-- Page Content-->
        <div class="pb-5 mb-2 px-3">
            <!-- Toolbar-->
            <div class="d-flex flex-wrap justify-content-center align-items-center pb-2 mt-3">
                <h4 class="mr-2 text-center font-weight-medium"><i class="czi-mail font-weight-medium"></i><em> Pedidos Entregados</em></h4>
            </div>

            <div class="d-flex flex-nowrap justify-content-between mb-1">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.pedidos.entregados')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="id-num-pedido">Buscar por Nº de pedido:</label>
                        <input id="id-num-pedido" type="text" class="form-control" placeholder="Ingrese n° de pedido" name="num_pedido"
                               value="{{request()->get('num_pedido')}}">
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('admin.pedidos.entregados')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>

            <!-- Orders list-->
            <div class="table-responsive font-size-md text-nowrap">
                <table class="table table-hover table-striped mb-0">
                    <thead>
                    <tr>
                        <th>Pedido</th>
                        <th>Estado</th>
                        <th>Cliente</th>
                        <th>Correo</th>
                        <th>Celular</th>
                        <th>Fecha Modificación</th>
                        <th>Tipo de Envío</th>
                        <th style="text-align: right">Cto.Envío</th>
                        <th style="text-align: right">Sub Total</th>
                        <th style="text-align: right">Dscto</th>
                        <th style="text-align: right">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recibos as $recibo)
                        <tr>
                            <td class="py-2">
                                <a class="text-dark font-weight-medium" href="#"
                                   onclick="verModal('{{$recibo->id}}')"
                                   data-toggle="modal">PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</a>
                            </td>
                            <td class="py-2">
                                <span class="badge badge-primary m-0 font-size-md">Entregado</span>
                            </td>
                            <td class="">{{ucwords($recibo->envio->nombres)}}</td>
                            <td class="">{{$recibo->envio->correo_electronico}}</td>
                            <td><em class="">{{$recibo->envio->celular}}</em></td>
                            <td class="py-2">{{date_format($recibo->created_at, 'd-m-Y H:i:s A')}}</td>
                            <td class="py-2">
                                @if($recibo->tipoenvio == 1)
                                    <span class="text-success font-weight-medium"><i class="czi-dollar font-weight-medium"></i>Contra Entrega</span>
                                @else
                                    <span class="text-primary font-weight-medium"><i class="czi-card font-weight-medium"></i> Depósito en Cuenta</span>
                                @endif
                            </td>
                            <td class="py-2" style="text-align: right">S/{{number_format($recibo->costoenvio, 2, '.', ',')}}</td>
                            <td class="py-2 font-weight-medium" style="text-align: right">S/ {{number_format($recibo->total, 2, '.', ',')}}</td>
                            <td class="py-2 text-primary font-weight-medium" style="text-align: right">-S/{{number_format($recibo->descuento, 2, '.', ',')}}</td>
                            <td class="py-2 text-info font-weight-medium" style="text-align: right">S/{{number_format($recibo->total_final, 2, '.', ',')}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="pb-4">
            <!-- Pagination-->
            <div class="paginador">
                {{$recibos->appends(request()->query())->links()}}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.id-btn-confirmar-envio').click(function (e) {
                var idPedido = $(this).attr('data');
                console.log(idPedido);
                var pedido = (idPedido + "").padStart(8, "0");

                Swal.fire({
                    title: `Pedido #PED-${pedido}`,
                    text: `¿Deseas confirmar el Envío del Pedido?`,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.value) {
                        //PUT  a admin/pedidos-con-costo-envio/{idPedido}
                        data = {_method: 'put', "_token": "{{ csrf_token() }}"};

                        url = '/admin/pedidos-con-costo-envio/' + idPedido;

                        request = $.post(url, data);

                        request.done(function (res) {

                            console.log("RESPUESTA ", res);

                            Swal.fire({
                                icon: 'info',
                                title: 'Slider',
                                text: '¡ Se envió un Correo al Cliente indicando que su pedido ha sido enviado !'
                            }).then((result) => {
                                document.location.reload();
                            });

                        });
                    }
                });
            });
        });
    </script>
@endsection
