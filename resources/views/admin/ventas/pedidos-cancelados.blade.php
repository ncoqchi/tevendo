@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">

        <!-- Page Content-->
        <div class="pb-5 mb-2 px-3">
            <!-- Toolbar-->
            <div class="d-flex flex-wrap justify-content-center align-items-center py-3">
                <h4 class="mr-2 text-center font-weight-bold"><em>Pedidos Cancelados por el Admin</em></h4>
            </div>

            <div class="d-flex flex-nowrap justify-content-between mb-1">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.pedidos.cancelados')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="id-num-pedido">Buscar por Nº de pedido:</label>
                        <input id="id-num-pedido" type="text" class="form-control" placeholder="Ingrese n° de pedido" name="num_pedido"
                               value="{{request()->get('num_pedido')}}">
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('admin.pedidos.cancelados')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>

            <!-- Orders list-->
            <div class="table-responsive font-size-md text-nowrap">
                <table class="table table-hover table-striped mb-0">
                    <thead>
                    <tr>
                        <th>Pedido</th>
                        <th>Cliente</th>
                        <th>Celular</th>
                        <th>Fecha de Compra</th>
                        <th>Tipo de Envío</th>
                        <th style="text-align: right">Costo de Envío</th>
                        <th>Estado</th>
                        <th style="text-align: right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recibos as $recibo)
                        <tr>
                            <td class="py-2">
                                <a class="nav-link-style font-weight-medium font-size-sm" href="#"
                                   onclick="verModal('{{$recibo->id}}')"
                                   data-toggle="modal">PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</a>
                            </td>
                            <td class="font-weight-medium">{{ucwords($recibo->envio->nombres)}}</td>
                            <td><em class="text-info">{{$recibo->envio->celular}}</em></td>
                            <td class="py-2">{{date_format($recibo->created_at, 'd-m-Y H:i:s A')}}</td>
                            <td class="py-2">
                                @if($recibo->tipoenvio == 1)
                                    Contra Entrega
                                @else
                                    Depósito en Cuenta
                                @endif
                            </td>
                            <td class="py-2" style="text-align: right">S/ {{$recibo->costoenvio}}</td>
                            <td class="py-2">
                                <span class="badge badge-danger m-0">CANCELADO</span>
                            </td>
                            <td class="py-2" style="text-align: right">
                                S./ {{number_format($recibo->total, 2, '.', ',')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="pb-4">
            <!-- Pagination-->
            <div class="paginador">
                {{$recibos->appends(request()->query())->links()}}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>

    </script>
@endsection
