@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-9">
        <div class="px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="text-center">
                <h2 class="py-3 text-accent"><em>Asignar Costo de Envío</em></h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-7 mb-5">
                    <form action="{{ route('admin.actualizar.productopendiente', $recibo->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row pb-2">
                            <label class="col-sm-4" for="id-producto">Pedido #</label>
                            <input class="form-control col-sm-8 font-weight-bold text-primary" style="font-family: monospace; font-size: 2.5rem" disabled type="text"
                                   id="id-producto" name="id" value="PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}">
                        </div>
                        <div class="row pb-2">
                            <label class="col-sm-4 mt-2" for="id-producto">Cliente:</label>
                            <div class="col-sm-8 pl-0">
                                <span class="font-weight-bold">{{strtoupper($recibo->user->name)}}</span>
                                <img class="rounded ml-2" width="50" src="{{asset($recibo->user->avatar)}}" alt="{{$recibo->user->name}}"/>
                            </div>
                        </div>
                        <div class="row pb-2 mt-3">
                            <label class="col-sm-4" for="id-producto">Estado:</label>
                            <div class="col-sm-8 pl-0" >
                                @if($recibo->estado == 1)
                                    <span class="badge badge-warning font-size-lg">Pendiente</span>
                                @elseif($recibo->estado == 2)
                                    <span class="badge badge-info font-size-lg">Costo Envío Asignado</span>
                                @elseif($recibo->estado == 3)
                                    <span class="badge badge-success font-size-lg">Enviado</span>
                                @elseif($recibo->estado == 4)
                                    <span class="badge badge-danger font-size-lg">Completado</span>
                                @endif
                            </div>
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4" for="id-tipo-envio">Tipo de Envio:</label>
                            <input class="form-control col-sm-8" disabled type="text" id="id-tipo-envio"
                                   name="id-tipo-envio" value="@if($recibo->envio == 1) CONTRA ENTREGA @else DEPOSITO EN CUENTA  @endif">
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4">Comentarios:</label>
                            <textarea class="form-control col-sm-8" rows="4" disabled name="descripcion">{{$recibo->comentarios}}</textarea>
                        </div>
{{--                        <div class="row pb-2">--}}
{{--                            <label class="col-sm-3">Tipo de Envío:</label>--}}
{{--                            <input class="form-control col-sm-9" rows="4" disabled name="descripcion">--}}
{{--                                @if($recibo->envio == 1)--}}
{{--                                    <span></span>--}}
{{--                                @else--}}
{{--                                @endif--}}
{{--                        </div>--}}
                        <div class="row pb-2">
                            <label class="col-sm-4" >SubTotal S/:</label>
                            <input class="form-control col-sm-8 text-right" disabled type="text" name="subtotal"
                                   value="{{number_format($recibo->subtotal, 2, '.', '')}}">
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4" >IGV S/:</label>
                            <input class="form-control col-sm-8 text-right" disabled type="text" name="igv"
                                   value="{{number_format($recibo->igv, 2, '.', '')}}">
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4" for="id-total" >Total Actual S/:</label>
                            <input class="form-control col-sm-8 text-right font-weight-bold text-primary"
                                   id="id-total" disabled type="text" name="totalactual"
                                   value="{{number_format($recibo->total, 2, '.', '')}}">
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4" for="id-costo-envio" >Costo de Envío S/:</label>
                            <input class="form-control col-sm-8 font-size-xl text-right" id="id-costo-envio"
                                   type="number" name="costoenvio" value="{{$recibo->costoenvio}}">
                        </div>

                        <div class="row pb-2">
                            <label class="col-sm-4" for="id-nuevo-total" >Nuevo Total S/:</label>
                            <input class="form-control col-sm-8 text-right text-primary font-weight-bold" id="id-nuevo-total"
                                   style="font-family: monospace; font-size: 2.5rem; font-style: italic" disabled
                                   type="text" name="totalactualizado" value="{{number_format($recibo->total, 2, '.', '')}}">
                        </div>

                        <div class="text-center mt-2" >
                            <button @if($recibo->estado >= 2) disabled @endif class="btn btn-success" type="submit">
                                <i class="czi-check font-size-lg mr-2"></i>Asignar Costo de Envío</button>
                            <a class="btn btn-danger" href="{{route('admin.pedidos.pendientes')}}">
                                <i class="czi-navigation font-size-lg mr-2"></i>Ver Pedidos Pendientes</a>
                        </div>
                    </form>
                </div>
                <div class="col-sm-5">
                    <div class="card p-3">
                        <div class="text-center"><h4 class="mb-3">Productos <small>({{sizeof($detalle)}})</small></h4></div>
                        @foreach($detalle as $pro)
                        <div class="d-flex mb-2">
                            <div class="media d-block d-flex">
                                <a class="d-inline-block mr-4" href="#">
                                    <img class="rounded" style="height: 80px" src="{{asset(explode(',',$pro->imagen)[0]) }}" alt="Product">
                                </a>
                                <div class="media-body pt-2">
                                    <span class="product-title mb-1"><a href="">{{$pro->detalle}}</a></span>
                                    <div class="text-accent">S/. {{$pro->subtotal}} x {{$pro->cantidad}} = S/.{{$pro->total}}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function(){
            // alert("Hola");
            $('#id-costo-envio').keyup(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).change(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).focusout(function(e) {
                // $(this).val(e.target.value.toFixed(2));
                console.log("Focus Out:", e);
                // $('#id-costo-envio').val($('#id-costo-envio').val().toFixed(2));
                // let costoenvio = parseFloat($(this).val());
                // $(this).val(parseFloat(costoenvio.toFixed(2) ));
            });
        });
    </script>

@endsection

