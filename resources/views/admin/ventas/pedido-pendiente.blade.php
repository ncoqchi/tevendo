@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-10">
        <div class="px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="text-center">
                <h2 class="py-3 text-accent font-weight-bold"><em>Asignar Costo de Envío</em></h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            @if($recibo)
            <div class="row mt-2 mb-5">
                <div class="col-sm-5">
                    <div class="card border-0 box-shadow p-3">
                        <div class="text-center">
                            <h4 class="mb-3">Pedido
                                <span class="font-weight-bold text-primary" style="font-family: monospace; font-size: 2rem">
                                <u>PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</u>
                            </span>
                            </h4>
                        </div>
                        <div class="form-group" >
                            <div class="row mb-3 pr-4">
                                <div class="col-5 font-weight-medium">Registrado Por:</div>
                                <div class="col-7 pl-0">
                                    <div class="">{{strtoupper($recibo->user->name)}}</div>
                                    <img class="rounded" width="50" src="{{asset($recibo->user->avatar)}}" alt="{{$recibo->user->name}}"/>
                                </div>
                            </div>
                            <div class="row mb-3 pr-4">
                                <div class="col-5 font-weight-medium">Estado:</div>
                                <div class="col-7 pl-0">
                                    @if($recibo->estado == 1)
                                        <span class="badge badge-warning font-size-lg">Pendiente</span>
                                    @elseif($recibo->estado == 2)
                                        <span class="badge badge-info font-size-lg">Costo Envío Asignado</span>
                                    @elseif($recibo->estado == 3)
                                        <span class="badge badge-success font-size-lg">Enviado</span>
                                    @elseif($recibo->estado == 4)
                                        <span class="badge badge-danger font-size-lg">Entregado</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3 pr-4">
                                <div class="col-5 font-weight-medium" >Tipo de Envío</div>
                                <div class="col-7 pl-0">@if($recibo->tipoenvio == 1) CONTRA ENTREGA @else DEPOSITO EN CUENTA  @endif</div>
                            </div>

                            <div class="row mb-4 pr-4">
                                <div class="col-5 font-weight-medium">Comentarios:</div>
                                <div class="col-7 pl-0"><span>{{$recibo->comentarios}}</span></div>
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-5 font-weight-medium" >SubTotal S/</label>
                                <input class="form-control col-7 text-right" disabled type="text" name="subtotal"
                                       value="{{number_format($recibo->subtotal, 2, '.', '')}}">
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-5 font-weight-medium" >IGV S/</label>
                                <input class="form-control col-7 text-right" disabled type="text" name="igv"
                                       value="{{number_format($recibo->igv, 2, '.', '')}}">
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-5 font-weight-medium" for="id-total" >Total Actual S/</label>
                                <input class="form-control col-7 text-right font-weight-bold text-primary"
                                       id="id-total" disabled type="text" name="totalactual"
                                       value="{{number_format($recibo->total, 2, '.', '')}}">
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-5 font-weight-medium" for="id-costo-envio" >Costo Envío S/</label>
                                <input class="form-control col-7 font-size-xl text-right" id="id-costo-envio"
                                       type="number" name="costoenvio" value="{{$recibo->costoenvio}}">
                            </div>

                            <div class="row pb-2 pr-4">
                                <label class="col-5 font-weight-medium" for="id-nuevo-total" >Nuevo Total S/</label>
                                <input class="form-control col-7 text-right text-primary font-weight-bold" id="id-nuevo-total"
                                       style="font-family: monospace; font-size: 2.5rem; font-style: italic" disabled
                                       type="text" name="totalactualizado" value="{{number_format($recibo->total, 2, '.', '')}}">
                            </div>

                            <div class="mt-2 text-center mb-3">
                                <button id="id-btn-pedido-pendiente" data="{{$recibo->id}}" @if($recibo->estado >= 2) disabled @endif class="btn btn-success mt-3" >
                                    <i class="czi-check font-size-lg mr-2"></i>Asignar Costo de Envío</button>
                                <a class="btn btn-danger mt-3" href="{{route('admin.pedidos.pendientes')}}">
                                    <i class="czi-navigation font-size-lg mr-2"></i>Ver Pedidos Pendientes</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Datos de Envío</u></h4></div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Cliente</div>
                                    <div class="">{{ucwords($recibo->envio->nombres)}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Correo Electrónico</div>
                                    <div>{{$recibo->envio->correo_electronico}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Celular</div>
                                    <div class="">{{$recibo->envio->celular}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Ubigeo</div>
                                    <div class="">{{$recibo->envio->distrito_id}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Dirección</div>
                                    <div class="">{{$recibo->envio->direccion}}</div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Referencia</div>
                                    <div class="">{{ $recibo->envio->referencia}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Productos</u></h4></div>
                                @foreach($detalle as $pro)
                                    <div class="mb-2">
                                        <div class="media d-block d-flex">
                                            <div class="w-50">
                                                <a class="d-inline-block mr-2" href="#">
                                                    <img class="rounded" style="height: 80px" src="{{asset(explode(',',$pro->imagen)[0]) }}" alt="Product">
                                                </a>
                                            </div>
                                            <div class="w-50 media-body pt-1">
                                                <div class="font-size-xs mb-1"><a class="text-dark" href="">{{$pro->categoria}}</a></div>
                                                <div class="font-size-xs"><a href="" class="text-info">{{ucwords( strtolower($pro->detalle)  )}}</a></div>
                                                <div class="font-size-xs">S/ {{$pro->subtotal}} x {{$pro->cantidad}} = <span class="font-weight-medium">S/ {{$pro->total}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="container py-5 mb-lg-3">
                <div class="row justify-content-center pt-lg-4 text-center">
                    <div class="col-sm-9">
                        <h1 class="display-404">404</h1>
                        <h2 class="h3 mb-4">Este Pedido, ya no se encuentra como estado pendiente</h2>
                        <p class="font-size-md mb-4"><a href="{{route('admin.pedidos.pendientes')}}">Regresar</a></p>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function(){
            // alert("Hola");
            $('#id-costo-envio').keyup(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).change(function(e) {
                console.log(e.target.value);
                if(e.target.value == ''){
                    $('#id-nuevo-total').val($('#id-total').val());
                }else{
                    let costo_envio = parseFloat(e.target.value.replace(',',''));
                    let total = parseFloat($('#id-total').val().replace(',',''));
                    let nuevoTotal = costo_envio + total;
                    $('#id-nuevo-total').val(nuevoTotal.toFixed(2));
                }
            }).focusout(function(e) {
                console.log("Focus Out:", e);
            });


            $('#id-btn-pedido-pendiente').click(function (e) {
                var costoEnvio = parseFloat($('#id-costo-envio').val()).toFixed(2);
                var idPedido = $(this).attr('data');
                console.log(idPedido);
                var pedido = (idPedido +"").padStart(8,"0");

                Swal.fire({
                    icon:'question',
                    title: `Pedido PED-${pedido}`,
                    html: `¿Deseas asignar <span class="text-primary font-weight-bold font-size-xl"><em>S/ ${costoEnvio}</em></span>, como costo de envío para este Pedido?`,
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar Costo de Envío',
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return fetch(`/admin/pedido-pendiente/actualizar-costoenvio/${idPedido}/${costoEnvio}`)
                            .then(response => {
                                console.log("Return Fetch " , response);
                                if (!response) {
                                    console.log("Return Fetch Error");
                                    throw new Error(response)
                                } else {
                                    console.log("ELSE", response);
                                    if (response.url.substring( response.url.lastIndexOf('/'), response.url.length ) == '/login') {
                                        throw new Error("Su sesión ha caducado, necesita loguearse");
                                    } else {
                                        console.log("Then");
                                        return response.json();
                                    }
                                }
                            })
                            .catch(error => {
                                console.log("Errorcito ", error);
                                Swal.showValidationMessage (
                                    `${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado Fetch :", result);
                    if (result.isConfirmed) {
                        Swal.fire({
                            icon:'success',
                            title: `Pedido #PED-${pedido}`,
                            html: `El costo de envío actualizado fue enviado al correo de ${result.value.nombres.toUpperCase()}:
                                    <a href="" style="color:blue">${result.value.correo}</a>`,
                        }).then(res => {
                            window.location.href = '/admin/pedidos-pendientes'
                        });
                    }
                });
            });
        });
    </script>

@endsection

