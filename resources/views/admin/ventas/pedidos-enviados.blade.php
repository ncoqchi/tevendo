@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <!-- Page Content-->
        <div class="pb-5 mb-2 px-3">
            <!-- Toolbar-->
            <div class="d-flex flex-wrap justify-content-center align-items-center py-3">
                <h4 class="mr-2 text-center font-weight-medium"><i class="czi-delivery font-weight-medium"></i><em> Pedidos Enviados - En Dirección al Cliente</em></h4>
            </div>
            <div class="mb-2">
                <div>
                    <span class="text-info font-weight-medium"><em>El administrador debe confirmar que el pedido le ha llegado satisfactoriamente al cliente</em></span>
                </div>
            </div>
            <div class="d-flex flex-nowrap justify-content-between mb-1">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('admin.pedidos.enviados')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="id-num-pedido">Buscar por Nº de pedido:</label>
                        <input id="id-num-pedido" type="text" class="form-control" placeholder="Ingrese n° de pedido" name="num_pedido"
                               value="{{request()->get('num_pedido')}}">
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('admin.pedidos.enviados')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>
            <!-- Orders list-->
            <div class="table-responsive font-size-md text-nowrap">
                <table class="table table-hover table-striped mb-0">
                    <thead>
                    <tr>
                        <th>Pedido</th>
                        <th>Estado</th>
                        <th>Cliente</th>
                        <th>Celular</th>
                        {{--                                <th>Correo</th>--}}
                        <th>Fecha Modificación</th>
                        <th>Tipo de Envío</th>

                        <th style="text-align: right">Cto.Envío</th>
                        <th style="text-align: right">Sub Total</th>
                        <th style="text-align: right">Dscto</th>
                        <th style="text-align: right">TOTAL</th>
                        <th style="text-align: center">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recibos as $recibo)
                        <tr>
                            <td class="py-2">
                                <a class="text-dark font-weight-medium font-size-sm" href="#"
                                   onclick="verModal('{{$recibo->id}}')"
                                   data-toggle="modal">PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</a>
                            </td>
                            <td class="py-2">
                                <span class="badge badge-success m-0 font-size-md">Enviado</span>
                            </td>
                            <td class="">{{ucwords($recibo->envio->nombres)}}</td>
                            <td><em class="">{{$recibo->envio->celular}}</em></td>
                            {{--                                    <td><u style="color:blue">{{$recibo->envio->correo_electronico}}</u></td>--}}
                            <td class="py-2">{{date_format($recibo->created_at, 'd-m-Y H:i:s')}}</td>
                            <td class="py-2">
                                @if($recibo->tipoenvio == 1)
                                    <span class="text-success font-weight-medium"><i class="czi-dollar font-weight-medium"></i> Contra Entrega</span>
                                @else
                                    <span class="text-primary font-weight-medium"><i class="czi-card font-weight-medium"></i> Depósito en Cuenta</span>
                                @endif
                            </td>

                            <td class="py-2" style="text-align: right">S/{{number_format($recibo->costoenvio, 2, '.', ',')}}</td>
                            <td class="py-2 font-weight-medium" style="text-align: right">S/ {{number_format($recibo->total, 2, '.', ',')}}</td>
                            <td class="py-2 text-primary font-weight-medium" style="text-align: right">-S/{{number_format($recibo->descuento, 2, '.', ',')}}</td>
                            <td class="py-2 text-info font-weight-medium" style="text-align: right">S/{{number_format($recibo->total_final, 2, '.', ',')}}</td>
                            <td>
                                <button data="{{$recibo->id}}" cliente="{{$recibo->envio->nombres}}"
                                        correo="{{$recibo->envio->correo_electronico}}"
                                        style="padding: 3px 8px" class="id-btn-confirmar-envio btn btn-primary">
                                    Confirmar Entrega
                                </button>

                                <button data="{{$recibo->id}}" cliente="{{$recibo->envio->nombres}}"
                                        correo="{{$recibo->envio->correo_electronico}}"
                                        style="padding: 3px 8px" class="id-btn-cancelar-pedido btn btn-dark">
                                    Cancelar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="pb-4">
            <!-- Pagination-->
            <div class="paginador">
                {{$recibos->appends(request()->query())->links()}}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.id-btn-confirmar-envio').click(function (e) {
                var idPedido = $(this).attr('data');
                var cliente = $(this).attr('cliente');
                var correo = $(this).attr('correo');
                console.log(idPedido);
                var pedido = (idPedido + "").padStart(8, "0");
                Swal.fire({
                    icon: 'question',
                    title: `Pedido PED-${pedido}`,
                    html: `¿Deseas confirmar que el pedido de <span class="font-weight-bold">${cliente}</span> se entregó satisfactoriamente?
                                    <div>Se le notificará al usuario al correo: <a class="d-block" href="" style="color:blue">${correo}</a></div>`,
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar Entrega',
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return fetch(`/admin/pedidos-enviados/actualizar/${idPedido}`)
                            .then(response => {
                                console.log("Ajax ", response);
                                if (!response) {
                                    throw new Error(response)
                                }
                                return response.json();
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado :", result);
                    if (result.isConfirmed) {
                        Swal.fire({
                            icon: 'success',
                            title: `Pedido PED-${pedido}`,
                            html: `Se ha confirmado la Entrega del pedido de <span style="color: red">${result.value.nombres.toUpperCase()}</span>, se envió una copia a:
                                    <a href="" style="color:blue">${result.value.correo}</a>`,
                            // text: `El pedido de ${result.value.nombres.toUpperCase()} con celular:${result.value.celular},  ha sido enviado a: ${result.value.correo}`
                            // imageUrl: result
                        }).then(res => {
                            document.location.reload();
                        });
                    }
                });
            });
            $('.id-btn-cancelar-pedido').click(function (e) {
                var idPedido = $(this).attr('data');
                var cliente = $(this).attr('cliente');
                var correo = $(this).attr('correo');

                console.log(idPedido);
                var pedido = (idPedido + "").padStart(8, "0");
                Swal.fire({
                    icon: 'question',
                    title: `Pedido PED-${pedido}`,
                    input: 'text',
                    inputPlaceholder: 'Ingrese un Motivo',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    html: `¿Deseas <span class="text-primary font-weight-bold">CANCELAR</span> el pedido de <span class="font-weight-bold">${cliente}</span>?
                           <div>Se enviará una notificación a:<a class="d-block" href="" style="color:blue">${correo}</a></div>`,
                    showCancelButton: true,
                    confirmButtonText: 'Cancelar Pedido',
                    cancelButtonText: 'Salir',
                    cancelButtonColor: '#d33',
                    showLoaderOnConfirm: true,
                    preConfirm: (motivo) => {
                        return fetch(`/admin/pedidos/cancelar/${idPedido}/${motivo}`)
                            .then(response => {
                                console.log("Ajax ", response);
                                if (!response) {
                                    throw new Error(response)
                                }
                                return response.json();
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log("Resultado :", result);

                    if (result.isConfirmed) {
                        Swal.fire({
                            icon: 'success',
                            title: `Pedido PED-${pedido}`,
                            html: `Se <span class="text-primary font-weight-bold">CANCELÓ</span>
                                    el pedido de ${result.value.nombres.toUpperCase()}, se envió una correo a
                                    <a href="" style="color:blue">${result.value.correo}</a>`
                        }).then(res => {
                            document.location.reload();
                        });
                    }
                });
            });
        });
    </script>
@endsection
