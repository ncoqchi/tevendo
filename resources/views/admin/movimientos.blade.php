@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3">
            <div class="text-center pt-3 pb-1">
                <h4 class="py-2 mr-2 mb-0 font-weight-bold"><em>(KARDEX) Movimientos de INGRESO y SALIDA del Producto: <span class="text-primary">{{$producto->nombre}}</span> </em></h4>
            </div>
            <div class="bg-secondary mb-2 p-2">
                <div>
                    <h5>Datos del Producto</h5>
                </div>
                <div class="d-flex flex-nowrap justify-content-between">
                    <div class="w-50">
                        <p class="font-size-md mb-0">Stock Actual: <span class="text-primary font-weight-bold font-size-lg">{{$producto->stock}}</span></p>
                        <p class="font-size-md mb-0">Nombre: <span class="font-weight-bold">{{$producto->nombre}}</span></p>
                        <p class="font-size-md mb-0">Categoría: <span class="font-weight-bold">{{$producto->categoria->nombre}}</span></p>
{{--                        <p class="font-size-md mb-0">Descripción: <span class="">{{$producto->descripcion}}</span></p>--}}
                        <p class="font-size-md mb-0">Precio: <span class="text-primary font-weight-bold">S/ {{number_format($producto->precio,2, '.', ',')}}</span></p>
                        <p class="font-size-md mb-0">Precio Rebaja: <span class="text-primary font-weight-bold">S/ {{number_format($producto->precioRebaja,2, '.', ',')}}</span></p>
                    </div>

                    <div class="w-50 text-right">
                        <img src="{{asset($producto->imagen1)}}" width="110px" alt="">
                    </div>
                </div>
            </div>

            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="bg-secondary mb-2 p-2">
                <h5 class="text-accent font-weight-medium">Registre un movimiento de Ingreso o Salida del Producto</h5>
                <form id="if-form-productos" method="POST" action="{{route('admin.producto.registrar.movimiento', $producto->id)}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="">Tipo:</label>
                            <select required class="custom-select font-weight-bold" id="unp-category" name="tipo">
                                <option value="1">INGRESO</option>
                                <option value="0">SALIDA</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label for="">Cantidad a Ingresar o Salir:</label>
                            <input required type="number" min="0" class="form-control" name="cantidad" value="1" placeholder="Ingrese una Cantidad">
                        </div>
                        <div class="col-sm-6">
                            <label for="">Comentario Opcional:</label>
                            <input type="text" class="form-control" name="comentarios" value="" placeholder="Ingrese un comentario">
                        </div>
                        <div class="col-sm-2 mt-4">
                            <button class="btn btn-primary btn-shadow" type="submit">Registrar Movimiento</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="table-responsive mb-3 text-nowrap mt-2">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Ingreso/Salida</th>
                        <th>Cantidad</th>
                        <th>Fecha Movimiento</th>
                        <th>Comentarios</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($movimientos as $movimiento)
                        <tr>
                            <td class="text-accent font-weight-bold">
                                @if($movimiento->ingreso_salida == 1)
                                    <span class="text-success">INGRESO</span>
                                @else
                                    <span class="text-primary">SALIDA</span>
                                @endif
                            </td>
                            <td>{{$movimiento->cantidad}}</td>
                            <td>{{$movimiento->created_at->format('d-m-Y H:i:s A')}}</td>
                            <td>{{$movimiento->comentario}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="d-flex justify-content-sm-center">
{{--                    {{$productos->appends(request()->query())->links()}}--}}
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
