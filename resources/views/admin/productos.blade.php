@extends('layouts.admin')
@section('seccion')
    <section class="mb-3">
        <div class="pb-5 mb-2 px-3">
            <div class="text-center pb-2 pt-3">
                <h3 class="mr-2 font-weight-bold"><em>Lista de Productos</em></h3>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
            </div>
            <form id="if-form-productos" action="{{route('admin.productos')}}">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <input type="text" class="form-control" name="nombre" value="{{request()->get('nombre')}}"
                               placeholder="Ingrese un nombre de producto">
                    </div>
                    <div class="col-sm-6 form-group">
                        <select class="custom-select" id="unp-category" name="categoria"
                                onchange="event.preventDefault();document.getElementById('if-form-productos').submit();">
                            <option value="">- Todas Las Categorías -</option>
                            @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}"
                                        @if (request()->get('categoria') == $categoria->id) selected @endif>
                                    {{$categoria->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
            <div class="table-responsive mb-3 text-nowrap">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="text-right">#</th>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th class="text-right">Stock</th>
                        <th class="text-right">P. Normal</th>
                        <th class="text-right">P. Rebaja</th>
                        <th>Categoría</th>
                        <th>Ultima Modificación</th>
                        <th class="text-center">Acciones</th>
                        <th class="text-center">Stock</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($productos as $producto)
                        <tr>
                            <td class="text-right"><strong>{{$productos->firstItem() + $loop->index}}</strong></td>
                            <td>{{$producto->nombre}}</td>
                            <td><img width="50px" src="{{asset($producto->imagen1)}}" alt=""></td>
                            <td class="text-right">{{$producto->stock}}</td>
                            <td class="text-right font-weight-medium text-accent">S/ {{number_format($producto->precio,2, '.', ',')}}</td>
                            <td class="text-right font-weight-medium text-primary">S/ {{number_format($producto->precioRebaja,2, '.', ',')}}</td>
                            <td>{{$producto->categoria->nombre}}</td>
                            @if($producto->updated_at == null)
                                <td>-</td>
                            @else
                                <td>{{$producto->updated_at->format('d-m-Y H:i:s A')}}</td>
                            @endif
                            <td class="text-center">
                                <a class="btn btn-info btn-sm"
                                   href="{{route('admin.editar.producto', $producto->id)}}"><i class="czi-edit"></i> Editar</a>
                                <form class="d-inline" method="POST"
                                      action="{{route('admin.eliminar.producto', $producto->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-primary btn-sm ml-2" type="submit">
                                        <i class="czi-trash"></i> Eliminar
                                    </button>
                                </form>
                            </td>
                            <td class="text-center">
                                <a class="btn btn-success btn-sm"
                                   href="{{route('admin.producto.movimientos', $producto->id)}}"><i class="czi-arrow-right-circle"></i> Actualizar Movimientos</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="d-flex justify-content-sm-center">
                    {{$productos->appends(request()->query())->links()}}
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
