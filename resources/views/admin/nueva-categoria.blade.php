@extends('layouts.admin')
@section('seccion')
    <section class="mb-3 px-3 px-sm-5 px-lg-10">
        <div class="mb-2">
            <div class="text-center pt-3 pb-1">
{{--                style="text-shadow: 1px 2px 3px #969bf5;"--}}
                <h3 class="py-2 mr-2 font-weight-bold" ><em>Nueva Categoría</em></h3>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            </div>

            <form action="{{ route('admin.registrar.categoria')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group pb-2">
                    <label class="font-weight-medium" for="unp-product-name">Nombre:</label>
                    <input class="form-control mb-2" required type="text" id="unp-product-name" name="nombre" placeholder="Ingrese un Nombre para esta Categoría">
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-medium">Foto:</label>
                    <div class="mt-2">
                        <span class="alert alert-info">Dimesiones de Imagen aceptable: 600px X 450px</span>
                    </div>
                    <div class="cz-file-drop-area form-group mt-4">
                        <div class="cz-file-drop-icon czi-cloud-upload"></div>
                        <span class="cz-file-drop-message">Arrastra y suelta una imagen para esta categoría</span>
                        <input class="cz-file-drop-input" required type="file" name="foto">
                        <button class="cz-file-drop-btn btn btn-info btn-sm mb-2" type="button">Seleccionar Foto</button>
                    </div>
                </div>

                <div class="text-center mt-2" >
                    <button class="btn btn-info" type="submit"><i class="czi-check font-size-lg mr-2"></i>Guardar</button>
                    <a class="btn btn-danger" href="{{route('admin.categorias')}}" ><i class="czi-navigation font-size-lg mr-2"></i>Ver Categorías</a>
                </div>
            </form>

        </div>
    </section>

@endsection
