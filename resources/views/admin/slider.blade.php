@extends('layouts.admin')
@section('seccion')
    <section class="col-lg-9 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 pl-lg-0 pr-xl-5">
            <!-- Title-->
            <div class="d-sm-flex flex-wrap justify-content-center pb-2">
                <h2 class="py-2 mr-2 text-accent text-center text-sm-left font-weight-bold"><em>Editar Slider</em></h2>
            </div>
            <div>
                @if(session('mensaje'))
                    <div class="alert alert-success alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon czi-check-circle"></i></div>
                        {{session('mensaje')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <form class="mb-4" action="/admin/sliders/{{$slide->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="font-weight-medium" for="unp-category">Enlazar a una/un:</label>
                    <div class="custom-control custom-radio custom-control-inline ml-2">
                        <input class="custom-control-input" type="radio" id="ex-radio-1" value="1" name="radio"  {{ ($slide->categoria_id != 0) ? 'checked' : ""}}>
                        <label class="custom-control-label" for="ex-radio-1">Categoria</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline mb-2">
                        <input class="custom-control-input" type="radio" id="ex-radio-2" value="2" name="radio" {{ ($slide->producto_id != 0 )? 'checked' : ""}}>
                        <label class="custom-control-label" for="ex-radio-2">Producto</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium" for="unp-category">Seleccione una Categoria:</label>
                        <select class="custom-select mr-2 @error('categoria') is-invalid @enderror" id="unp-category" name="categoria">
                            <option value="">- SELECCIONAR -</option>
                            @foreach($categorias as $categoria)
{{--                                @if ( old('categoria') == $categoria->id )--}}
                                <option value="{{$categoria->id}}" @if( old('categoria', $slide->categoria_id) == $categoria->id ) selected @endif>
                                    {{$categoria->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium" for="unp-producto">Seleccione un Producto:</label>
                        <select class="custom-select mr-2 @error('producto') is-invalid @enderror" id="unp-producto" name="producto">
                            <option value="">- SELECCIONAR -</option>
                            @foreach($productos as $producto)
{{--                                @if (old('producto') == $producto->id)--}}
                                    <option value="{{$producto->id}}" @if(old('producto',$slide->producto_id) == $producto->id) selected @endif>{{$producto->nombre}}</option>
{{--                                @else--}}
{{--                                    <option value="{{$producto->id}}">{{$producto->nombre}}</option>--}}
{{--                                @endif--}}
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium">Imagen Para PC Actual:</label>
                        <div class="img-thumbnail rounded">
                            <img src="{{asset($slide->imagen)}}" alt="">
                        </div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium">Imagen Para PC Nueva:</label>
                        <div class="cz-file-drop-area">
                            <div class="cz-file-drop-icon czi-cloud-upload"></div>
                            <span class="cz-file-drop-message">Arrastra y Suelta una imagen para Actualizar</span>
                            <input class="cz-file-drop-input" type="file" name="imagenParaPc" >
                            <button class="cz-file-drop-btn btn btn-success btn-sm mb-2"  type="button">Seleccionar Archivo</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium">Imagen Para Celular Actual:</label>
                        <div class="img-thumbnail rounded">
                            <img src="{{asset($slide->imagen2)}}" alt="">
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-weight-medium">Imagen Para Celular Nueva:</label>
                        <div class="cz-file-drop-area">
                            <div class="cz-file-drop-icon czi-cloud-upload"></div>
                            <span class="cz-file-drop-message">Arrastra y Suelta una imagen para Actualizar</span>
                            <input class="cz-file-drop-input" type="file" name="imagenParaCelular" >
                            <button class="cz-file-drop-btn btn btn-success btn-sm mb-2"  type="button">Seleccionar Archivo</button>
                        </div>
                    </div>
                </div>

                <div class="text-center" >
                    <button class="btn btn-success" type="submit">
                        <i class="czi-check font-size-lg mr-2"></i>Actualizar
                    </button>
                    <a class="btn btn-primary ml-2" href="/admin/sliders"><i class="czi-navigation font-size-lg mr-2"></i>Ver Sliders</a>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('#ex-radio-1').click(function () {
                // $('#unp-category').removeAttr('disabled');
                // $('#unp-producto').attr('disabled',true);
                $('#unp-producto').val('');
            });

            $('#ex-radio-2').click(function () {
                // $('#unp-producto').removeAttr('disabled');
                // $('#unp-category').attr('disabled',true);
                $('#unp-category').val('');
            });

            $('.eliminar-slider').click(function (e) {
                // console.log("DATA ", $(this).attr('data'));
                let idSlide = $(this).attr('data');
                Swal.fire({
                    title: 'Slider',
                    text: `¿Deseas eliminar el Slider?`,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    console.log("Result :", result);
                    if (result.value) {
                        data = { _method: 'delete' , "_token": "{{ csrf_token() }}"};
                        url = '/admin/sliders/'+idSlide;
                        request = $.post(url, data);
                        request.done(function(res){
                            console.log("RESPUESTA ", res);
                            Swal.fire({
                                icon: 'info',
                                title: 'Slider',
                                text: 'Se eliminó el Slider con exito!'
                            }).then((result) =>{
                                location.reload();
                            });
                        });


                    }
                });
            });

        });
    </script>

@endsection
