@extends('layouts.home')
@section('contenido')
    <section class="container pt-4 mb-2 mb-sm-5">
        <div style="width: 100%;" class="p-1 p-sm-3">
            <h2 style="text-shadow: 0px 0px 10px rgba(0,0,0,0.3); font-family: 'Roboto', Sans-serif; font-weight: 600;">
                <span style="justify-content: center; order: initial; display: flex; box-sizing: border-box;margin: 0;border: 0;vertical-align: baseline; font-size: 100%;">
                    <span style="width: 120px;height: 5px;background-color: red;box-sizing: border-box;"></span>
                </span>
                <div class="text-center text-dark mt-2">
                    <span>¡Nuestros Productos!</span>
                </div>
            </h2>
        </div>
        @include('include.producto')
        <div class="d-flex justify-content-sm-center">
            {{$productos->appends(request()->query())->links()}}
        </div>
    </section>
@endsection
@section('scripting')
    {{--    <script src="{{asset('js/app.js')}}"></script>--}}
@endsection
