@extends('layouts.home')

@section('contenido')
    <!-- Page Title (Shop)-->
    <div class="page-title-overlap pt-4" style="background-image: radial-gradient(circle, #ff6734, #ff582c, #fe4724, #fe311e, #fd0019);">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-start">
                        <li class="breadcrumb-item">
                            <a class="text-nowrap" href="{{route('home')}}">
                                <i class="czi-home"></i>
                                Principal</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap">
                            <a href="{{route('productos.list')}}">Tienda</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">
                            Producto
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0">{{$producto->nombre}}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container">
        <!-- Gallery + details-->
        <div class="bg-light box-shadow-lg rounded-lg px-4 py-3 mb-5">
            <div class="px-lg-3">
                <div class="row">
                    <!-- Product gallery-->
                    <div class="col-lg-7 pr-lg-0 pt-lg-4">
                        <div class="cz-product-gallery">
                            <div class="cz-preview order-sm-2">
                                <div class="cz-preview-item active" id="first">
                                    <img class="cz-image-zoom" src="{{asset($producto->imagen)}}" data-zoom="{{asset($producto->imagen)}}"
                                         alt="{{$producto->nombre}}">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="second">
                                    <img class="cz-image-zoom" src="{{asset($producto->imagen)}}" data-zoom="{{asset($producto->imagen)}}"
                                         alt="{{$producto->nombre}}">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="third">
                                    <img class="cz-image-zoom" src="{{asset('img/shop/single/gallery/03.jpg')}}" data-zoom="{{asset('img/shop/single/gallery/03.jpg')}}"
                                         alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="fourth">
                                    <img class="cz-image-zoom" src="{{asset('img/shop/single/gallery/04.jpg')}}" data-zoom="{{asset('img/shop/single/gallery/04.jpg')}}"
                                         alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                            </div>
                            <div class="cz-thumblist order-sm-1">
                                <a class="cz-thumblist-item active" href="#first">
                                    <img src="{{asset($producto->imagen)}}" alt="Imagen de Producto">
                                </a>
                                <a class="cz-thumblist-item" href="#second">
                                    <img src="{{asset($producto->imagen)}}" alt="Imagen de Producto">
                                </a>
                                <a class="cz-thumblist-item" href="#third">
                                    <img src="{{asset('img/shop/single/gallery/03.jpg')}}" alt="Imagen de Producto">
                                </a>
                                <a class="cz-thumblist-item" href="#fourth">
                                    <img src="{{asset($producto->imagen)}}" alt="Imagen de Producto">
                                </a>
                                <a class="cz-thumblist-item video-item" href="https://www.youtube.com/watch?v=1vrXpMLLK14">
                                    <div class="cz-thumblist-item-text">
                                        <i class="czi-video"></i>Video
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Product details-->
                    <div class="col-lg-5 pt-4 pt-lg-0">
                        <div class="product-details ml-auto pb-3">
                            <div class="d-flex justify-content-between align-items-center mb-2"><a href="#reviews" data-scroll>
                                    <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                    </div><span class="d-inline-block font-size-sm text-body align-middle mt-1 ml-1">74 Comentarios</span></a>
                                <button class="btn-wishlist mr-0 mr-lg-n3" type="button" data-toggle="tooltip" title=""><i class="czi-heart"></i></button>
                            </div>
                            <div class="mb-3">
                                <span class="h3 font-weight-normal mr-1">
                                    S/. {{number_format($producto->precioRebaja, 2, '.', ',')}}
                                </span>
                            </div>
                            <div class="mb-3">
                                <del class="text-muted font-size-lg mr-3">S/. {{ number_format($producto->precio, 2, '.', ',') }}</del>
                                <span class="badge badge-shadow align-middle mt-n2"
                                      style="background-color: red;color: white">-{{round((($producto->precio - $producto->precioRebaja)  / $producto->precio)*100,0)}}%
                                </span>
                            </div>

                            <form class="mb-grid-gutter" method="post">
                                <div class="form-group d-flex align-items-center">
                                    <select class="custom-select mr-3" style="width: 5rem;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <button class="btn btn-shadow btn-block" style="background-color: red;color: white" type="submit">
                                        <i class="czi-cart font-size-lg mr-2"></i>Agregar al Carrito</button>
                                </div>
                            </form>
                            <!-- Product panels-->
                            <div class="accordion mb-4" id="productPanels">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="accordion-heading">
                                            <a href="#productInfo" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="productInfo">
                                                <i class="czi-announcement text-muted font-size-lg align-middle mt-n1 mr-2"></i>
                                                Detalle del Producto<span class="accordion-indicator"><i data-feather="chevron-up"></i></span></a></h3>
                                    </div>
                                    <div class="collapse show" id="productInfo" data-parent="#productPanels">
                                        <div class="card-body">
                                            {{$producto->descripcion}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sharing-->
                            <h6 class="d-inline-block align-middle font-size-base my-2">Compartir</h6>

                            <a class="share-btn sb-instagram my-2" href="#">
                                <i class="czi-instagram"></i>Instagram
                            </a>
                            <a class="share-btn sb-facebook my-2" href="#">
                                <i class="czi-facebook"></i>Facebook
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product description section 2-->
    </div>
    <!-- Reviews-->
    <div class="border-top border-bottom my-lg-3 py-5">
        <div class="container pt-md-2" id="reviews">
            <div class="row pb-3">
                <div class="col-lg-4 col-md-5">
                    <h2 class="h3 mb-4">74 Comentarios</h2>
                    <div class="star-rating mr-2"><i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                        <i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                        <i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                        <i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                        <i class="czi-star font-size-sm text-muted mr-1"></i>
                    </div>
                    <span class="d-inline-block align-middle">4.1 Calificación General</span>
                    <p class="pt-3 font-size-sm text-muted">58 de 74 (77%)<br>Los clientes recomendaron este producto</p>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">5</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">43</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">4</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 27%; background-color: #a7e453;" aria-valuenow="27" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">16</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">3</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 17%; background-color: #ffda75;" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">9</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">2</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 9%; background-color: #fea569;" aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">4</span>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">1</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">2</span>
                    </div>
                </div>
            </div>
            <hr class="mt-4 pb-4 mb-3">
            <div class="row">
                <!-- Reviews list-->
                <div class="col-md-7">
                    <div class="d-flex justify-content-end pb-4">
                        <div class="form-inline flex-nowrap">
                            <label class="text-muted text-nowrap mr-2 d-none d-sm-block" for="sort-reviews">Ordenar Por:</label>
                            <select class="custom-select custom-select-sm" id="sort-reviews">
                                <option>La Más Nueva</option>
                                <option>La Más Antigua</option>
                                <option>Popular</option>
                                <option>Alta Calificación</option>
                                <option>Baja Calificación</option>
                            </select>
                        </div>
                    </div>
                    <!-- Review-->
                    <div class="product-review pb-4 mb-4 border-bottom">
                        <div class="d-flex mb-3">
                            <div class="media media-ie-fix align-items-center mr-4 pr-2"><img class="rounded-circle" width="50" src="{{asset('img/shop/reviews/01.jpg')}}" alt="Rafael Marquez"/>
                                <div class="media-body pl-3">
                                    <h6 class="font-size-sm mb-0">Rafael Marquez</h6><span class="font-size-ms text-muted">June 28, 2019</span>
                                </div>
                            </div>
                            <div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                </div>
                            </div>
                        </div>
                        <p class="font-size-md mb-2">
                            Para su tiempo libre, y son independientes para nosotros, lo más importante es lo que es muy agradable en su opción de elegir
                            cuándo están habilitados para no hacer nada para detenerlo, si no fuera así, todo placer es ser ...
                        </p>
                        <div class="text-nowrap">
                            <button class="btn-like" type="button">15</button>
                            <button class="btn-dislike" type="button">3</button>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-outline-accent" type="button">
                            <i class="czi-reload mr-2"></i>Cargar más comentarios</button>
                    </div>
                </div>
                <!-- Leave review form-->
                <div class="col-md-5 mt-2 pt-4 mt-md-0 pt-md-0">
                    <div class="bg-secondary py-grid-gutter px-grid-gutter rounded-lg">
                        <h3 class="h4 pb-2">Escribe un comentario</h3>
                        <form class="needs-validation" method="post" novalidate>
                            <div class="form-group">
                                <label for="review-rating">Calificación<span class="text-danger">*</span></label>
                                <select class="custom-select" required id="review-rating">
                                    <option value="">Elegir una calificación</option>
                                    <option value="5">5 Estrellas</option>
                                    <option value="4">4 Estrellas</option>
                                    <option value="3">3 Estrellas</option>
                                    <option value="2">2 Estrellas</option>
                                    <option value="1">1 Estrellas</option>
                                </select>
                                <div class="invalid-feedback">Por favor elija calificación!</div>
                            </div>
                            <div class="form-group">
                                <label for="review-text">Comentario<span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="6" required id="review-text"></textarea>
                                <div class="invalid-feedback">Please write a review!</div>
                                <small class="form-text text-muted">Su opinión debe tener al menos 100 caracteres.</small>
                            </div>
                            <button class="btn btn-shadow btn-block" style="background-color: red;color: white" type="submit">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
