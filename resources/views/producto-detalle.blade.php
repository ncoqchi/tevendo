@extends('layouts.home')
@section("head")
    <link rel="image_src" href="{{asset($producto['imagen1'])}}">
    <meta name="og:site_name" content="TeVendo.PE">
    <meta name="og:locale" content="es_ES">
    <meta name="og:url" content="http://tevendo.perumas.pe/producto/{{$producto->id}}" />
    <meta name="og:type" content="website" />
    <meta name="og:title" content="Producto {{$producto->nombre}}" />
    <meta name="og:description" content="{!!$producto->descripcion!!}" />
    <meta name="og:image" content="{{asset($producto['imagen1'])}}" />
@endsection

@section('contenido')
    <!-- Page Title (Shop)-->
    <div class="page-title-overlap pt-2" style="background-color: white">
        <div class="container d-lg-flex justify-content-between py-0 py-sm-1">
            <div class="order-lg-2 mb-1 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-dark flex-lg-nowrap justify-content-center justify-content-lg-start">
                        <li class="breadcrumb-item">
                            <a class="text-nowrap" href="{{route('home')}}">
                                <i class="czi-home"></i>Principal
                            </a>
                        </li>
                        <li class="breadcrumb-item text-nowrap">
                            <a href="{{route('tienda')}}"><i class="czi-upload"></i>Tienda</a>
                        </li>
                        <li class="breadcrumb-item font-weight-medium text-primary" aria-current="page">
                            <i class="czi-camera"></i> Producto
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left my-1">
                <span class="h3 font-weight-bold text-accent mb-0"><em>{{$producto->nombre}}</em></span>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container">
        <!-- Gallery + details-->
        <div class="bg-light box-shadow-lg rounded-lg px-3 py-2 mb-3">
            <div class="px-lg-2">
                <div class="row">
                    <!-- Product gallery-->
                    <div class="col-lg-6 pr-lg-0 pt-lg-1">
                        <div class="cz-product-gallery">

                            <div class="cz-preview order-sm-2">
                                @for ($i = 1; $i <= 6; $i++)
                                    @if($i == 1)
                                        <div class="cz-preview-item active" id="id_imagen_{{$i}}">
                                            <img loading="lazy" class="cz-image-zoom" src="{{asset($producto['imagen'.$i])}}" data-zoom="{{asset($producto['imagen'.$i])}}" alt="Producto imagen">
                                            <div class="cz-image-zoom-pane"></div>
                                        </div>
                                    @else
                                        @if($producto['imagen'.$i] != '/productos/no-image-product.png')
                                            <div class="cz-preview-item" id="id_imagen_{{$i}}">
                                                <img loading="lazy" class="cz-image-zoom" src="{{asset($producto['imagen'.$i])}}" data-zoom="{{asset($producto['imagen'.$i])}}" alt="Producto imagen">
                                                <div class="cz-image-zoom-pane"></div>
                                            </div>
                                        @endif
                                    @endif
                                @endfor
                            </div>

                            <div class="cz-thumblist order-sm-1">
                                @for ($i = 1; $i <= 6; $i++)
                                    @if($i == 1)
                                        <a class="cz-thumblist-item active" href="#id_imagen_{{$i}}">
                                            <img loading="lazy" src="{{ asset($producto['imagen'.$i]) }}" alt="Product thumb">
                                        </a>
                                    @else
                                        @if($producto['imagen'.$i] != '/productos/no-image-product.png')
                                            <a class="cz-thumblist-item" href="#id_imagen_{{$i}}">
                                                <img loading="lazy" src="{{ asset($producto['imagen'.$i]) }}" alt="Product thumb">
                                            </a>
                                        @endif
                                    @endif
                                @endfor
                            </div>
                        </div>
                    </div>
                    <!-- Product details-->
                    <div class="col-lg-6 pt-3 pt-lg-0">
                        <div class="p-3">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <a href="#reviews" data-scroll>
                                    <div class="star-rating mr-2">
                                        @for($i = 1; $i <= 5; $i++)
                                            @if($i > $producto->rating )
                                                <i class="czi-star font-size-sm text-muted mr-1"></i>
                                            @else
                                                <i class="czi-star-filled font-size-sm text-warning mr-1"></i>
                                            @endif
                                        @endfor
                                    </div>
                                    <span class="d-inline-block font-size-sm text-body align-middle mt-1 ml-1">{{$calificaciones['total']}} Comentarios</span>
                                </a>
                                @if($producto->precioRebaja < $producto->precio)
                                    <span class="badge badge-shadow align-middle mt-0" style="background-color: red;color: white">
                                        -{{round((($producto->precio - $producto->precioRebaja)  / $producto->precio)*100,0)}}%
                                    </span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <span class="h3 font-weight-normal mr-1 text-primary">
                                    S/ {{number_format($producto->precioRebaja, 2, '.', ',')}}
                                </span>
                                @if($producto->precioRebaja < $producto->precio)
                                    <del class="text-dark font-size-lg mr-3">S/ {{ number_format($producto->precio, 2, '.', ',') }}</del>
                                @endif
                            </div>
                            <div class="form-inline mb-grid-gutter" method="post">
                                <div class="form-group d-flex align-items-center">
                                    <select class="custom-select mr-3" style="width: 5rem;" id="id-select-cantidad">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    <button class="btn btn-shadow btn-accent" id="id-btn-add-cart" onclick="agregarProducto({{$producto}})">
                                        <i class="czi-cart font-size-lg mr-2"></i>Agregar al Carrito</button>
                                </div>
                            </div>
                            <!-- Product panels-->
                            <div class="accordion mb-4" id="productPanels">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="accordion-heading">
                                            <a href="#productInfo" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="productInfo">
                                                <i class="czi-announcement text-muted font-size-lg align-middle mt-n1 mr-2"></i>
                                                Detalle del Producto<span class="accordion-indicator"><i data-feather="chevron-up"></i></span></a></h3>
                                    </div>
                                    <div class="collapse show" id="productInfo" data-parent="#productPanels">
                                        <div class="py-2 px-3">
                                            {!!$producto->descripcion!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sharing-->
                            <div class="fb-share-button" data-href="http://tevendo.perumas.pe/producto/{{$producto->id}}" data-layout="button_count"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product description section 2-->
    </div>

    <!-- Reviews-->
    <div class="my-lg-3 py-2">
        <div class="container pt-md-2" id="reviews">
            <div class="row pb-3">
                <div class="col-lg-4 col-md-5">
                    <h3 class="h3 mb-3">{{$calificaciones['total']}} Comentarios</h3>
                    <div class="star-rating mr-2">
                        @for($i = 1; $i <= 5; $i++)
                            @if($i > $producto->rating )
                                <i class="czi-star font-size-sm text-muted mr-1"></i>
                            @else
                                <i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                            @endif
                        @endfor
                    </div>
                    <span class="d-inline-block align-middle">{{$producto->rating}} Calificación General</span>
                    <p class="pt-3 font-size-sm text-muted">({{$calificaciones['aprobadas']}} de {{$calificaciones['total']}}) {{$calificaciones['porcentaje']}}% de los clientes que compraron este producto lo recomiendan</p>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3">
                            <span class="d-inline-block align-middle text-muted">5</span><i class="czi-star-filled font-size-xs ml-1"></i>
                        </div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: {{$calificaciones['por_cinco']}}%;" aria-valuenow="{{$calificaciones['por_cinco']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <span class="text-muted ml-3">{{$calificaciones['cinco']}}</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3">
                            <span class="d-inline-block align-middle text-muted">4</span><i class="czi-star-filled font-size-xs ml-1"></i>
                        </div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: {{$calificaciones['por_cuatro']}}%; background-color: #a7e453;" aria-valuenow="{{$calificaciones['por_cuatro']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">{{$calificaciones['cuatro']}}</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">3</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: {{$calificaciones['por_tres']}}%; background-color: #ffda75;" aria-valuenow="{{$calificaciones['por_tres']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">{{$calificaciones['tres']}}</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">2</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: {{$calificaciones['por_dos']}}%; background-color: #fea569;" aria-valuenow="{{$calificaciones['por_dos']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">{{$calificaciones['dos']}}</span>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">1</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: {{$calificaciones['por_una']}}%;" aria-valuenow="{{$calificaciones['por_una']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">{{$calificaciones['una']}}</span>
                    </div>
                </div>
            </div>
            <hr class="mt-4 pb-4 mb-3">
            <div class="row px-1">
                <!-- Reviews list-->
                <div class="col-md-7">
                    <div class="d-flex justify-content-end pb-4">
                        <form id="id_form_ordenar_comentarios" class="form-inline flex-nowrap" action="{{route('producto.detalle', $producto->id)}}">
                            <label class="text-muted text-nowrap mr-2 d-none d-block" for="sort-reviews">Ordenar Por:</label>
                            <select id="sort-reviews" class="custom-select custom-select-sm"
                                    onchange="event.preventDefault();document.getElementById('id_form_ordenar_comentarios').submit();"
                                    name="ordenar_comentarios">
                                <option value="mas_nueva" @if(request()->get('ordenar_comentarios') == 'mas_nueva') selected @endif>La Más Nueva</option>
                                <option value="mas_antigua" @if(request()->get('ordenar_comentarios') == 'mas_antigua') selected @endif>La Más Antigua</option>
                                <option value="alta_calificacion" @if(request()->get('ordenar_comentarios') == 'alta_calificacion') selected @endif>Alta Calificación</option>
                                <option value="baja_calificacion" @if(request()->get('ordenar_comentarios') == 'baja_calificacion') selected @endif>Baja Calificación</option>
                            </select>
                        </form>
                    </div>
                    @if(sizeof($comentarios)==0)
                        <div class="product-review pb-4 mb-4 border-bottom">
                            <em>Sé el primero en comentar sobre este producto</em>
                        </div>
                    @endif
                    <!-- Comentarios -->
                    @foreach($comentarios as $comentario)
                        <div class="product-review pb-4 mb-4 border-bottom">
                            <div class="d-flex mb-3">
                                <div class="media media-ie-fix align-items-center mr-4">
                                    <img class="rounded-circle" width="50" src="{{asset($comentario->user->avatar)}}"
                                         alt="{{$comentario->user->name}}"/>
                                    <div class="media-body pl-2">
                                        <h6 class="font-size-sm mb-0">{{ $comentario->user->name}}</h6>
                                        <span class="font-size-ms text-muted">{{$comentario->created_at->format('d-m-Y H:i:s A')}}</span>
                                    </div>
                                </div>
                                <div>
                                    <div class="star-rating">
                                        @for($i = 1; $i <= 5; $i++)
                                            @if($i > $comentario->calificacion )
                                                <i class="sr-star czi-star-filled"></i>
                                            @else
                                                <i class="sr-star czi-star-filled active"></i>
                                            @endif
                                        @endfor
                                    </div>
                                </div>
                            </div>
                            <p class="font-size-md mb-2">
                                {{$comentario->texto}}
                            </p>
                            <div class="text-nowrap">
                                <button class="class-like btn-like @if($comentario->liked == 'like') font-weight-bold @endif" data2="like" data="{{$comentario->id}}" type="button">
                                    {{$comentario->like}}
                                </button>
                                <button class="class-like btn-dislike @if($comentario->liked == 'unlike') font-weight-bold @endif" data2="unlike" data="{{$comentario->id}}" type="button">
                                    {{$comentario->notlike}}
                                </button>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center">
                        <a class="btn btn-accent" type="button" href="{{route('producto.detalle', $producto->id)}}">
                            <i class="czi-reload mr-2"></i>Cargar más comentarios</a>
                    </div>
                </div>
                <!-- Leave review form-->
                <div class="col-md-5 mt-2 pt-4 mt-md-0 pt-md-0">
                    <div class="bg-secondary py-grid-gutter px-grid-gutter rounded-lg" style="padding: 15px !important;">
                        <h3 class="h4 pb-2">Escribe un comentario</h3>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="needs-validation" method="post" action="/api/comentarios">
                            @csrf
                            <div class="form-group">
                                <label for="review-rating">Calificación<span class="text-danger">*</span></label>
                                <select class="custom-select  @error('calificacion') is-invalid @enderror" required id="review-rating" name="calificacion" >
                                    <option value="">- Elegir una calificación -</option>
                                    <option value="5">5 Estrellas</option>
                                    <option value="4">4 Estrellas</option>
                                    <option value="3">3 Estrellas</option>
                                    <option value="2">2 Estrellas</option>
                                    <option value="1">1 Estrella</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id-text-comentario">Comentario<span class="text-danger">*</span></label>
                                <textarea id="id-text-comentario"  class="form-control  @error('comentario') is-invalid @enderror" rows="6" required name="comentario" maxlength="200"></textarea>
                                <small id="id-mensaje-comentario" class="form-text text-muted">Le quedan 190 caracteres</small>
                            </div>
                            <div>
                                <input type="hidden" name="producto_id" value="{{$producto->id}}">
                            </div>
                            <button class="btn btn-shadow btn-primary btn-block" type="submit">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('otros')


{{--<script src="/js/app_290820200648am.js"></script>--}}

<script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>

        $(function() {
            console.log("Hola ");
            $('#id-text-comentario').bind('input propertychange', function() {
                // $("#yourBtnID").hide();
                if(this.value.length) {
                    // console.log("asdasd");
                    // $("#yourBtnID").show();
                    var texto = this.value;
                    var tamanioactual = 190 - texto.length;
                    if(tamanioactual < 0){
                        // alert("Llego al limite para su mensaje");
                        $('#id-text-comentario').val(texto.substring(0,texto.length-1));
                  }else{
                        $('#id-mensaje-comentario').text(`Le quedan ${tamanioactual} caracteres.`);
                    }
                }
            });

            $(".class-like").click(function (e) {
                console.log("Evento ", e);
                // console.log("$(this).html()  ",$(this).html());
                console.log("$(this) ",$(this));
                // $(this).addClass( "tilin" );
                // $(this).html('ji');
                var aClassLike = $(this);
                var idComentario = $(this).attr('data');
                var likeunlike = $(this).attr('data2');

                // $(this).setAttribute('data', $(this).html());
                var sendData = {
                    "_token": "{{ csrf_token() }}" ,
                    comentario_id : idComentario ,
                    liked : likeunlike
                };

                console.log("DATA 2", sendData);
                // return;

                $.post("/api/liked", sendData ,
                    function(data) {
                        // console.log("Data ", data);
                    }).done(function(res) {

                        console.log("Done ", res);
                        //Se agrega el like nuevo, si en caso todo va bien
                        if(res.code==2 || res.code == 3) {
                            $(aClassLike).html(res.new_like);
                            $(aClassLike).addClass( "font-weight-bold" )
                        } else if(res.code==1) {
                            alert(res.mensaje);
                        }

                    }).fail(function(e) {
                        console.log("Error ", e);
                        if(e.status == 401){
                            alert('Debe iniciar sesion para dar LIKE a un comentario');
                        }
                    });
            });


            // $('#id-btn-add-cart').click(function(){
            //     var cantidad = $('#id-select-cantidad').val();
            //     agregarCarrito();
            // });

        });

        function agregarProducto(producto){

            // alert($('#id-select-cantidad').val());
            var cantidad = parseInt($('#id-select-cantidad').val());
            // producto.categoria = {
            //     id: producto.categoria_id,
            //     nombre : nameCategoria
            // }
            agregarCarrito(producto , cantidad);
        }
    </script>


@endsection
