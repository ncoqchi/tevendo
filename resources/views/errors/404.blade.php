@extends('errors.layout')
{{--@section('title', __('Forbidden'))--}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card box-shadow" style="border-radius: 1em">
                    <div class="card-header bg-info text-light text-center p-3">
                        <i class="czi-close-circle" style="font-size: 4em"></i>
                    </div>
                    <div class="card-body tab-content py-4 text-center">
                        <h3 class="font-weight-bold">404</h3>
                        <h5>Recurso no Encontrado</h5>
                        <div style="padding: 0.5em 6em">
                            <p>{{$exception->getMessage()}}</p>
                        </div>
                        <div>
                            <a class="btn btn-info font-weight-medium" href="{{route('home')}}"><i class="czi-home"></i> Ir a Pantalla Principal</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
{{--@section('code', '403')--}}
{{--@section('message', __($exception->getMessage() ?: 'Forbidden'))--}}


