@extends('errors.layout')
{{--@section('title', __('Forbidden'))--}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card box-shadow" style="border-radius: 1em">
                    <div class="card-header bg-dark text-light text-center p-3">
                        <i class="czi-security-prohibition" style="font-size: 4em"></i>
                    </div>
                    <div class="card-body tab-content py-4 text-center">
                        <h3 class="font-weight-bold">Error 419</h3>
                        <h5>Pagina Expirada</h5>
                        <div style="padding: 0.5em 6em">
                            <p>{{$exception->getMessage()}}</p>
                        </div>
                        <div>
                            <a class="btn btn-primary font-weight-medium" href="{{route('home')}}">Ir a Pantalla Principal</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
{{--@section('code', '403')--}}
{{--@section('message', __($exception->getMessage() ?: 'Forbidden'))--}}


