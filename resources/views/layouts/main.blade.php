<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{config('app.name', 'TeVendo.pe')}}</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Cartzilla - Bootstrap E-commerce Template">
    <meta name="keywords" content="bootstrap, shop, e-commerce, market, modern,
     responsive,  business, mobile, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
    <meta name="author" content="Createx Studio">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#fe6a6a" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{asset('css/vendor.min.css')}}">
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" id="main-styles" href="{{ asset('css/theme2.min.css') }}">

</head>
<!-- Body-->
<body class="toolbar-enabled">

<!-- Sign in / sign up modal-->
<div class="modal fade" id="signin-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="nav nav-tabs card-header-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" href="#signin-tab" data-toggle="tab" role="tab"
                                            aria-selected="true"><i class="czi-unlocked mr-2 mt-n1"></i>Iniciar
                            Sesion</a></li>
                    <li class="nav-item"><a class="nav-link" href="#signup-tab" data-toggle="tab" role="tab"
                                            aria-selected="false"><i class="czi-user mr-2 mt-n1"></i>Cerrar Sesion</a>
                    </li>
                </ul>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tab-content py-4">
                <form class="needs-validation tab-pane fade show active" autocomplete="off" novalidate id="signin-tab">
                    <div class="form-group">
                        <label for="si-email">Email address</label>
                        <input class="form-control" type="email" id="si-email" placeholder="johndoe@example.com"
                               required>
                        <div class="invalid-feedback">Please provide a valid email address.</div>
                    </div>
                    <div class="form-group">
                        <label for="si-password">Password</label>
                        <div class="password-toggle">
                            <input class="form-control" type="password" id="si-password" required>
                            <label class="password-toggle-btn">
                                <input class="custom-control-input" type="checkbox"><i
                                    class="czi-eye password-toggle-indicator"></i><span
                                    class="sr-only">Show password</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group d-flex flex-wrap justify-content-between">
                        <div class="custom-control custom-checkbox mb-2">
                            <input class="custom-control-input" type="checkbox" id="si-remember">
                            <label class="custom-control-label" for="si-remember">Remember me</label>
                        </div>
                        <a class="font-size-sm" href="#">Forgot password?</a>
                    </div>
                    <button class="btn btn-primary btn-block btn-shadow" type="submit">Sign in</button>
                </form>
                <form class="needs-validation tab-pane fade" autocomplete="off" novalidate id="signup-tab">
                    <div class="form-group">
                        <label for="su-name">Full name</label>
                        <input class="form-control" type="text" id="su-name" placeholder="John Doe" required>
                        <div class="invalid-feedback">Please fill in your name.</div>
                    </div>
                    <div class="form-group">
                        <label for="su-email">Email address</label>
                        <input class="form-control" type="email" id="su-email" placeholder="johndoe@example.com"
                               required>
                        <div class="invalid-feedback">Please provide a valid email address.</div>
                    </div>
                    <div class="form-group">
                        <label for="su-password">Password</label>
                        <div class="password-toggle">
                            <input class="form-control" type="password" id="su-password" required>
                            <label class="password-toggle-btn">
                                <input class="custom-control-input" type="checkbox"><i
                                    class="czi-eye password-toggle-indicator"></i><span
                                    class="sr-only">Show password</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="su-password-confirm">Confirm password</label>
                        <div class="password-toggle">
                            <input class="form-control" type="password" id="su-password-confirm" required>
                            <label class="password-toggle-btn">
                                <input class="custom-control-input" type="checkbox"><i
                                    class="czi-eye password-toggle-indicator"></i><span
                                    class="sr-only">Show password</span>
                            </label>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block btn-shadow" type="submit">Sign up</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Quick View Modal-->
<div class="modal-quick-view modal fade" id="quick-view" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title product-title"><a href="shop-single-v1.html" data-toggle="tooltip"
                                                         data-placement="right" title="Go to product page">Sports Hooded
                        Sweatshirt<i class="czi-arrow-right font-size-lg ml-2"></i></a></h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Product gallery-->
                    <div class="col-lg-7 pr-lg-0">
                        <div class="cz-product-gallery">
                            <div class="cz-preview order-sm-2">
                                <div class="cz-preview-item active" id="first"><img class="cz-image-zoom"
                                                                                    src="img/shop/single/gallery/01.jpg"
                                                                                    data-zoom="img/shop/single/gallery/01.jpg"
                                                                                    alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="second"><img class="cz-image-zoom"
                                                                              src="img/shop/single/gallery/02.jpg"
                                                                              data-zoom="img/shop/single/gallery/02.jpg"
                                                                              alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="third"><img class="cz-image-zoom"
                                                                             src="img/shop/single/gallery/03.jpg"
                                                                             data-zoom="img/shop/single/gallery/03.jpg"
                                                                             alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="fourth"><img class="cz-image-zoom"
                                                                              src="img/shop/single/gallery/04.jpg"
                                                                              data-zoom="img/shop/single/gallery/04.jpg"
                                                                              alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                            </div>
                            <div class="cz-thumblist order-sm-1"><a class="cz-thumblist-item active" href="#first"><img
                                        src="img/shop/single/gallery/th01.jpg" alt="Product thumb"></a><a
                                    class="cz-thumblist-item" href="#second"><img src="img/shop/single/gallery/th02.jpg"
                                                                                  alt="Product thumb"></a><a
                                    class="cz-thumblist-item" href="#third"><img src="img/shop/single/gallery/th03.jpg"
                                                                                 alt="Product thumb"></a><a
                                    class="cz-thumblist-item" href="#fourth"><img src="img/shop/single/gallery/th04.jpg"
                                                                                  alt="Product thumb"></a></div>
                        </div>
                    </div>
                    <!-- Product details-->
                    <div class="col-lg-5 pt-4 pt-lg-0 cz-image-zoom-pane">
                        <div class="product-details ml-auto pb-3">
                            <div class="d-flex justify-content-between align-items-center mb-2"><a
                                    href="shop-single-v1.html#reviews">
                                    <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                    </div>
                                    <span class="d-inline-block font-size-sm text-body align-middle mt-1 ml-1">74 Reviews</span></a>
                                <button class="btn-wishlist" type="button" data-toggle="tooltip"
                                        title="Add to wishlist"><i class="czi-heart"></i></button>
                            </div>
                            <div class="mb-3"><span class="h3 font-weight-normal text-accent mr-1">$18.<small>99</small></span>
                                <del class="text-muted font-size-lg mr-3">$25.<small>00</small></del>
                                <span class="badge badge-danger badge-shadow align-middle mt-n2">Sale</span>
                            </div>
                            <div class="font-size-sm mb-4"><span
                                    class="text-heading font-weight-medium mr-1">Color:</span><span class="text-muted">Red/Dark blue/White</span>
                            </div>
                            <div class="position-relative mr-n4 mb-3">
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color1" checked>
                                    <label class="custom-option-label rounded-circle" for="color1"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url(img/shop/single/color-opt-1.png)"></span></label>
                                </div>
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color2">
                                    <label class="custom-option-label rounded-circle" for="color2"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url(img/shop/single/color-opt-2.png)"></span></label>
                                </div>
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color3">
                                    <label class="custom-option-label rounded-circle" for="color3"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url(img/shop/single/color-opt-3.png)"></span></label>
                                </div>
                                <div class="product-badge product-available mt-n1"><i class="czi-security-check"></i>Product
                                    available
                                </div>
                            </div>
                            <form class="mb-grid-gutter">
                                <div class="form-group">
                                    <label class="font-weight-medium pb-1" for="product-size">Size:</label>
                                    <select class="custom-select" required id="product-size">
                                        <option value="">Select size</option>
                                        <option value="xs">XS</option>
                                        <option value="s">S</option>
                                        <option value="m">M</option>
                                        <option value="l">L</option>
                                        <option value="xl">XL</option>
                                    </select>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <select class="custom-select mr-3" style="width: 5rem;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <button class="btn btn-primary btn-shadow btn-block" type="submit"><i
                                            class="czi-cart font-size-lg mr-2"></i>Add to Cart
                                    </button>
                                </div>
                            </form>
                            <h5 class="h6 mb-3 pb-2 border-bottom"><i
                                    class="czi-announcement text-muted font-size-lg align-middle mt-n1 mr-2"></i>Product
                                info</h5>
                            <h6 class="font-size-sm mb-2">Style</h6>
                            <ul class="font-size-sm pl-4">
                                <li>Hooded top</li>
                            </ul>
                            <h6 class="font-size-sm mb-2">Composition</h6>
                            <ul class="font-size-sm pl-4">
                                <li>Elastic rib: Cotton 95%, Elastane 5%</li>
                                <li>Lining: Cotton 100%</li>
                                <li>Cotton 80%, Polyester 20%</li>
                            </ul>
                            <h6 class="font-size-sm mb-2">Art. No.</h6>
                            <ul class="font-size-sm pl-4 mb-0">
                                <li>183260098</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Navbar 3 Level (Light)-->
<header class="box-shadow-sm">
    <!-- Topbar-->
    <div class="topbar topbar-dark bg-dark">
        <div class="container">
            <div class="topbar-text dropdown d-md-none"><a class="topbar-link dropdown-toggle" href="#"
                                                           data-toggle="dropdown">Links</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="tel:00331697720"><i class="czi-support text-muted mr-2"></i>999999999</a>
                    </li>
                    <li><a class="dropdown-item" href="order-tracking.html"><i class="czi-location text-muted mr-2"></i>Order
                            tracking</a></li>
                </ul>
            </div>
            <div class="topbar-text text-nowrap d-none d-md-inline-block"><i class="czi-support"></i><span
                    class="text-muted mr-1">Contacto</span><a class="topbar-link" href="tel:00331697720">999999</a>
            </div>

        </div>
    </div>


    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
    <div class="navbar-sticky bg-light">
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="container"><a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0" href="index.html"
                                      style="min-width: 7rem;"><img width="142" src="img/logo-dark.png"
                                                                    alt="Cartzilla"/></a><a
                    class="navbar-brand d-sm-none mr-2" href="index.html" style="min-width: 4.625rem;"><img width="74"
                                                                                                            src="img/logo-icon.png"
                                                                                                            alt="Cartzilla"/></a>
                <div class="input-group-overlay d-none d-lg-flex mx-4">
                    <input class="form-control appended-form-control" type="text" placeholder="Buscar en Productos">
                    <div class="input-group-append-overlay"><span class="input-group-text"><i
                                class="czi-search"></i></span></div>
                </div>
                <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span></button>
                    <a class="navbar-tool navbar-stuck-toggler" href="#"><span
                            class="navbar-tool-tooltip">Expand menu</span>
                        <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-menu"></i></div>
                    </a><a class="navbar-tool d-none d-lg-flex" href="account-wishlist.html"><span
                            class="navbar-tool-tooltip">Wishlist</span>
                        <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-heart"></i></div>
                    </a><a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="#signin-modal" data-toggle="modal">
                        <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-user"></i></div>
                        <div class="navbar-tool-text ml-n3"><small>Hola,{{ Auth::user()->name }}</small>Mi Cuenta</div>
                    </a>
                    <div class="navbar-tool dropdown ml-3">
                        <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="shop-cart.html">
                            <span id="carrito_count" class="navbar-tool-label"></span>
                            <i class="navbar-tool-icon czi-cart"></i>
                        </a>
                        <a class="navbar-tool-text" href="shop-cart.html"><small>Mi Carrito</small>S/. <span id="carrito_total"></span></a>
                        <!-- Cart dropdown-->
                        <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
                            <div class="widget widget-cart px-3 pt-2 pb-3">
                                <div style="height: 15rem;" data-simplebar data-simplebar-auto-hide="false">
                                    <div id="carrito">

                                    </div>
                                    <div class="d-flex flex-wrap justify-content-between align-items-center py-3">
                                        <div class="font-size-sm mr-2 py-2">
                                            <span class="text-muted">Subtotal: S/.</span>
                                            <span id="carrito_subtotal" class="text-accent font-size-base ml-1">

                                            </span>
                                        </div>
                                        <a class="btn btn-outline-secondary btn-sm" href="shop-cart.html">
                                            Ver Carrito<i class="czi-arrow-right ml-1 mr-n1"></i>
                                        </a>
                                    </div>
                                    <a class="btn btn-primary btn-sm btn-block" href="checkout-details.html">
                                        <i class="czi-card mr-2 font-size-base align-middle"></i>Comprar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-expand-lg navbar-light navbar-stuck-menu mt-n2 pt-0 pb-2">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Search-->
                    <div class="input-group-overlay d-lg-none my-3">
                        <div class="input-group-prepend-overlay">
                            <span class="input-group-text">
                                <i class="czi-search"></i>
                            </span>
                        </div>
                        <input class="form-control prepended-form-control" type="text" placeholder="Search for products">
                    </div>
                    <!-- Departments menu-->
                    <ul class="navbar-nav mega-nav pr-lg-2 mr-lg-2">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pl-0" href="#"
                               data-toggle="dropdown"><i class="czi-view-grid mr-2"></i>Categorías</a>
                            <div class="dropdown-menu px-2 pl-0 pb-4">
                                <div class="d-flex flex-wrap flex-md-nowrap">
                                    <div class="mega-dropdown-column pt-4 px-3">
                                        <div class="widget widget-links">
                                            <a class="d-block overflow-hidden rounded-lg mb-3" href="#">
                                                <img src="img/shop/departments/01.jpg" alt="Shoes"/>
                                            </a>
                                            <h6 class="font-size-base mb-2">Clothing</h6>
                                            <ul class="widget-list">
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Women's clothing</a>
                                                </li>
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Men's clothing</a>
                                                </li>
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Kid's clothing</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap flex-md-nowrap">
                                    <div class="mega-dropdown-column pt-4 px-3">
                                        <div class="widget widget-links">
                                            <a class="d-block overflow-hidden rounded-lg mb-3" href="#">
                                                <img src="img/shop/departments/04.jpg" alt="Shoes"/>
                                            </a>
                                            <h6 class="font-size-base mb-2">Furniture &amp; Decor</h6>
                                            <ul class="widget-list">
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Home furniture</a></li>
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Office furniture</a></li>
                                                <li class="widget-list-item">
                                                    <a class="widget-list-link" href="#">Lighting and decoration</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- Primary menu-->
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="/" data-toggle="dropdown">Principal</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown position-static mb-0">
                                    <a class="dropdown-item py-2 border-bottom" href="home-fashion-store-v1.html">
                                        <span class="d-block text-heading">Home</span>
                                        <small class="d-block text-muted">CompraAcaPe</small>
                                    </a>
                                    <div class="dropdown-menu h-100 animation-0 mt-0 p-3">
                                        <a class="d-block" href="home-fashion-store-v1.html" style="width: 250px;"><img
                                                src="img/home/preview/th01.jpg" alt="Fashion Store v.1"/></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Tienda</a>
                            <div class="dropdown-menu p-0">
                                <div class="d-flex flex-wrap flex-md-nowrap px-2">
                                    <div class="mega-dropdown-column py-4 px-3">
                                        <div class="widget widget-links mb-3">
                                            <h6 class="font-size-base mb-3">Productos</h6>
                                            <ul class="widget-list">
                                                <li class="widget-list-item pb-1">
                                                    <a class="widget-list-link" href="shop-grid-ls.html">Los más Vendidos</a>
                                                </li>
                                                <li class="widget-list-item pb-1">
                                                    <a class="widget-list-link" href="shop-grid-ls.html">Categorías Destacadas</a>
                                                </li>
                                                <li class="widget-list-item pb-1">
                                                    <a class="widget-list-link" href="shop-grid-rs.html">Todos los Productos</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Mi Cuenta</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Cuenta de Usuario</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="account-orders.html">Mi Pedido Actual</a></li>
                                        <li><a class="dropdown-item" href="account-profile.html">Mi Historial</a></li>
                                        <li><a class="dropdown-item" href="account-address.html">Mi Información Personal</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Products grid (Trending products)-->

@yield('contenido')


<!-- Blog + Instagram info cards-->
<section class="container-fluid px-0">
    <div class="row no-gutters">
        <div class="col-md-6">
            <a class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-primary"
               href="blog-list-sidebar.html">
                <div class="card-body text-center">
                    <i class="czi-edit h3 mt-2 mb-4 text-primary"></i>
                    <h3 class="h5 mb-1">Lee Nuestro Blog</h3>
                    <p class="text-muted font-size-sm">Últimos productos Importados</p>
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-accent">
                <div class="card-body text-center">
                    <i class="czi-instagram h3 mt-2 mb-4 text-accent"></i>
                    <h3 class="h5 mb-1">Sigueme en Instagram</h3>
                    <p class="text-muted font-size-sm">#MiPuntoDeVenta</p>
                </div>
            </a>
        </div>
    </div>
</section>



<footer class="bg-dark pt-5">
    <div class="container">
        <div class="row pb-2">
            <div class="col-md-4">
                <div class="widget pb-2 mb-4">
                    <h3 class="widget-title text-light pb-1">Mantente Informado</h3>
                    <form class="validate"
                          action="https://studio.us12.list-manage.com/subscribe/post-json?u=c7103e2c981361a6639545bd5&amp;amp;id=29ca296126&amp;c=?"
                          method="get" name="mc-embedded-subscribe-form" id="mc-embedded-subscribe-form">
                        <div class="input-group input-group-overlay flex-nowrap">
                            <div class="input-group-prepend-overlay">
                                <span class="input-group-text text-muted font-size-base"><i class="czi-mail"></i></span>
                            </div>
                            <input class="form-control prepended-form-control" type="email" name="EMAIL" id="mce-EMAIL"
                                   value="" placeholder="Tu correo" required>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" name="subscribe"
                                        id="mc-embedded-subscribe">Suscribete*
                                </button>
                            </div>
                        </div>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input type="text" name="b_c7103e2c981361a6639545bd5_29ca296126" tabindex="-1">
                        </div>
                        <small class="form-text text-light opacity-50" id="mc-helper">*Suscríbase a nuestro boletín para
                            recibir ofertas de descuentos anticipados, actualizaciones e información sobre nuevos
                            productos.</small>
                        <div class="subscribe-status"></div>
                    </form>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row pb-2">
                    <div class="col-md-6 text-center text-md-right mb-4">
                        <div class="mb-3">
                            <a class="social-btn sb-light sb-twitter ml-2 mb-2" href="#">
                                <i class="czi-twitter"></i>
                            </a>
                            <a class="social-btn sb-light sb-facebook ml-2 mb-2" href="#">
                                <i class="czi-facebook"></i>
                            </a>
                            <a class="social-btn sb-light sb-instagram ml-2 mb-2" href="#">
                                <i class="czi-instagram"></i>
                            </a>
                            <a class="social-btn sb-light sb-pinterest ml-2 mb-2" href="#">
                                <i class="czi-pinterest"></i>
                            </a>
                            <a class="social-btn sb-light sb-youtube ml-2 mb-2" href="#">
                                <i class="czi-youtube"></i>
                            </a>
                        </div>
                        <img class="d-inline-block" width="187" src="img/cards-alt.png" alt="Payment methods"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="pt-3 bg-darker">
        <div class="container">
            <div class="pb-2 font-size-xs text-light opacity-50 text-center text-md-left">© Todos Los Derechos
                Reservados
                <a class="text-light" href="https://createx.studio/" target="_blank" rel="noopener">NelsonSystem</a>
            </div>
        </div>
    </div>
</footer>
<!-- Toolbar for handheld devices-->
<div class="cz-handheld-toolbar">
    <div class="d-table table-fixed w-100">
        <a class="d-table-cell cz-handheld-toolbar-item" href="account-wishlist.html">
            <span class="cz-handheld-toolbar-icon"><i class="czi-heart"></i></span>
            <span class="cz-handheld-toolbar-label">Lista Deseos</span></a>
        <a class="d-table-cell cz-handheld-toolbar-item" href="#navbarCollapse" data-toggle="collapse"
           onclick="window.scrollTo(0, 0)">
            <span class="cz-handheld-toolbar-icon">
                <i class="czi-menu"></i></span>
            <span class="cz-handheld-toolbar-label">Menu</span>
        </a>
        <a class="d-table-cell cz-handheld-toolbar-item" href="shop-cart.html">
            <span class="cz-handheld-toolbar-icon">
                <i class="czi-cart"></i>
                <span class="badge badge-primary badge-pill ml-1">4</span>
            </span>
            <span class="cz-handheld-toolbar-label">$265.00</span>
        </a>
    </div>
</div>

<!-- Back To Top Button-->
<a class="btn-scroll-top" href="#top" data-scroll>
    <span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Arriba</span>
    <i class="btn-scroll-top-icon czi-arrow-up"> </i>
</a>

<script src="{{asset('js/vendor.min.js')}}"></script>
<script src="{{asset('js/theme.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/underscore/underscore-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/underscore/json2.js')}}"></script>
@yield('scripting')
</body>

</html>
