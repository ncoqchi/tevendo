<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>{{config('app.name', 'Tevendo.pe')}} - Admin</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Administrador TeVendo.pe">
    <meta name="keywords" content="Comercio Electrónico, Importación, Por Mayor y Menor,Ventas, Perumas">
    <meta name="author" content="Nelson Coqchi Apaza - nelson.coqchi@gmail.com">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
{{--    <link rel="manifest" href="site.webmanifest">--}}
    <link rel="mask-icon" color="#fe6a6a" href="{{asset('safari-pinned-tab.svg')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{asset('css/vendor.min.css')}} ">
    <link rel="stylesheet" media="screen" href="{{asset('css/chartist.min.css')}} ">
    <link rel="stylesheet" media="screen" href="{{asset('css/prism.min.css')}} ">
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" id="main-styles" href="{{asset('css/theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('js/sweet/sweetalert2.css')}}">
</head>
<!-- Body-->
<body class="toolbar-enabled">

<header>
    <div class="navbar-sticky bg-light">
        <div class="navbar navbar-expand-lg navbar-dark py-0" style="background-color: black">
            <div class="container">
                <a class="navbar-brand mr-3 flex-shrink-0 text" href="{{route('admin.inicio')}}" style="min-width: 7rem;">
                    Administrador {{ config('app.name', 'TeVendo') }}
                </a>
                <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span></button>
                    <a class="navbar-tool navbar-stuck-toggler" href="#">
                        <span class="navbar-tool-tooltip">Expand menu</span>
                        <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-menu"></i></div>
                    </a>
                    <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="#signin-modal" data-toggle="modal">
                        <div class="navbar-tool-icon-box"><img class="rounded-circle" style="width: 35px" src="{{Auth::user()->avatar}}" alt="Createx Studio"></div>
                        <div class="navbar-tool-text ml-n3"><small>Hola, {{strtoupper(Auth::user()->name)}}</small></div></a>
                </div>
            </div>
        </div>
        <div class="navbar navbar-expand-lg navbar-dark navbar-stuck-menu py-0" style="background-color: black">
            <div class="">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Primary menu-->
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle  {{ Request::is('admin/*categoria*') ? 'text-danger font-size-lg' : '' }}" href="#" data-toggle="dropdown">
                                <i class="czi-send font-weight-bold {{ Request::is('admin/*categoria*') ? 'text-danger' : '' }}"></i> Categorías
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item {{ Request::is('admin/nueva-categoria') ? 'active' : '' }}" href="{{route('admin.nueva.categoria')}}"><i class="czi-add opacity-60 mr-2 font-weight-bold"></i>Crear</a></li>
                                <li><a class="dropdown-item {{ Request::is('admin/categorias') ? 'active' : '' }}" href="{{route('admin.categorias')}}"><i class="czi-list opacity-60 mr-2 font-weight-bold"></i>Ver Todas</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle {{ Request::is('admin/*producto*') ? 'text-danger font-size-lg' : '' }}" href="#" data-toggle="dropdown">
                                <i class="czi-camera font-weight-medium {{ Request::is('admin/*producto*') ? 'text-danger' : '' }}"></i> Productos
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/nuevo-producto') ? 'active' : '' }}"
                                       href="{{route('admin.nuevo.producto')}}"><i class="czi-add opacity-60 mr-2 font-weight-bold"></i>Crear
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/productos') ? 'active' : '' }}" href="{{route('admin.productos')}}">
                                        <i class="czi-list opacity-60 mr-2 font-weight-bold"></i>Ver Todos
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('admin/sliders') ? 'text-danger font-size-lg' : '' }}" href="/admin/sliders">
                                <i class="czi-image {{ Request::is('admin/sliders') ? 'text-danger' : '' }}"></i> Sliders
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle {{ Request::is('admin/pedidos*') ? 'text-danger font-size-lg' : '' }}" href="#" data-toggle="dropdown">
                                <i class="czi-delivery {{ Request::is('admin/pedidos*') ? 'text-danger' : '' }}"></i> Pedidos
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-pendientes') ? 'active' : '' }}" href="{{route('admin.pedidos.pendientes')}}"><i class="czi-loading opacity-60 mr-2"></i>1. Pedidos Pendientes
                                        <small class="d-block text-muted ml-4">Primero se debe registrar un costo de envío</small></a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-con-costo-envio') ? 'active' : '' }}" href="{{route('admin.pedidos.costoenvio')}}"><i class="czi-dollar opacity-60 mr-2"></i>2. Pedidos con Costo de Envío Asignado
                                        <small class="d-block text-muted ml-4">Pedidos Contra Entrega y Depósito en Cuenta</small></a></li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-depositados') ? 'active' : '' }}" href="{{route('admin.pedidos.depositados')}}"><i class="czi-card opacity-60 mr-2"></i>Pedidos con Depósito Confirmado
                                        <small class="d-block text-muted ml-4">Solo para Pedidos con Depósito en Cuenta</small></a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-enviados') ? 'active' : '' }}" href="{{route('admin.pedidos.enviados')}}"><i class="czi-send opacity-60 mr-2"></i>3. Pedidos Enviados
                                        <small class="d-block text-muted ml-4">Pedidos en Camino a Dirección del Cliente</small></a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-entregados') ? 'active' : '' }}" href="{{route('admin.pedidos.entregados')}}"><i class="czi-package opacity-60 mr-2"></i>4. Pedidos Entregados
                                        <small class="d-block text-muted ml-4">Pedidos Recogidos por el Cliente</small></a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{ Request::is('admin/pedidos-cancelados') ? 'active' : '' }}" href="{{route('admin.pedidos.cancelados')}}"><i class="czi-close opacity-60 mr-2"></i>5. Pedidos Cancelados
                                        <small class="d-block text-muted ml-4">Pedidos anulados o cancelados por el administrador</small></a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle {{ Request::is('admin/ventas*') ? 'text-danger font-size-lg' : '' }}" href="#" data-toggle="dropdown">
                                <i class="czi-dollar {{ Request::is('admin/ventas*') ? 'text-danger' : ''}}"></i> Ventas
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item {{Request::is('admin/ventas-por-anio') ? 'active' : ''}}"
                                       href="{{route('admin.ventas.poranio')}}">
                                        <i class="czi-check mr-2 font-weight-bold"></i>Ver ventas por Mes x Año
                                        <small class="d-block text-muted ml-4">Total de ingresos por Mes por Año</small>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item {{Request::is('admin/ventas-por-rangos') ? 'active' : ''}}"
                                       href="{{route('admin.ventas.porrangos')}}">
                                        <i class="czi-time mr-2 font-weight-bold"></i>Ver ventas por Rango de Fechas
                                        <small class="d-block text-muted ml-4">Total de ingresos por Rango de Fechas</small>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('admin.codigospromocionales')}}">
                                        <i class="czi-paypal mr-2 font-weight-bold"></i>Generar Código Promocional
                                        <small class="d-block text-muted ml-4">Generar Código Promocional con fecha de expiración</small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('admin/informacion*') ? 'text-danger font-size-lg' : '' }}" href="{{route('admin.informacion')}}">
                                <i class="czi-increase opacity-100 mr-2 {{ Request::is('admin/informacion') ? 'text-danger' : '' }}"></i>Información General
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="{{route('home')}}">
                                <i class="czi-home opacity-100 mr-2"></i>IR A LA TIENDA
                            </a>
                        </li>
                        <li class="nav-item">
                            <a style="cursor: pointer;" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="czi-sign-out opacity-100 mr-2"></i>Cerrar Sesión</a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header>

{{--MODAL--}}
<div class="modal fade" id="order-details">
    <div class="modal-xl modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header mb-1 px-3 py-2">
                <h5 class="modal-title font-weight-bold"><em>Pedido: &nbsp; &nbsp;<span id="modal-pedido-id" class="text-primary">PED-00</span></em></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body pt-0">
                <div class="row">
                    <div class="col-lg-4 pt-1">
                        <div class="card border-0 box-shadow p-3">
                            <div class="text-center"><h4 class="mb-3"><u>Productos</u></h4></div>
                            <div id="modal-pedido-detalle">

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 pt-1">
                        <div class="card border-0 box-shadow p-3">
                            <div class="text-center"><h4 class="mb-3"><u>Datos de Envío</u></h4></div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Cliente</div>
                                <div class="" id="id-envio-cliente"></div>
                            </div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Correo Electrónico</div>
                                <div id="id-envio-correo"></div>
                            </div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Celular</div>
                                <div id="id-envio-celular" class=""></div>
                            </div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Ubigeo</div>
                                <div id="id-envio-ubigeo" class=""></div>
                            </div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Dirección</div>
                                <div id="id-envio-direccion" class=""></div>
                            </div>

                            <div class="mb-2">
                                <div class="font-weight-medium">Referencia</div>
                                <div id="id-envio-referencia" class=""></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 pt-2">
                        <div class="card border-0 box-shadow p-3">
                            <div class="text-center">
                                <h4 class="mb-3"><u>Captura Voucher</u></h4></div>
                            <div class="img-thumbnail rounded">
                                <img id="id-imagen-voucher" class="rounded w-100">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer-->
            <div class="font-size-md bg-secondary px-3 py-3">
                <div class="row">
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Usuario:&nbsp;</span>
                        <span id="modal-pedido-registrado"></span>
                    </div>

                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Foto:&nbsp;</span>
                        <img class="rounded" width="40" id="modal-pedido-avatar" src=""/>
                    </div>
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Tipo Envío:&nbsp;</span>
                        <span id="modal-pedido-tipo-envio"></span>
                    </div>
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Estado:&nbsp;</span>
                        <span id="modal-pedido-estado" class="badge font-size-sm"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Subtotal:&nbsp;</span>
                        <span id="modal-pedido-subtotal">S/ 0.00</span>
                    </div>
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Costo Envío (18%):&nbsp;</span>
                        <span id="modal-pedido-igv">S/ 0.000</span>
                    </div>
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Descuento:&nbsp;</span>
                        <span id="modal-pedido-costoenvio">S/ 0.00</span>
                    </div>
                    <div class="col-6 col-sm-3">
                        <span class="text-dark font-weight-medium">Total:&nbsp;</span>
                        <span id="modal-pedido-total" class="font-size-lg font-weight-bold text-primary">S/ 0.00</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container mb-5 pb-3 px-0 px-sm-2">

    <div>
        @yield('seccion')
    </div>


{{--    <div class="bg-light box-shadow-lg rounded-lg overflow-hidden">--}}
{{--        <div class="row">--}}
{{--            <!-- Sidebar-->--}}
{{--            <!-- Content-->--}}
{{--            @yield('seccion')--}}
{{--        </div>--}}
{{--    </div>--}}

</div>
<!-- Footer-->


<script src="{{asset('js/vendor.min.js')}}"></script>
<script src="{{asset('js/chartist.min.js')}}"></script>
<script src="{{asset('js/prism.min.js')}}"></script>
<script src="{{asset('js/theme.min.js')}}"></script>
<script src="{{asset('js/sweet/sweetalert2.all.js')}}"></script>
@yield('scripts')


    <script>
    function verModal(id, envio) {
        console.log("ID ", id);

        // console.log("envio ", JSON.parse(envio));

        // let envioObj = JSON.parse(envio);

        $('#modal-pedido-detalle').html("Cargando información ...");
        var pedido = (id +"").padStart(8,"0");
        $('#modal-pedido-id').html('PED-' + pedido);
        $('#order-details').modal('show');
        $.get(`/api/ventas/${id}`, function(res) {
            console.log("Pedido ", res);
            $('#modal-pedido-subtotal').html('S/ '+res.recibo.subtotal.toFixed(2));
            $('#modal-pedido-igv').html('S/ '+res.recibo.costoenvio.toFixed(2));
            $('#modal-pedido-costoenvio').html('S/ '+res.recibo.descuento.toFixed(2));
            $('#modal-pedido-total').html('S/ '+res.recibo.total_final.toFixed(2));

            $('#modal-pedido-registrado').html(res.recibo.user.name.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase()));
            $('#modal-pedido-avatar').prop('src',res.recibo.user.avatar);

            if(res.recibo.estado == '1'){
                $('#modal-pedido-estado').html("Pendiente").addClass('badge-warning');
            }else if(res.recibo.estado == '2'){
                $('#modal-pedido-estado').html("Costo Envío Asignado").addClass('badge-info');
            }else if(res.recibo.estado == '3'){
                $('#modal-pedido-estado').html("Enviado").addClass('badge-success');
            }else if(res.recibo.estado == '4'){
                $('#modal-pedido-estado').html("Entregado").addClass('badge-primary');
            }else if(res.recibo.estado == '6'){
                $('#modal-pedido-estado').html("Voucher Adjuntado - Pago Confirmado").addClass('badge-accent');
                $('#id-imagen-voucher').prop('src', res.recibo.imagen_voucher);
            }else if(res.recibo.estado == '9'){
                $('#modal-pedido-estado').html("Cancelado").addClass('badge-danger');
            }

            if(res.recibo.tipoenvio == '1'){
                $('#modal-pedido-tipo-envio').html("Contra Entrega").addClass('text-success').addClass('font-weight-bold');
            }else{
                $('#modal-pedido-tipo-envio').html("Depósito En Cuenta").addClass('text-primary').addClass('font-weight-bold');
            }

            //ENVIO
            $('#id-envio-cliente').html(res.recibo.envio.nombres.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase()));
            $('#id-envio-correo').html(res.recibo.envio.correo_electronico);
            $('#id-envio-celular').html(+res.recibo.envio.celular);
            $('#id-envio-ubigeo').html(res.recibo.envio.departamento + " / " +res.recibo.envio.provincia + " / "+ res.recibo.envio.distrito);
            $('#id-envio-direccion').html(res.recibo.envio.direccion);
            $('#id-envio-referencia').html(res.recibo.envio.referencia);

            //FIN ENVIO

            $('#modal-pedido-detalle').html("No tiene detalle");

            if(res.detalle.length > 0){

                $('#modal-pedido-detalle').empty();

                res.detalle.forEach(producto => {

                    let templ_total = producto.total.toFixed(2);
                    let templ_subtotal = producto.subtotal.toFixed(2);

                    let template_item_modal = `
                            <div class="mb-1 pb-2 p-sm-1 ">
                                <div class="media d-block d-flex justify-content-between">
                                       <div class="w-25">
                                            <a class="d-inline-block mx-auto mr-sm-4" href="#">
                                                <img style="width: 80px" src="${producto.imagen}" alt="Product">
                                            </a>
                                        </div>
                                        <div class="w-75">
                                            <h3 class="product-title font-size-base mb-1">
                                             <a href="">${producto.categoria}</a>
                                            </h3>
                                            <h2 class="mb-1 font-size-sm"><a class="text-dark font-weight-normal" href="">${producto.detalle.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase())}</a></h2>
                                            <div class="font-size-xs pt-1">S/ ${templ_subtotal} X ${producto.cantidad} = <span class="text-primary font-weight-bold">S/ ${templ_total}</span></div>
                                        </div>
                                </div>
                            </div>`;

                    $('#modal-pedido-detalle').append(template_item_modal);
                });
            }
        });
    }
</script>

</body>

<!-- Mirrored from demo.createx.studio/cartzilla/dashboard-add-new-product.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Jun 2020 23:33:54 GMT -->
</html>
