<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>{{config('app.name', 'TeVendo.pe')}}</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="TeVendo.pe">
    <meta name="keywords" content="Comercio Electrónico, Importación, Por Mayor y Menor,Ventas, Perumas">
    <meta name="author" content="Nelson Coqchi Apaza - nelson.coqchi@gmail.com">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
{{--    <link rel="manifest" href="site.webmanifest">--}}
    <link rel="mask-icon" color="#fe6a6a" href="{{asset('safari-pinned-tab.svg')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{asset('css/vendor.min.css')}}">
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" id="main-styles" href="{{asset('css/theme2.min.css')}}">
    <link rel="stylesheet" href="{{asset('js/sweet/sweetalert2.css')}}">
    <link rel="stylesheet" href="{{asset('css/ncoqchi.css')}}">
    @yield("head")
</head>
<!-- Body-->
<body class="toolbar-enabled">
<div id="fb-root"></div>
<div  id="app">
<header class="mb-1">
    <div class="bg-black pt-1 px-2 text-light d-flex justify-content-between">
        <div>
            <img class="rounded-circle" style="width: 15px;" src="/iconos/whastapp.png" alt="">
            <a class="topbar-link text-light" href="tel:{{$info->whastapp}}">{{$info->whastapp}}</a>
        </div>
        <div class="">
            <a class="share-btn sb-facebook" style="color: #82aed2" target="_blank" href="{{$info->facebook}}"><i class="czi-facebook"></i>Facebook</a>
            <a class="share-btn sb-instagram" style="color: #9a96e6" target="_blank" href="{{$info->instagram}}"><i class="czi-instagram"></i>Instagram</a>
        </div>
    </div>
    <div class="navbar-sticky pt-sm-2" style="background-color : #FE0003">
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="container pb-sm-2">
                <a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0 py-0" href="{{route('home')}}" style="min-width: 1rem;">
                    <img width="50px" style="color: white" src="{{asset('img/logoplomo_main.png')}}" alt="TeVendo.pe"/>
                    <img width="100px" style="color: white" src="{{asset('img/logoplomo_nombre.png')}}" alt="TeVendo.pe"/>
                </a>
                <a class="navbar-brand d-sm-none mr-2 py-0" href="{{route('home')}}" style="min-width: 1rem;">
                    <img width="60px" style="color: white" src="{{asset('img/logoplomo_1.png')}}" alt="TeVendo.pe"/>
                </a>
                <div class="input-group-overlay d-none d-lg-flex mx-4">
                    <form action="{{route('productos.por.nombre')}}" style="width: 100%">
                        <input class="form-control appended-form-control" name="q" type="text"
                               style="border: none; border-radius: 2px"
                               placeholder="Buscar en Productos">
                        <div class="input-group-append-overlay" style="border-radius: 0; border: none">
                            <span class="input-group-text">
                                <i class="czi-search"></i>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">
                    <button class="navbar-toggler text-light" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                        <i class="navbar-tool-icon czi-menu"></i>
                    </button>
                    <a class="navbar-tool navbar-stuck-toggler" href="#">
                        <span class="navbar-tool-tooltip">Expandir Menú</span>
                        <div class="navbar-tool-icon-box">
                            <i class="navbar-tool-icon czi-menu text-light"></i>
                        </div>
                    </a>
                    <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="#" data-toggle="modal">
                        <div class="navbar-tool-icon-box text-light">
                            @if(Auth::user() != null)
                                <img class="rounded-circle" style="width: 50px" src="{{asset(Auth::user()->avatar)}}" alt="Imagen">
                            @else
                                <i class="navbar-tool-icon czi-user"></i>
                            @endif
                        </div>
                        <div class="">
                            @if(Auth::user() != null)
                                <p class="mb-0">
                                    <small><a class="text-light" href="#">&nbsp; {{strtoupper(Auth::user()->name)}}</a></small>
                                </p>
                            @else
                                <small> <a class="text-light" href="{{route('login')}}">Iniciar Sesión</a></small>
                            @endif
                        </div>
                    </a>
                    {{--CARRITO MOBILE--}}
                    <div class="navbar-tool dropdown ml-3">
                        <a class="navbar-tool-icon-box bg-primary dropdown-toggle" href="{{route('carrito')}}">
                            <span class="navbar-tool-label carrito_count" style="background-color: white;color: red"></span>
                            <i class="navbar-tool-icon czi-cart" style="color: white"></i>
                        </a>
                        <a class="navbar-tool-text text-light" href="{{route('carrito')}}">
                            <small class="text-light">Mi Carrito</small>S/. <span class="carrito_total">0.50</span>
                        </a>
                        <!-- Cart dropdown-->
                        <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
                            <div class="widget widget-cart px-3 pt-2 pb-3">

                                <div style="height: 15rem;" data-simplebar data-simplebar-auto-hide="false">
                                    <div id="carrito">
                                        {{--  CARRITO --}}
                                    </div>
                                    <div class="d-flex flex-wrap justify-content-between align-items-center py-3">
                                        <div class="font-size-sm mr-2 py-2">
                                            <span class="text-dark">TOTAL: <span class="text-accent">S/</span></span>
                                            <span class="text-accent font-size-base ml-1 carrito_total">25.999</span>
                                        </div>
                                        <span id="flotante"></span>
                                        <a class="btn btn-outline-secondary btn-sm" href="{{route("carrito")}}">Ver Carrito<i class="czi-arrow-right ml-1 mr-n1"></i></a>
                                    </div>
                                    <a class="btn btn-primary btn-sm btn-block" href="{{route("carrito")}}">
                                        <i class="czi-card mr-2 font-size-base align-middle"></i>Comprar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-expand-lg navbar-light navbar-stuck-menu pt-0" style="background-color: white">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Search-->
                    <div class="input-group-overlay d-lg-none my-3">
                        <div class="input-group-prepend-overlay">
                            <span class="input-group-text">
                                <i class="czi-search"></i>
                            </span>
                        </div>
                        <input class="form-control prepended-form-control" type="text" placeholder="Buscar en productos">
                    </div>

                    <ul class="navbar-nav navbar-light">
                        <li class="nav-item dropdown">
                            <a class="nav-link @if(Request::is('home') || Request::is('/')) font-weight-bold text-primary nc-border-b @endif" href="{{route('home')}}">
                                <i class="czi-home font-weight-bold @if(Request::is('home') || Request::is('/')) text-primary @endif "></i>&nbsp;&nbsp;Principal
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link @if(Request::is('tienda') || Request::is('producto/*')) font-weight-bold text-primary nc-border-b @endif" href="{{route('tienda')}}">
                                <i class="czi-share font-weight-bold @if(Request::is('tienda') || Request::is('producto/*')) text-primary @endif"></i>&nbsp;&nbsp;Tienda</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link @if(Request::is('ofertas')) font-weight-bold text-primary nc-border-b @endif"  href="{{route('ofertas')}}">
                                <i class="czi-dollar @if(Request::is('ofertas')) text-primary @endif font-weight-bold"></i>&nbsp;&nbsp;En Oferta</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link @if(Request::is('carrito'))) font-weight-bold text-primary nc-border-b @endif" href="{{route('carrito')}}">
                                <i class="czi-cart font-weight-bold @if(Request::is('carrito')) text-primary @endif"></i>&nbsp;&nbsp;Mi Carrito ( <span class="carrito_count"></span> )</a>
                        </li>
                        @if(Auth::user() != null)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="{{route('cuenta.perfil')}}" data-toggle="dropdown">
                                <i class="czi-user"></i>&nbsp;&nbsp;Mi Cuenta
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item @if(Request::is('mis-pedidos')) text-primary font-weight-bold @endif"
                                       href="{{route('cuenta.pedidos')}}">Mis Pedidos</a>
                                </li>
                                <li>
                                    <a class="dropdown-item @if(Request::is('mi-perfil')) text-primary font-weight-bold @endif"
                                       href="{{route('cuenta.perfil')}}">Mi Perfil</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" onclick="event.preventDefault();document.getElementById('logout-formulario').submit();">
                                <i class="czi-close"></i>&nbsp;&nbsp;Cerrar Sesión
                            </a>
                            <form id="logout-formulario" action="{{route('logout')}}" method="POST" style="display:none;">
                                @csrF
                            </form>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

@yield("prev-contenido")

@yield("contenido")

<footer class="pt-5 mt-5 d-none d-sm-block" style="background-color: black; text-align: center">
    <div class="container  pb-3">

            <a class="social-btn sb-light sb-facebook ml-2 mb-2" href="#" style="color: #3b5998; background-color: white">
                <i class="czi-facebook"></i>
            </a>
            <a class="social-btn sb-light sb-google ml-2 mb-2" href="#" style="color: #ea4335; background-color: white">
                <i class="czi-google"></i>
            </a>
            <a class="social-btn sb-light sb-instagram ml-2 mb-2" href="#" style="color: #5851db; background-color: white">
                <i class="czi-instagram"></i>
            </a>
            <a class="social-btn sb-light sb-youtube ml-2 mb-2" href="#" style="color: #ff0000; background-color: white">
                <i class="czi-youtube"></i>
            </a>
    </div>
    <div class="pt-1" style="background-color: black">
        <div class="container">
            <div class="pb-2 font-size-xs text-light opacity-50">© Todos Los Derechos Reservados
                <a class="text-light" href="" target="_blank" rel="noopener">TeVendo.pe</a>
            </div>
        </div>
    </div>
</footer>
<!-- Toolbar for handheld devices-->
<div class="cz-handheld-toolbar" style="background-image: radial-gradient(circle, #ff6734, #ff582c, #fe4724, #fe311e, #fd0019);">
    <div class="d-table table-fixed w-100">
        <a class="d-table-cell cz-handheld-toolbar-item" href="#navbarCollapse" data-toggle="collapse"
           onclick="window.scrollTo(0, 0)">
            <span class="cz-handheld-toolbar-icon text-light">
                <i class="czi-menu"></i>
            </span>
            <span class="cz-handheld-toolbar-label text-light">Menu</span>
        </a>
        <a class="d-table-cell cz-handheld-toolbar-item"  href="{{route('home')}}">
            <span class="cz-handheld-toolbar-icon text-light"><i class="czi-home"></i></span>
            <span class="cz-handheld-toolbar-label text-light" >Principal</span>
        </a>
        @if(Request::is('tienda'))
            <a class="d-table-cell cz-handheld-toolbar-item" href="#shop-sidebar" data-toggle="sidebar">
              <span class="cz-handheld-toolbar-icon text-light"><i class="czi-filter-alt"></i></span>
                <span class="cz-handheld-toolbar-label text-light">Filtros</span>
            </a>
        @endif
        <a class="d-table-cell cz-handheld-toolbar-item"  href="{{route('tienda')}}">
            <span class="cz-handheld-toolbar-icon text-light"><i class="czi-navigation"></i></span>
            <span class="cz-handheld-toolbar-label text-light" >Tienda</span>
        </a>
{{--        <a class="d-table-cell cz-handheld-toolbar-item"  href="{{route('carrito')}}">--}}
{{--            <span class="cz-handheld-toolbar-icon text-light"><i class="czi-cart"></i></span>--}}
{{--            <span class="cz-handheld-toolbar-label text-light" >Mi Carrito</span>--}}
{{--        </a>--}}
        <a class="d-table-cell cz-handheld-toolbar-item"  href="{{route('cuenta.perfil')}}">
            <span class="cz-handheld-toolbar-icon text-light"><i class="czi-user"></i></span>
            <span class="cz-handheld-toolbar-label text-light" >Mi Cuenta</span>
        </a>
        <a class="d-table-cell cz-handheld-toolbar-item" href="{{route('carrito')}}">
            <span class="cz-handheld-toolbar-icon text-light">
                <i class="czi-cart"></i>
                <span class="badge badge-light badge-pill ml-1 carrito_count font-weight-bold" style="color: #fd7e14"></span>
            </span>
            <span class="cz-handheld-toolbar-label text-light">
                S/.<span class="carrito_total"></span>
            </span>
        </a>
    </div>
</div>

<!-- Back To Top Button-->
<a class="btn-scroll-top" href="#top" data-scroll><span
        class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Arriba</span>
    <i class="btn-scroll-top-icon czi-arrow-up"> </i>
</a>
</div>

@yield('scripting')
<script src="/js/vendor.min.js"></script>
<script src="/js/theme2.min.js"></script>
<script src="/js/sweet/sweetalert2.all.js"></script>
@yield('otros')
{{--<script src="{{asset('js/app.js')}}"></script>--}}

<script>

    $(function(){
        showAllCarritos();
    });

    function eliminarCarrito(idProducto){
        console.log("Eliminando Carro" , idProducto);
        var carrito = JSON.parse(localStorage.getItem("carrito"));
        carrito = carrito.filter(producto => producto.id != idProducto);
        localStorage.setItem("carrito", JSON.stringify(carrito));
        $('#id_carrito_'+idProducto).remove();
        $("#carrito_count").html(carrito.length);
    }

    function agregarCarrito(producto, cantidadParam = 1) {
        Swal.fire({
            title: 'Mi Carrito',
            text: `¿Deseas agregar este producto a tu carrito?`,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                let carrito = [];
                let newcarrito = [];
                if(localStorage.getItem("carrito")) {
                    carrito = JSON.parse(localStorage.getItem("carrito"));
                    let produ = carrito.find(prod => prod.id == producto.id);
                    if(produ){
                        producto.cantidad = produ.cantidad + cantidadParam;
                        producto.subtotal = producto.cantidad * produ.precioRebaja;
                        newcarrito = carrito.filter(p => p.id != producto.id);
                    }else{
                        newcarrito = carrito;
                        producto.cantidad = cantidadParam;
                        producto.subtotal = producto.cantidad * producto.precioRebaja;
                    }
                } else {
                    producto.cantidad = cantidadParam;
                    producto.subtotal = producto.cantidad * producto.precioRebaja;
                }
                //////////////////
                // axios.get(`/api/producto/consulta-stock/${producto.id}`).then(res => {
                $.get( `/api/producto/consulta-stock/${producto.id}`, function( data ) {
                    console.log("Stock", data);
                    // return;
                    this.isLoading = false;
                    let stockActual = data;
                    if(producto.cantidad) {
                        // producto.cantidad = 0;
                        if(producto.cantidad > stockActual) {
                            Swal.fire({
                                // icon: 'info',
                                title: 'Mi Carrito',
                                html:`No hay stock suficiente, sólo se cuenta con <span class="text-primary font-weight-bold">${stockActual} uni.</span>`,
                                imageUrl : producto.imagen1,
                                imageHeight: 150,
                                imageAlt: 'No Image Product'
                            }).then((result) => {
                                if(stockActual>0){
                                    producto.cantidad = stockActual;
                                    producto.subtotal = producto.cantidad * producto.precioRebaja;
                                    newcarrito.push(producto);
                                }
                                localStorage.setItem("carrito", JSON.stringify(newcarrito));
                                showAllCarritos();

                            });
                        } else {
                            newcarrito.push(producto);
                            localStorage.setItem("carrito", JSON.stringify(newcarrito));
                            Swal.fire({
                                // icon: 'info',
                                title: 'Mi Carrito',
                                text: '¡ Se agregó este producto a tu carrito !',
                                imageUrl : producto.imagen1,
                                imageHeight: 150,
                                imageAlt: 'No Image Product'
                            });

                            showAllCarritos();
                        }
                    }



                });
            }
        });
    }

    function cerrarMensajeCarrito() {
        $("#mensaje_carrito").hide();
    }

    function showAllCarritos() {

        if(JSON.parse(localStorage.getItem("carrito"))) {
            $("#carrito").empty();
            var carrito = JSON.parse(localStorage.getItem("carrito"));
            $(".carrito_count").html(carrito.length);
            let total = 0;
            let cantidad = 0;

            carrito.forEach(product => {
                let subtotal = product.cantidad * product.precioRebaja;
                total = total + subtotal;
                cantidad = cantidad + Number(product.cantidad);
                let template = `
                        <div class="widget-cart-item py-2 border-bottom">
                            <button class="close text-danger" type="button" aria-label="Remove"><span aria-hidden="true">&times;</span></button>
                            <div class="media align-items-center">
                                <a class="d-block mr-2"><img width="64" src="${product.imagen1}" alt="Product"/></a>
                                <div class="media-body">
                                    <h6 class="widget-product-title">${product.categoria.nombre}</h6>
                                    <span class="font-weight-normal">${product.nombre}</span>
                                    <div class="widget-product-meta">
                                        <span class="text-accent mr-2">S/ ${product.precioRebaja.toFixed(2)}</span>
                                        <span class="text-dark">X &nbsp&nbsp ${product.cantidad}</span>
                                    </div>
                                </div>
                             </div>
                        </div>`;

                $("#carrito").append(template);
            });

            console.log("Hola ",cantidad);
            $(".carrito_count").html(cantidad);
            $(".carrito_total").html(total.toFixed(2));

        }else{
            $(".carrito_count").html(0);
            $(".carrito_total").html('0.00');
        }
    }
</script>

</body>
</html>
