@extends('layouts.home')
@section('head')
    <style>
        .paginador > * {
            padding-top: .5rem !important;
            justify-content: space-between !important;
            display: flex !important;
        }
        .pagination {
            margin: 0 auto;
        }
    </style>
@endsection
@section('contenido')
    <!-- Page Content-->
    <div class="container mt-4 pb-5 mb-2 mb-md-3">
        <div class="row">
            @include('cuenta.menu')
            <section class="col-lg-8">
                <!-- Orders list-->
                <div class="mt-1">
                    <div class="text-center mb-1">
                        <h2 class="font-weight-bold text-accent mb-1"><em>Mis datos</em></h2>
                    </div>
                    <div>
                        @if(session('mensaje'))
                            <div class="alert alert-success alert-with-icon" role="alert">
                                <div class="alert-icon-box">
                                    <i class="alert-icon czi-check-circle"></i></div>
                                {{session('mensaje')}}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form action="{{ route('cuenta.update.perfil')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="font-weight-medium" for="unp-product-name">Nombre</label>
                            <input class="form-control  @error('nombre') is-invalid @enderror" type="text" id="unp-product-name"
                                   name="nombre" placeholder="Ingrese un Nombre" value="{{ strtoupper(Auth::user()->name) }}">
                        </div>

                        <div class="form-group">
                            <label class="font-weight-medium" for="unp-product-mail">Correo</label>
                            <input class="form-control  @error('email') is-invalid @enderror" type="text" id="unp-product-mail"
                                   name="email" placeholder="Ingrese un Correo" value="{{ Auth::user()->email }}">
                        </div>

                        <div class="form-group">
                            <label class="font-weight-medium" for="unp-category">Perfil</label>
                            <select class="custom-select mr-2 @error('perfil') is-invalid @enderror" id="unp-category" name="perfil">
                                <option value="0">- SELECCIONAR -</option>
                                <option value="1">ADMINISTRADOR</option>
                                <option value="2">CLIENTE</option>
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-medium" for="unp-standard-price">Foto Actual</label>
                                <div class="img-thumbnail rounded">
                                    <img src="{{asset(Auth::user()->avatar)}}" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-medium" for="unp-standard-price">Foto Nueva</label>
                                <div class="cz-file-drop-area">
                                    <div class="cz-file-drop-icon czi-cloud-upload"></div>
                                    <span class="cz-file-drop-message">Arrastra y suelta una foto para actualizar</span>
                                    <input class="cz-file-drop-input" type="file" name="avatar">
                                    <button class="cz-file-drop-btn btn btn-primary btn-sm mb-2" type="button">Seleccionar Imagen</button>
                                </div>
                            </div>
                        </div>
                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">
                                <i class="czi-check font-size-lg mr-2"></i>Actualizar
                            </button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripting')
    <script>

    </script>
@endsection
