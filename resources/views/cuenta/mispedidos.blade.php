@extends('layouts.home')
@section('head')

@endsection
@section('contenido')
    <div class="modal fade" id="order-details">
        <div class="modal-xl modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header mb-1">
                    <h5 class="modal-title font-weight-bold"><em>Pedido <span id="modal-pedido-id" class="text-primary">PED-00</span></em></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pt-0">
                    <div class="row">
                        <div class="col-lg-4 pt-2">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Productos</u></h4></div>
                                <div id="modal-pedido-detalle">

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 pt-2">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center"><h4 class="mb-3"><u>Datos de Envío</u></h4></div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Cliente</div>
                                    <div class="" id="id-envio-cliente"></div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Correo Electrónico</div>
                                    <div id="id-envio-correo"></div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Celular</div>
                                    <div id="id-envio-celular" class=""></div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Ubigeo</div>
                                    <div id="id-envio-ubigeo" class=""></div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Dirección</div>
                                    <div id="id-envio-direccion" class=""></div>
                                </div>

                                <div class="mb-2">
                                    <div class="font-weight-medium">Referencia</div>
                                    <div id="id-envio-referencia" class=""></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 pt-2">
                            <div class="card border-0 box-shadow p-3">
                                <div class="text-center">
                                    <h4 class="mb-3"><u>Captura Voucher</u></h4></div>
                                <div class="img-thumbnail rounded">
                                    <img id="id-imagen-voucher" class="rounded w-100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer-->
                <div class="font-size-md bg-secondary px-3 py-3">
                    <div class="row">
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Usuario:&nbsp;</span>
                            <span id="modal-pedido-registrado"></span>
                        </div>

                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Foto:&nbsp;</span>
                            <img class="rounded" width="40" id="modal-pedido-avatar" src=""/>
                        </div>
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Tipo Envío:&nbsp;</span>
                            <span id="modal-pedido-tipo-envio"></span>
                        </div>
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Estado:&nbsp;</span>
                            <span id="modal-pedido-estado" class="badge font-size-sm"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Subtotal:&nbsp;</span>
                            <span id="modal-pedido-subtotal">S/ 0.00</span>
                        </div>
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Costo Envío:&nbsp;</span>
                            <span id="modal-pedido-igv">S/ 0.000</span>
                        </div>
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Descuento:&nbsp;</span>
                            <span id="modal-pedido-costoenvio">S/ 0.00</span>
                        </div>
                        <div class="col-6 col-sm-3">
                            <span class="text-dark font-weight-medium">Total:&nbsp;</span>
                            <span id="modal-pedido-total" class="font-size-lg font-weight-bold text-primary">S/ 0.00</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pt-1">
        <div class="container d-lg-flex justify-content-between py-1 py-lg-1">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-1">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-dark flex-lg-nowrap justify-content-center justify-content-lg-start">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="{{route('home')}}"><i
                                    class="czi-home"></i>Principal</a></li>
                        <li class="breadcrumb-item text-nowrap">
                            <a href="#">Mi Cuenta</a>
                        </li>
                        <li class="breadcrumb-item text-primary font-weight-bold" aria-current="page">Mis pedidos</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 mb-0 text-accent font-weight-bold"><em class="">Mis Pedidos</em></h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="pb-5 mb-2 mb-md-2 px-3">
            <div class="d-flex flex-nowrap justify-content-between pt-2 pb-3 pb-lg-2 mb-lg-2 pr-1">
                <div class="w-50">
                    <form class="form-inline" id="form-mis-pedidos" action="{{route('cuenta.pedidos')}}">
                        <label class="d-none d-sm-inline mt-sm-0 mr-2" for="id-num-pedido">Buscar por Nº de pedido:</label>
                        <input id="id-num-pedido" type="text" class="form-control" placeholder="Ingrese N° de pedido" name="num_pedido"
                               value="{{request()->get('num_pedido')}}">
                    </form>
                </div>
                <div class="w-50 pb-3 text-right">
                    <a class="btn btn-primary btn-sm btn-shadow" href="{{route('cuenta.pedidos')}}">
                        <i class="czi-reload "></i>&nbsp; Recargar
                    </a>
                </div>
            </div>

            <div class="mb-1">
                <span class="alert alert-info font-size-xs p-1"><em>Para visualizar el detalle del Pedido puede realizar Click sobre el N° de Pedido</em></span>
            </div>
            <div class="table-responsive font-size-md text-nowrap" style="height: 450px">
                <table class="table table-hover table-striped mb-0">
                    <thead>
                    <tr>
                        <th>N° de Pedido</th>
                        <th>Cliente</th>
                        <th>Celular</th>
                        <th>Fecha de Compra</th>
                        <th>Tipo de Envío</th>
                        <th>Estado</th>
                        <th style="text-align: right">SubTotal</th>
                        <th style="text-align: right">Costo Envío</th>
                        <th style="text-align: right">Descuento</th>
                        <th style="text-align: right">TOTAL</th>
{{--                        @if($recibo->estado == 2 && $recibo->tipoenvio == 2)--}}
{{--                        <th>Acciones</th>--}}
{{--                        @endif--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recibos as $recibo)
                        <tr>
                            <td class="py-2">
                                <a class="text-dark font-weight-bold font-size-sm " href="#"
                                   onclick="verModal('{{$recibo->id}}')"
                                   data-toggle="modal">PED-{{str_pad($recibo->id, 8, "0", STR_PAD_LEFT)}}</a>
                            </td>
                            <td class="py-2">{{ucwords($recibo->envio->nombres)}}</td>
                            <td class="py-2">{{ucwords($recibo->envio->celular)}}</td>
                            {{--                            <td><em class="text-info">{{$recibo->envio->celular}}</em></td>--}}
                            {{--<td><u style="color:blue">{{$recibo->envio->correo_electronico}}</u></td>--}}
                            <td class="py-2">{{date_format($recibo->created_at, 'd-m-Y H:i:s')}}</td>
                            <td class="py-2">
                                @if($recibo->tipoenvio == 1)
                                    <span class="text-success font-weight-medium"><i class="czi-dollar font-weight-medium"></i>Contra Entrega</span>
                                @else
                                    <span class="text-primary font-weight-medium">
                                        <i class="czi-card font-weight-medium"></i> Depósito en Cuenta
                                    </span>
                                @endif
                            </td>
                            <td class="py-2">
                                @if( $recibo->estado == 1 )
                                    <span class="badge badge-warning m-0">Pendiente</span>
                                @elseif( $recibo->estado == 2 )
                                    <span class="badge badge-info m-0">Costo de Envío Asignado</span>
                                @elseif( $recibo->estado == 3 )
                                    <span class="badge badge-success m-0">Enviado</span>
                                @elseif( $recibo->estado == 4 )
                                    <span class="badge badge-danger m-0">Entregado</span>
                                @elseif( $recibo->estado == 6 )
                                    <span class="badge badge-accent m-0 font-size-md">Voucher Adjuntado - Sin Confirmar</span>
                                @endif
                            </td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->total, 2, '.', ',')}}</td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->costoenvio, 2, '.', ',')}}</td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->descuento, 2, '.', ',')}}</td>
                            <td class="py-2" style="text-align: right">S/ {{number_format($recibo->total_final, 2, '.', ',')}}</td>
{{--                            <td style="text-align: center">--}}
{{--                                <a  class="btn btn-outline-primary btn-sm @if( $recibo->estado == 6) disabled @endif" href="{{route('cuenta.editar.pedido.cea.deposito' , $recibo->id)}}">Adjuntar Voucher</a>--}}
{{--                            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="pb-4">
            <div class="">
                {{$recibos->appends(request()->query())->links()}}
            </div>
        </div>
    </div>



@endsection

@section('scripting')
    <script>

        function verModal(id, envio) {
            console.log("ID ", id);

            // console.log("envio ", JSON.parse(envio));

            // let envioObj = JSON.parse(envio);

            $('#modal-pedido-detalle').html("Cargando información ...");
            var pedido = (id +"").padStart(8,"0");
            $('#modal-pedido-id').html('PED-' + pedido);
            $('#order-details').modal('show');
            $.get(`/api/ventas/${id}`, function(res) {
                console.log("Pedido ", res);
                $('#modal-pedido-subtotal').html('S/ '+res.recibo.subtotal.toFixed(2));
                $('#modal-pedido-igv').html('S/ '+res.recibo.costoenvio.toFixed(2));
                $('#modal-pedido-costoenvio').html('S/ '+res.recibo.descuento.toFixed(2));
                $('#modal-pedido-total').html('S/ '+res.recibo.total_final.toFixed(2));

                $('#modal-pedido-registrado').html(res.recibo.user.name.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase()));
                $('#modal-pedido-avatar').prop('src',res.recibo.user.avatar);
                if(res.recibo.estado == '1'){
                    $('#modal-pedido-estado').html("Pendiente").addClass('badge-warning');
                }else if(res.recibo.estado == '2'){
                    $('#modal-pedido-estado').html("Costo Envío Asignado").addClass('badge-info');
                }else if(res.recibo.estado == '3'){
                    $('#modal-pedido-estado').html("Enviado").addClass('badge-success');
                }else if(res.recibo.estado == '4'){
                    $('#modal-pedido-estado').html("Entregado").addClass('badge-primary');
                }else if(res.recibo.estado == '6'){
                    $('#modal-pedido-estado').html("Voucher Adjuntado - Pago Confirmado").addClass('badge-accent');
                    $('#id-imagen-voucher').prop('src', res.recibo.imagen_voucher);
                }else if(res.recibo.estado == '9'){
                    $('#modal-pedido-estado').html("Cancelado").addClass('badge-danger');
                }
                if(res.recibo.tipoenvio == '1'){
                    $('#modal-pedido-tipo-envio').html("Contra Entrega").addClass('text-success').addClass('font-weight-bold');
                }else{
                    $('#modal-pedido-tipo-envio').html("Depósito En Cuenta").addClass('text-primary').addClass('font-weight-bold');
                }
                //ENVIO
                $('#id-envio-cliente').html(res.recibo.envio.nombres.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase()));
                $('#id-envio-correo').html(res.recibo.envio.correo_electronico);
                $('#id-envio-celular').html(+res.recibo.envio.celular);
                $('#id-envio-ubigeo').html(res.recibo.envio.departamento + " / " +res.recibo.envio.provincia + " / "+ res.recibo.envio.distrito);
                $('#id-envio-direccion').html(res.recibo.envio.direccion);
                $('#id-envio-referencia').html(res.recibo.envio.referencia);
                //FIN ENVIO

                $('#modal-pedido-detalle').html("No tiene detalle");

                if(res.detalle.length > 0){

                    $('#modal-pedido-detalle').empty();

                    res.detalle.forEach(producto => {

                        let templ_total = producto.total.toFixed(2);
                        let templ_subtotal = producto.subtotal.toFixed(2);

                        let template_item_modal = `
                            <div class="mb-1 pb-2 p-sm-1 ">
                                <div class="media d-block d-flex justify-content-between">
                                       <div class="w-50">
                                            <a class="d-inline-block mx-auto mr-sm-4" href="#">
                                                <img style="width: 80px" src="${producto.imagen}" alt="Product">
                                            </a>
                                        </div>
                                        <div class="w-50">
                                            <h3 class="product-title font-size-base mb-1">
                                             <a href="">${producto.categoria}</a>
                                            </h3>
                                            <h2 class="mb-1 font-size-sm"><a class="text-dark font-weight-normal" href="">${producto.detalle.toLowerCase().replace(/\b[a-z]/g,c=>c.toUpperCase())}</a></h2>
                                            <div class="font-size-xs pt-1">S/ ${templ_subtotal} X ${producto.cantidad} = <span class="text-primary font-weight-bold">S/ ${templ_total}</span></div>
                                        </div>
                                </div>
                            </div>`;

                        $('#modal-pedido-detalle').append(template_item_modal);
                    });
                }
            });
        }
    </script>
@endsection
