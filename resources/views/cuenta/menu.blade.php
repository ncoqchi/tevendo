<!-- Sidebar-->
<aside class="col-lg-4 pt-2 pt-lg-0">
    <div class="cz-sidebar-static rounded-lg box-shadow-lg px-0 py-2 mb-3 mb-lg-0">
        <div class="px-2">
            <div class="media align-items-center">
                <div class="img-thumbnail rounded-circle position-relative"
                     style="width: 6.375rem;">
                    <img class="rounded-circle" src="{{ asset(Auth::user()->avatar) }}" alt="Imagen"></div>
                <div class="media-body pl-3">
                    <h3 class="font-size-base mb-0">{{ strtoupper(Auth::user()->name) }}</h3>
                    <span class="text-accent font-size-sm">{{ Auth::user()->email }}</span>
                </div>
            </div>
        </div>
{{--        <div class="bg-secondary px-4 py-3">--}}
{{--            <h3 class="font-size-sm mb-0 text-muted">Cuenta Personal</h3>--}}
{{--        </div>--}}
{{--        <ul class="list-unstyled mb-0">--}}
{{--            <li class="border-bottom mb-0">--}}
{{--                <a class="nav-link-style d-flex align-items-center @if(Request::is('mis-pedidos')) active @endif px-4 py-3"--}}
{{--                   href="{{route('cuenta.pedidos')}}">--}}
{{--                    <i class="czi-bag opacity-60 mr-2"></i>Mis Pedidos<span class="font-size-sm text-muted ml-auto"></span></a>--}}
{{--            </li>--}}
{{--            <li class="border-bottom mb-0">--}}
{{--                <a class="nav-link-style d-flex align-items-center @if(Request::is('mi-perfil')) active @endif  px-4 py-3"--}}
{{--                                              href="{{route('cuenta.perfil')}}"><i--}}
{{--                        class="czi-user opacity-60 mr-2"></i>Perfil</a></li>--}}
{{--            <li class="mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3"--}}
{{--                                href="account-payment.html"><i class="czi-card opacity-60 mr-2">--}}

{{--                    </i>Cambiar--}}
{{--                    Contrasena</a></li>--}}
{{--            <li class="d-lg-none border-top mb-0">--}}
{{--                <a class="nav-link-style d-flex align-items-center px-4 py-3" href="account-signin.html">--}}
{{--                    <i class="czi-sign-out opacity-60 mr-2"></i>Cerrar Sesión</a></li>--}}
{{--        </ul>--}}
    </div>
</aside>
