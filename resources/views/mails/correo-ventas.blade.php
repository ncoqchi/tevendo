<!Doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Correo de Ventas</title>
    <style>
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%;
        }

        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%; }
        table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%;
        }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 20px;
            width: 580px;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            margin: 0 auto;
            max-width: 580px;
            padding: 30px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%;
        }

        .wrapper {
            box-sizing: border-box;
            padding: 20px;
        }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            margin-top: 10px;
            text-align: center;
            width: 100%;
        }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            margin-bottom: 30px;
        }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize;
        }

        h2 {
            font-size: 28px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize;
        }

        p,
        ul,
        ol {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            margin-bottom: 15px;
        }
        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px;
        }

        a {
            color: #3498db;
            text-decoration: underline;
        }

        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%; }
        .btn > tbody > tr > td {
            padding-bottom: 15px; }
        .btn table {
            width: auto;
        }
        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center;
        }
        .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize;
        }

        .btn-primary table td {
            background-color: #3498db;
        }

        .btn-primary a {
            background-color: #3498db;
            border-color: #3498db;
            color: #ffffff;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        .mt0 {
            margin-top: 0;
        }

        .mb0 {
            margin-bottom: 0;
        }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0;
        }

        .powered-by a {
            text-decoration: none;
        }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            margin: 20px 0;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }
            table[class=body] .content {
                padding: 0 !important;
            }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            table[class=body] .btn table {
                width: 100% !important;
            }
            table[class=body] .btn a {
                width: 100% !important;
            }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
            #MessageViewBody a {
                color: inherit;
                text-decoration: none;
                font-size: inherit;
                font-family: inherit;
                font-weight: inherit;
                line-height: inherit;
            }
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>
</head>
<body class="">
<div class="container">
    <div class="content main">
        <div>
            <h1 style="font-weight: bold; font-size: 32px"><span style="border-bottom: 4px solid red">TEVENDO<span style="color: #FE4344">.PE</span></span></h1>
            <p><span style="font-weight: bold;font-size: 24px">PEDIDO <span style="color: #FE4344">#PED-{{str_pad($details->id, 8, "0", STR_PAD_LEFT)}}</span></span></p>
            <p><span style="font-weight: bold">Cliente:</span> {{ strtoupper($details->envio['nombreCompleto'])}}</p>
            <p><span style="font-weight: bold">Tipo de Envío:</span>
                @if ($details->recibo['envio'] == 1)
                    CONTRA ENTREGA
                @else
                    DEPÓSITO EN CUENTA
                @endif
            </p>
            <p>Total: <span>S/ {{number_format($details->recibo['total'] ,2, '.', ',')}}</span></p>
            <p>Costo de Envío: <span>S/ {{ number_format( $details->recibo['costoenvio'],2, '.', ',')}}</span></p>
            <p>Descuento: <span>S/ {{ number_format( $details->recibo['descuento'],2, '.', ',')}}</span></p>
            <p>Código Promocional Aplicado: <span>{{ $details->recibo['codigo_promocional']}}</span></p>
            <p style="font-weight: bold; font-size: 1.5em">Total Final: <span style="font-size: 1.5em; color: red">S/ {{number_format($details->recibo['total_final'],2, '.', ',')}}</span></p>
            <p>Consulte Sus Pedidos <a href="{{$details->path.'/mis-pedidos'}}">AQUÍ</a></p>
        </div>
        <hr>
        <div>
            <h2 style="font-weight: bold; color: #2f66ca">Detalle de la Compra</h2>
            @foreach($details->detalle as $det)
            <div style="background-color: #fff; padding: 10px; margin-top: 5px; display: flex; flex-wrap: wrap;align-items: center">
                <div style="width: 40%; text-align: right">
                    <img width="150px" style="border-radius: 5px" src="{{$details->path.$det['imagen']}}" alt="Producto Sin Imagen">
{{--                    <img width="150px" style="border-radius: 5px" src="{{$details->path.explode(',', $det['imagen'])[0]}}" alt="Producto Sin Imagen">--}}
                </div>
                <div style="width: 40%; padding: 0px 5px">
                    <div style="font-weight: bold">{{$det['detalle']}}</div>
                    <div>Precio: S/.{{number_format($det['subtotal'],2, '.', ',')}}</div>
                    <div>Cantidad: {{$det['cantidad']}}</div>
                    <div style="font-weight: bold;">Total: <span style="color: red">S/.{{number_format($det['total'],2, '.', ',')}}</span></div>
                </div>
            </div>
            <hr>
            @endforeach
        </div>
        <div>
            <h2 style="font-weight: bold;color: #2f66ca">Detalle de Envío</h2>
            <p><span style="font-weight: bold">Correo Electrónico:</span> {{$details->envio['email']}}</p>
            <p><span style="font-weight: bold">Celular:</span> {{$details->envio['celular']}}</p>
            <p><span style="font-weight: bold">Departamento:</span> {{$details->envio['departamentoNombre']}}</p>
            <p><span style="font-weight: bold">Provincia:</span> {{$details->envio['provinciaNombre']}}</p>
            <p><span style="font-weight: bold">Distrito:</span> {{$details->envio['distritoNombre']}}</p>
            <p><span style="font-weight: bold">Dirección:</span> {{$details->envio['direccion']}}</p>
            <p><span style="font-weight: bold">Referencia:</span> {{$details->envio['referencia']}}</p>
            <p><span style="font-weight: bold">Comentarios:</span> {{$details->recibo['comentarios']}}</p>
        </div>


        <div>
            <div class="footer">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-block">
                            <span class="apple-link">Tevendo.pe</span>
                            <br> Desea ver más productos? <a href="{{$details->path}}">Visitar Tienda</a>.
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block powered-by">
                            Desarrollado por <a href="#">NCA, nelson.coqchi@gmail.com</a>.
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
