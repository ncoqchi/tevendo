//require('./bootstrap');
// import persistentState from 'vue-persistent-state';
window.Vue = require('vue');
import axios from 'axios';
window.axios = axios;
import VueCarousel from 'vue-carousel';
import VueGeolocation from "vue-browser-geolocation";

Vue.use(VueCarousel);
Vue.use(VueGeolocation);

import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
    load : {
        key : 'AIzaSyBVpd6kHJnmlotzPUcghm5KBc0x6EWYB5c'
    }
});

Vue.component('carrito', require('./components/carrito/Carrito.vue').default);
Vue.component('carrito-flotante', require('./components/modalcarrito/Car.vue').default);
Vue.component('productos', require('./components/home/productos').default);
Vue.component('tienda', require('./components/home/tienda').default);
Vue.component('categorias', require('./components/home/categorias').default);

Vue.component('pedidos', require('./components/cuenta/MisPedidos.vue').default);

Vue.component('compra-principal', require('./components/compra/CompraPrincipal').default);
Vue.component('direccion-envio', require('./components/compra/DireccionEnvio').default);
Vue.component('resumen', require('./components/compra/Resumen').default);

const app = new Vue({
    el: '#app',
});
