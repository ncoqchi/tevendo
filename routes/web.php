<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/404', function () {
    return view('pagina/404');
});

//Route::view('fotos','fotos', ['numero' => 125]);
Route::get('/', 'HomeController@index');

//Route::get('fotos','FotosController@index')->name('fotos');
//
//Route::get('blog','BlogController@index')->name('blog');
//
//Route::get('tienda','TiendaController@index')->name('tienda');
//
//Route::get('tienda/{idProducto}','TiendaController@getProducto')->name('tienda.detalle');
//
//Route::get('tienda/editar/{idProducto}','TiendaController@editar')->name('tienda.editar');
//
//Route::put('tienda/editar/{idProducto}','TiendaController@actualizar')->name('tienda.actualizar');
//
////Route::get('tienda/editar/{idProducto}','TiendaController@actualizar')->name('tienda.actualizar');
//
//Route::get('nosotros/{nombre?}', 'AdminController@inicio')->name('nosotros');
//
//Route::post('tienda', 'TiendaController@registrar')->name('tienda.registrar');

Route::get('admin', 'AdminController@inicio')->name('admin.inicio')->middleware(['auth','role:administrador','verified']);

Route::get('admin/informacion', 'AdminController@getInformacion')->name('admin.informacion')->middleware(['auth','role:administrador','verified']);

Route::put('admin/actualizar-informacion', 'AdminController@actualizarInformacion')->name('admin.actualizar.informacion')->middleware(['auth','role:administrador','verified']);


//********* CATEGORIAS ********
Route::get('admin/nueva-categoria', 'AdminController@nuevaCategoria')->name('admin.nueva.categoria')->middleware(['auth','role:administrador','verified']);

Route::get('admin/categorias', 'AdminController@listarCategorias')->name('admin.categorias')->middleware(['auth','role:administrador','role:administrador','verified']);

Route::post('admin/categorias', 'AdminController@registrarCategoria')->name('admin.registrar.categoria')->middleware(['auth','role:administrador','role:administrador','verified']);

Route::get('admin/categorias/{idCategoria}', 'AdminController@editarCategoria')->name('admin.editar.categoria')->middleware(['auth','role:administrador','role:administrador','verified']);

Route::put('admin/categorias', 'AdminController@actualizarCategoria')->name('admin.actualizar.categoria')->middleware(['auth','role:administrador','verified']);

Route::delete('admin/categorias/{idCategoria}', 'AdminController@eliminarCategoria')->name('admin.eliminar.categoria')->middleware(['auth','role:administrador','verified']);

//********* TIPO ENVIOS *********
Route::get('admin/nuevo-tipo-envio', 'AdminController@nuevoTipoEnvio')->name('admin.nuevo.tipoenvio')->middleware(['auth','role:administrador','verified']);

Route::get('admin/tipos-envio', 'AdminController@listarTiposEnvio')->name('admin.tiposenvio')->middleware(['auth','role:administrador','verified']);

Route::get('admin/tipos-envio/{idTipoEnvio}', 'AdminController@editarTipoEnvio')->name('admin.editar.tipoenvio')->middleware(['auth','role:administrador','verified']);

Route::put('admin/tipos-envio', 'AdminController@actualizarTipoEnvio')->name('admin.actualizar.tipoenvio')->middleware(['auth','role:administrador','verified']);

Route::post('admin/tipos-envio', 'AdminController@registrarTipoEnvio')->name('admin.registrar.tipoenvio')->middleware(['auth','role:administrador','verified']);

Route::delete('admin/tipos-envio/{idTipoEnvio}', 'AdminController@eliminarTipoEnvio')->name('admin.eliminar.tipoenvio')->middleware(['auth','role:administrador','verified']);

//********* PRODUCTOS ***********
Route::get('admin/nuevo-producto', 'AdminController@nuevoProducto')->name('admin.nuevo.producto')->middleware(['auth','role:administrador','verified']);

Route::get('admin/productos', 'AdminController@listarProductos')->name('admin.productos')->middleware(['auth','role:administrador','verified']);

Route::post('admin/productos', 'AdminController@registrarProducto')->name('admin.registrar.producto')->middleware(['auth','role:administrador','verified']);

Route::get('admin/productos/{idProducto}', 'AdminController@editarProducto')->name('admin.editar.producto')->middleware(['auth','role:administrador','verified']);

Route::put('admin/productos/{idProducto}', 'AdminController@actualizarProducto')->name('admin.actualizar.producto')->middleware(['auth','role:administrador','verified']);

Route::delete('admin/productos/{idProducto}', 'AdminController@eliminarProducto')->name('admin.eliminar.producto')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos pendiente  **********************//
Route::get('admin/pedidos-pendientes', 'AdminController@getpedidosPendientes')->name('admin.pedidos.pendientes')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedido-pendiente/{idPedido}', 'AdminController@editarPedidoPendiente')->name('admin.editar.productopendiente')->middleware(['auth','role:administrador','verified']);
//actualizacion
Route::get('admin/pedido-pendiente/actualizar-costoenvio/{idPedido}/{costoenvio}', 'AdminController@actualizarCostoEnvioDePedidoPendiente')->name('admin.actualizar.productopendiente')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos Con Costo de Envio  **********************//
Route::get('admin/pedidos-con-costo-envio', 'AdminController@getpedidosConCostoEnvio')->name('admin.pedidos.costoenvio')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-con-costo-envio/{idPedido}', 'AdminController@editarPedidoConCostoEnvio')->name('admin.pedidos.editar.costoenvio')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-con-costo-envio/actualizar/{idPedido}', 'AdminController@actualizarPedidoConCVaEnviado')->name('admin.pedidos.actualizar.costoenvio.aenviado')->middleware(['auth','role:administrador','verified']);

////ADJNUTAR VOUCHER
Route::get('admin/pedidos-con-costo-envio/adjuntar/{idPedido}', 'AdminController@getPedidoParaAdjuntarVocher')->name('admin.pedidos.adjuntar.voucher')->middleware(['auth','role:administrador','verified']);
Route::put('admin/pedidos-con-costo-envio/adjuntar/{idPedido}', 'AdminController@actualizarPedidoCostoAsigYDepositoEnCuenta')->name('admin.pedidos.adjuntar.voucher.enviar')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos Enviados  **********************
Route::get('admin/pedidos-enviados', 'AdminController@getPedidosEnviados')->name('admin.pedidos.enviados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-enviados/{idPedido}', 'AdminController@editarPedidoEnviado')->name('admin.pedidos.editar.enviados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-enviados/actualizar/{idPedido}', 'AdminController@actualizarPedidoEnviadoaEntregado')->name('admin.pedidos.actualizar.enviados')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos Entregados  ***********************
Route::get('admin/pedidos-entregados', 'AdminController@getPedidosEntregados')->name('admin.pedidos.entregados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-entregados/{idPedido}', 'AdminController@editarPedidoEntregados')->name('admin.pedidos.editar.entregados')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos DEPOSITADOS  ***********************
Route::get('admin/pedidos-depositados', 'AdminController@getPedidosDepositados')->name('admin.pedidos.depositados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-depositados/{idPedido}', 'AdminController@editarPedidoDepositado')->name('admin.pedidos.editar.depositados')->middleware(['auth','role:administrador','verified']);

//*********************  Pedidos DEPOSITADOS CONFIRMADOS  ***********************
Route::get('admin/pedidos-depositados-confirmados', 'AdminController@getPedidosDepositadosConfirmados')->name('admin.pedidos.depositados.confirmados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-depositados-confirmados/{idPedido}', 'AdminController@editarPedidoDepositadoConfirmado')->name('admin.pedidos.editar.depositadosconfirmados')->middleware(['auth','role:administrador','verified']);

Route::get('admin/pedidos-cancelados', 'AdminController@getPedidosCancelados')->name('admin.pedidos.cancelados')->middleware(['auth','role:administrador','verified']);
Route::get('admin/pedidos-cancelados/{idPedido}', 'AdminController@editarPedidoCancelado')->name('admin.pedidos.editar.cancelados')->middleware(['auth','role:administrador','verified']);
//Route::put('admin/pedidos-entregados/{idPedido}', 'AdminController@actualizarPedidoEntregados')->name('admin.pedidos.actualizar.entregados')->middleware(['auth','verified']);

//******************************************************************
// CANCELAR
Route::get('admin/pedidos/cancelar/{idPedido}/{motivo}', 'AdminController@actualizarPedidoCancelado')->name('admin.pedidos.cancelar')->middleware(['auth','role:administrador','verified']);

// MOVIMIENTOS DE UN PRODUCTO
Route::get('admin/producto-movimientos/{idProducto}', 'MovimientoController@getMovimientosPorProducto')->name('admin.producto.movimientos')->middleware(['auth','role:administrador','verified']);
Route::post('admin/producto-movimientos/{idProducto}', 'MovimientoController@registrarMovimiento')->name('admin.producto.registrar.movimiento')->middleware(['auth','role:administrador','verified']);

//VENTAS POR MES
Route::get('admin/ventas-por-anio', 'ChartsController@getVentasPorAnio')->name('admin.ventas.poranio')->middleware(['auth','role:administrador','verified']);
//VENTAS POR RANGO DE DIAS
Route::get('admin/ventas-por-rangos', 'ChartsController@getVentasPorRangos')->name('admin.ventas.porrangos')->middleware(['auth','role:administrador','verified']);
//CODIGOS PROMOCIONALES
Route::get('admin/codigos-promocionales', 'CodigoPromocionalController@getCodigos')->name('admin.codigospromocionales')->middleware(['auth','role:administrador','verified']);
Route::post('admin/codigos-promocionales', 'CodigoPromocionalController@registrarCodigo')->name('admin.registrar.codigo')->middleware(['auth','role:administrador','verified']);
Route::delete('admin/codigos-promocionales/{idCodigo}', 'CodigoPromocionalController@eliminarCodigo')->name('admin.eliminar.codigo')->middleware(['auth','role:administrador','verified']);
Route::get('/api/codigo-promocional/{codigo}', 'CodigoPromocionalController@getCodigo')->name('admin.codigopromocional')->middleware(['auth','verified']);
Auth::routes(['verify' => true]);

//CARRITO DE COMPRAS
Route::get('/carrito', 'HomeController@carrito')->name('carrito');
Route::get('/prueba/{idProducto}', 'HomeController@prueba')->name('prueba');
//HOME PRINCIPAL
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/productos/categoria/{idCategoria}', 'HomeController@listarProductosPorCategoria')->name('productos.categoria');

Route::get('/producto/{idProducto}', 'HomeController@detalleProducto')->name('producto.detalle');

Route::get('/tienda', 'HomeController@listarTodosLosProductos')->name('tienda');

Route::get('/ofertas', 'HomeController@listarTodasLasOfertas')->name('ofertas');

Route::get('/comprar', 'HomeController@comprar')->name('comprar')->middleware(['auth','verified']);

Route::get('/tienda/busqueda', 'HomeController@listarTodosLosProductosPorNombre')->name('productos.por.nombre');

Route::get('/listado-productos', 'HomeController@getAllProductos')->name('productos');

Route::get('/listado-productos-ofertas', 'HomeController@getAllProductosOfertas')->name('productos.ofertas');

Route::get('/listado-categorias', 'HomeController@getAllCategorias')->name('categorias');

Route::get('/user-info', 'HomeController@getUserInfo')->name('user-info');

Route::get('/api/producto/consulta-stock/{idProducto}', 'ProductoController@getStockActual')->name('user-info');

Route::prefix('api')->group(function() {
    Route::resource('ventas', 'VentaController')->middleware(['auth','verified']);
    Route::resource('comentarios', 'ComentarioController')->middleware(['auth','verified']);
    Route::resource('liked', 'LikedController')->middleware(['auth','verified']);
    Route::resource('categorias', 'CategoriaController');
    Route::resource('productos', 'ProductoController');
});

Route::prefix('admin')->group(function() {
    Route::resource('sliders', 'SliderController')->middleware(['auth','role:administrador','verified']);
});

Route::get('/api/categorias-productos', 'CategoriaController@getCategoriasConProductos')->name('categorias.productos');

//Route::prefix('api')->group(function() {
//    Route::resource('productos', 'ProductoController');
//});
//UBIGEO CONTROLLER
Route::get('/ubigeo/departamento', 'UbigeoController@getDepartamentos')->name('ubigeo.departamento');
Route::get('/ubigeo/provincia/{idDepartamento}', 'UbigeoController@getProvinciaPorDepartamento')->name('ubigeo.provincia');
Route::get('/ubigeo/distrito/{idProvincia}/{idDepartamento}', 'UbigeoController@getDistritosPorProvinciayDepartamento')->name('ubigeo.provincia.distrito');

// MIS PEDIDOS
Route::get('/mis-pedidos', 'CuentaController@index')->name('cuenta.pedidos')->middleware(['auth','verified']);

// ************ Muestra el pedido con estado Costo de Envio Asignado y tipo de envio Deposito en cuenta
// Acciones Adjuntar Voucher
Route::get('/mi-pedido/{idPedido}', 'CuentaController@getPedidoCostoAsigYDepositoEnCuenta')->name('cuenta.editar.pedido.cea.deposito')->middleware(['auth','verified']);



Route::get('/mi-perfil', 'CuentaController@verProfile')->name('cuenta.perfil')->middleware(['auth','verified']);
Route::post('/actualizar-perfil', 'CuentaController@updateProfile')->name('cuenta.update.perfil')->middleware(['auth','verified']);
Route::get('/api/pedidos', 'CuentaController@getPedidos')->name('api.pedidos')->middleware(['auth','verified']);


