<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likeds', function (Blueprint $table) {
            //$table->id();
            $table->unsignedBigInteger('comentario_id');
            $table->unsignedBigInteger('user_id');
            $table->boolean('like');
            $table->boolean('unlike');
            $table->timestamps();
            $table->foreign('comentario_id')->references('id')->on('comentarios');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likeds');
    }
}
