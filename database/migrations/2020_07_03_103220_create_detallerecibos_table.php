<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallerecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detallerecibos', function (Blueprint $table) {
            $table->id();
            $table->string('detalle');//se almacena como historial el nombre del producto que compro
            $table->string('categoria');//se almacena como historial el nombre del categoria del producto
            $table->integer('cantidad');
            $table->double('subtotal');
            $table->double('total');
            $table->string('imagen');
            $table->char('estado');
            $table->unsignedBigInteger('recibo_id');
            $table->foreign('recibo_id')->references('id')->on('recibos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detallerecibos');
    }
}
