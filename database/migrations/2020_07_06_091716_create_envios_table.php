<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');//se almacena como historial el nombre del producto que compro
            $table->string('apellidos');//se almacena como historial el nombre del categoria del producto
            $table->string('correo_electronico');
            $table->string('celular');
            $table->string('departamento_id');
            $table->string('provincia_id');
            $table->string('distrito_id');
            $table->text('direccion');
            $table->text('referencia');
            $table->string('coordenadas');
            $table->unsignedBigInteger('recibo_id');
            $table->timestamps();
            $table->foreign('recibo_id')->references('id')->on('recibos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
    }
}
