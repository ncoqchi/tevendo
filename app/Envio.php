<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envio extends Model
{
    protected $table = 'envios';

    public function user() {
        return $this->belongsTo(Recibo::class);
    }
}
