<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnviarCorreoNuevoPedido extends Mailable
{
    use Queueable, SerializesModels;
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        //
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        #PED-{{str_pad($details->id, 8, "0", STR_PAD_LEFT)}}
        return $this->view('mails.correo-nuevo-pedido')->subject("Gracias por su Compra :) Su pedido PED-".str_pad($this->details->id, 8, "0", STR_PAD_LEFT)." ha sido generado");
    }
}
