<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }

    public function tipoenvio(){
        return $this->belongsTo(Tipoenvio::class);
    }
}
