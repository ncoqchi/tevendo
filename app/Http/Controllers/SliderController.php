<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{

    public function index(Request $request)
    {
        $sliders = App\Slider::all();
        $categorias = App\Categoria::orderBy('nombre','desc')->get();
        $productos = App\Producto::orderBy('nombre','desc')->get();
        return view('admin.sliders', compact('sliders','categorias','productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    public function getCategoriasConProductos() {
//        $categorias = App\Categoria::all();
//        foreach ($categorias as $cat) {
//            $productos = App\Producto::where('categoria_id', $cat->id)->get();
//            $cat->productos = $productos;
//        }
//        return $categorias;
    }


    public function store(Request $request) {
        $request->validate([
            'categoria' => [Rule::requiredIf($request->radio == 1)],
            'producto' => [Rule::requiredIf($request->radio == 2)],
            'imagenParaPc' => 'required',
            'imagenParaCelular' => 'required'
        ]);

        $slide = new App\Slider();
        if($request->producto) {
            $slide->enlace = '/producto/'.$request->producto;
            $slide->producto_id = $request->producto;
            $slide->categoria_id = 0;
        } else {
            $slide->enlace = '/productos/categoria/'.$request->categoria;
            $slide->producto_id = 0;
            $slide->categoria_id = $request->categoria;
        }

        if($request->hasFile('imagenParaPc')){
            $file = $request->file('imagenParaPc');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'slide_pc_'.date('Ymd_His').$extension;
            $file->move(public_path().'/sliders/',$name);
            $slide->imagen = '/sliders/'.$name;
        }

        if($request->hasFile('imagenParaCelular')) {
            $file = $request->file('imagenParaCelular');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'slide_cel_'.date('Ymd_His').$extension;
            $file->move(public_path().'/sliders/',$name);
            $slide->imagen2 = '/sliders/'.$name;
        }

        $slide->save();
        return back()->with('mensaje', 'Slider Registrado Exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $slide =  App\Slider::findOrFail($id);
        $categorias = App\Categoria::all();
        $productos = App\Producto::all();
        return view('admin.slider', compact('slide','categorias','productos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'categoria' => [Rule::requiredIf($request->radio == 1)] ,
            'producto' => [Rule::requiredIf($request->radio == 2)] ,
            'imagenParaPc' => 'required' ,
            'imagenParaCelular' => 'required'
        ]);

        $slide = App\Slider::findOrFail($id);

        if($request->producto) {
            $slide->enlace = '/producto/'.$request->producto;
            $slide->producto_id = $request->producto;
            $slide->categoria_id = 0;
        } else {
            $slide->enlace = '/productos/categoria/'.$request->categoria;
            $slide->producto_id = 0;
            $slide->categoria_id = $request->categoria;
        }

        if($request->hasFile('imagenParaPc')){
            $fileAnterior = public_path().$slide->imagen;
//            if (is_writable($fileAnterior)) {
//                try {
//                $success = unlink($fileAnterior);
//                } catch (Exception $e) {
//
//                }
//            }
            $file = $request->file('imagenParaPc');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'slide_pc_'.date('Ymd_His').$extension;
            $file->move(public_path().'/sliders/', $name);
            $slide->imagen = '/sliders/'.$name;
        }

        if($request->hasFile('imagenParaCelular')) {
            $fileAnterior = public_path().$slide->imagen2;
//            if (is_writable($fileAnterior)) {
//                try {
//                    $success = unlink($fileAnterior);
//                } catch (Exception $e) {
//
//                }
//            }
            $file = $request->file('imagenParaCelular');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'slide_cel_'.date('Ymd_His').$extension;
            $file->move(public_path().'/sliders/', $name);
            $slide->imagen2 = '/sliders/'.$name;
        }

        $slide->save();
        return back()->with('mensaje', 'Slider Actualizado Exitosamente!');

    }


    public function destroy($id) {
        $slide =  App\Slider::findOrFail($id);
        $file = public_path().$slide->imagen;
        if (file_exists($file)) {
            $success = unlink($file);
        }
        $slide->delete();
        return "Ok";
        //return back()->with('mensaje', 'Slider Eliminado Exitosamente!');
        //$sliders = App\Slider::all();
        //$categorias = App\Categoria::all();
        //$productos = App\Producto::all();
        //return view('admin.sliders', compact('sliders','categorias','productos'));
    }
}
