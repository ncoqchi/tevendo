<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;

class UbigeoController extends Controller
{

    public function __construct()
    {
    }


    public function index() {

    }

    public function getDepartamentos() {
        $departamentos = App\Departamento::all();
        return $departamentos;
    }


    public function getProvinciaPorDepartamento($idDepartamento){
        $provincias = App\Provincia::where('departamento_id', $idDepartamento)->get();
        return $provincias;
    }

    public function getDistritosPorProvinciayDepartamento($idProvincia, $idDepartamento) {
        $distritos = App\Distrito::where('departamento_id', $idDepartamento)->where('provincia_id', $idProvincia)->get();
        return $distritos;
    }

}
