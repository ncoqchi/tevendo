<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Mail\EnviarCorreoNuevoPedido;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;
class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $mensaje = array();
        $infostock = array();
        $info = array();
        $flag = true;
        DB::beginTransaction();
        try {

            foreach ($request->detalleRecibo as $det) {
                $cantidadCompra = $det['cantidad'];
                $idProducto = $det['idProducto'];
                $producto = App\Producto::findOrFail($idProducto);
                $stockActual = $producto->stock;
                $nombreProducto = $producto->nombre;
                if($cantidadCompra > $stockActual){
                    //no hay stock
                    $flag = false;
                    $info['id'] = $producto->id;
                    $info['nombre'] = $producto->nombre;
                    $info['unidadesAComprar'] = $cantidadCompra;
                    $info['stockActual'] = $stockActual;
                    $info['mensaje'] = 'Unidades No Disponibles';
                    array_push($infostock, $info);
                }
                //""""""""""""""""""""""""""""""""""""""""""""
            }

            if($flag == false){
                $mensaje['code'] = '0';
                $mensaje['detalle'] = $infostock;
                return json_encode($mensaje);

            }else{

                //DB::table('users')->update(['votes' => 1]);
                //B::table('posts')->delete();
                $recibo = new App\Recibo();
                $recibo->user_id = $request->recibo['usuarioId'];
                $recibo->comentarios = $request->recibo['comentarios'];
                $recibo->tipoenvio = $request->recibo['tipoenvio'];
                $recibo->subtotal = $request->recibo['subtotal'];
                $recibo->igv = $request->recibo['igv'];
                $recibo->costoenvio = $request->recibo['costoenvio'];
                $recibo->total = $request->recibo['total'];
                $recibo->estado = $request->recibo['estado'];
                if($request->recibo['flagCodigoPromocional']){
                    $recibo->codigo_promocional = $request->recibo['codigoDescuento'];
                    $recibo->descuento = $request->recibo['descuento'];
                    $recibo->total_final = $request->recibo['nuevoTotal'];
                    //verificar la promocion si es de MODO 1 es decir para una persona, para un solo USO, como ya la uso.
                    //Ya no se puede Usar
                    $promocion = App\Promocion::where('codigo', $recibo->codigo_promocional)->first();
                    if($promocion->modo == 1){
                        $promocion->estado = false;
                        $promocion->save();
                    }
                }else{
                    $recibo->codigo_promocional = null;
                    $recibo->descuento = 0;
                    $recibo->total_final = $recibo->total;
                }

                $recibo->save();
                foreach ($request->detalleRecibo as $det) {
                    $detrecibo = new App\Detallerecibo();
                    $detrecibo->recibo_id   = $recibo->id;
                    $detrecibo->detalle     = $det['detalle'];
                    $detrecibo->categoria   = $det['categoria'];
                    $detrecibo->cantidad    = $det['cantidad'];
                    $detrecibo->subtotal    = $det['subtotal'];
                    $detrecibo->total       = $det['total'];
                    $detrecibo->imagen      = $det['imagen'];
                    $detrecibo->estado      = $det['estado'];
                    $detrecibo->producto_id = $det['idProducto'];
                    $detrecibo->save();
                    //""""""""""""""""""""""""""""""""""""""""""""
                    //SE REGISTRA UN MOVIMIENTO DE SALIDA
                    $movimiento = new App\Movimiento();
                    $movimiento->cantidad =  $detrecibo->cantidad;
                    $movimiento->ingreso_salida = "0";//SALIDA DE VENTAS COMO REGISTRO PENDIENTE
                    $movimiento->producto_id = $detrecibo->producto_id;
                    $movimiento->comentario = "Se registró un Pedido Pendiente N° ".$recibo->id." - Se vendió ".$detrecibo->cantidad." uni. del producto ".$detrecibo->detalle;
                    $movimiento->save();
                    //EL STOCK SE DISMINUYE
                    $producto = App\Producto::findOrFail($det['idProducto']);
                    $producto->stock = $producto->stock - $detrecibo->cantidad;
                    $producto->save();
                    //""""""""""""""""""""""""""""""""""""""""""""
                }

                $envio = new App\Envio();
                $envio->nombres = $request->envio['nombreCompleto'];
                $envio->apellidos = $request->envio['nombreCompleto'];
                $envio->correo_electronico = $request->envio['email'];
                $envio->celular = $request->envio['celular'];
                $envio->departamento_id = $request->envio['departamentoId'];
                $envio->provincia_id = $request->envio['provinciaId'];
                $envio->distrito_id = $request->envio['distritoId'];

                $envio->departamento = $request->envio['departamento'];
                $envio->provincia = $request->envio['provincia'];
                $envio->distrito = $request->envio['distrito'];

                $envio->direccion = $request->envio['direccion'];
                $envio->referencia = $request->envio['referencia'];
                $envio->coordenadas = $request->envio['coordenadas'];
                $envio->recibo_id = $recibo->id;
                $envio->save();
                DB::commit();
                //enviando correo
                $venta = (object)[];
                $venta->recibo = $recibo;
                $venta->detalle = App\Detallerecibo::where('recibo_id',$recibo->id)->get();
                $venta->path = str_replace('/'.$request->path(), "", $request->url());
//            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
                $venta->envio = $envio;
                $venta->id = $recibo->id;
                Mail::to($envio->correo_electronico)->queue(new EnviarCorreoNuevoPedido($venta));

                $mensaje['code'] = '1';
                $mensaje['correo'] = $envio->correo_electronico;
                $mensaje['nombres'] = $envio->nombres;
                $mensaje['celular'] = $envio->celular;
                $mensaje['error'] = '';
                return json_encode($mensaje);
//            return response()->json([
//                'status' => 'ok',
//                'msg'    => 'Se registro su pedido satisfactoriamente, en breve le llegara un correo de su pedido',
//                'errors' => '',
//            ], 200);
            }

        } catch (\Exception $exception) {
            DB::rollback();
            \Log::debug('VentaController->store() ' . $exception->getMessage());
            $mensaje['code'] = '-1';
            $mensaje['detalle'] = 'Ocurrio un error al registrar su pedido, consulte al administrador';
            $mensaje['error'] = $exception;
//            return $mensaje;
            return \response($mensaje, 500);
        }
    }

//    public function enviarCorreo($request, $idGenerado) {
//        //$listaProductos = array();
//        //array_push($listaProductos, $object);
//        try {
//            $venta = (object)[];
//            $venta->recibo = $request->recibo;
//            $venta->detalle = $request->detalleRecibo;
//            $venta->path = str_replace('/'.$request->path(), "", $request->url());
//            $venta->envio = $request->envio;
//            $venta->id = $idGenerado;
//            $destinoCorreo = Auth::user()->email;
//            Mail::to($destinoCorreo)->queue(new EnviarCorreoVentas($venta));
//        } catch (\Exception $e) {
//            // Almacenamos la información del error.
//            \Log::debug('Test var fails' . $e->getMessage());
//        }
//    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = array();
        $recibo = App\Recibo::findOrFail($id);
        $recibo->user = $recibo->user;
        $recibo->envio = $recibo->envio;
        $detallerecibo = App\Detallerecibo::where('recibo_id',$id)->get();
        //$envio = App\Envio::where('recibo_id',$id)->first();
        $pedido['recibo'] = $recibo;
        $pedido['detalle'] = $detallerecibo;
        //$pedido['envio'] = $recibo->envio;
        return $pedido;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
