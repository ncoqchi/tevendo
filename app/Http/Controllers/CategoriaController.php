<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categorias = App\Categoria::all();
        return $categorias;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    public function getCategoriasConProductos(){

        $categorias = App\Categoria::all();

        foreach ($categorias as $cat) {
            $productos = App\Producto::where('categoria_id', $cat->id)->get();
            $cat->productos = $productos;
        }

        return $categorias;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

//        $pedido = array();
//        $recibo = App\Recibo::findOrFail($id);
//        $detallerecibo = App\Detallerecibo::where('recibo_id', $id)->get();
//        $envio = App\Envio::where('recibo_id', $id)->get();
//        $pedido['recibo'] = $recibo;
//        $pedido['detalle'] = $detallerecibo;
//        $pedido['envio'] = $envio;
//        return $pedido;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
