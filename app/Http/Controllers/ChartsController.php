<?php
namespace App\Http\Controllers;

use App\Mail\EnviarCosEnvActCorreoPedidos;
use App\Mail\EnviarCorreoPedidoEntregado;
use App\Mail\EnviarCorreoPedidoEnviado;
use App\Mail\EnviarCorreoPedidoDepositoConfirmado;
use App\Mail\EnviarCorreoPedidoCancelado;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App;
use DB;
class ChartsController extends Controller{



    public function getVentasPorAnio(Request $request){
        if($request->p) {
            $anio = $request->p;
        } else{
            $anio = date('Y');
        }
        $ingresos = DB::select('call rtnGraficosIngresos( ? )', array($anio));
        return view('admin.charts.ventas', compact('ingresos'));
    }

    public function getVentasPorRangos(Request $request){

        if($request->desde && $request->hasta) {
            $desde = $request->desde;
            $hasta = $request->hasta;
        } else {
            $desde = date('Y-m-d');
            $hasta = date('Y-m-d');
        }

        $ingresos = DB::select('call rtnIngresosPorRangoFechas( ? , ? )', array($desde , $hasta));
        return view('admin.charts.ventas-porrango', compact('ingresos'));
    }

}
