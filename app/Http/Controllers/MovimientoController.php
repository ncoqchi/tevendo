<?php
namespace App\Http\Controllers;
use App\Mail\EnviarCosEnvActCorreoPedidos;
use App\Mail\EnviarCorreoPedidoEntregado;
use App\Mail\EnviarCorreoPedidoEnviado;
use App\Mail\EnviarCorreoPedidoDepositoConfirmado;
use App\Mail\EnviarCorreoPedidoCancelado;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App;
use DB;
class MovimientoController extends Controller{


    public function getMovimientosPorProducto(Request $request, $idProducto) {

        $producto = App\Producto::findOrFail($idProducto);
        $movimientos= App\Movimiento::where('producto_id', $idProducto)->orderBy('created_at','desc')->get();
        return view('admin.movimientos', compact('producto','movimientos'));

    }

    public function registrarMovimiento(Request $request, $idProducto) {

        $request->validate([
            'tipo' => 'required',
            'cantidad' => 'required'
        ]);
        $movimiento = new App\Movimiento();
        $movimiento->cantidad = $request->cantidad;
        $movimiento->ingreso_salida = $request->tipo;
        $movimiento->producto_id = $idProducto;
        if($request->comentarios){
            $movimiento->comentario = $request->comentarios;
        }else{
            $movimiento->comentario = "Sin Comentarios";
        }
        $movimiento->save();
        $producto = App\Producto::findOrFail($idProducto);
        if($request->tipo == 1){
            //INGRESO
            $producto->stock = $producto->stock + $request->cantidad;
        }else{
            //SALIDA
            $producto->stock = $producto->stock - $request->cantidad;
        }
        $producto->save();
        return back()->with('mensaje', 'Se ha registrado el movimiento con Éxito!');
    }

//    public function getVentasPorAnio(Request $request) {
//        if($request->p) {
//            $anio = $request->p;
//        } else{
//            $anio = date('Y');
//        }
//        $ingresos = DB::select('call rtnGraficosIngresos( ? )', array($anio));
//        return view('admin.charts.ventas', compact('ingresos'));
//    }
//
//    public function getVentasPorRangos(Request $request){
//
//        if($request->desde && $request->hasta) {
//            $desde = $request->desde;
//            $hasta = $request->hasta;
//        } else {
//            $desde = date('Y-m-d');
//            $hasta = date('Y-m-d');
//        }
//
//        $ingresos = DB::select('call rtnIngresosPorRangoFechas( ? , ? )', array($desde , $hasta));
//        return view('admin.charts.ventas-porrango', compact('ingresos'));
//    }

}
