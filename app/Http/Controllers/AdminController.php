<?php
namespace App\Http\Controllers;

use App\Mail\EnviarCosEnvActCorreoPedidos;
use App\Mail\EnviarCorreoPedidoEntregado;
use App\Mail\EnviarCorreoPedidoEnviado;
use App\Mail\EnviarCorreoPedidoDepositoConfirmado;
use App\Mail\EnviarCorreoPedidoCancelado;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App;
class AdminController extends Controller{

    public function inicio(){
        return redirect(route('admin.productos'));
        //$productos = App\Producto::all();
        //return view('admin.productos', compact('productos'));
    }

    //***** CATEGORIA  *******
    public function nuevaCategoria(){
        return view('admin.nueva-categoria');
    }

    //***** CATEGORIA  *******
    public function getInformacion() {
        $info = App\Informacion::findOrFail(1);
        return view('admin.informacion',compact('info'));
    }

    public function actualizarInformacion(Request $request){
        $request->validate([
            'banco' => 'required',
            'cuenta' => 'required',
            'cci' => 'required',
            'whastapp' => 'required'
        ]);

        $info = App\Informacion::findOrFail(1);
        $info->banco = $request->banco;
        $info->cci = $request->cci;
        $info->cuenta = $request->cuenta;
        $info->whastapp = $request->whastapp;
        $info->facebook = $request->facebook;
        $info->instagram = $request->instagram;
        $info->save();
        return back()->with('mensaje', 'Información Actualizada Exitosamente!');
    }


    public function listarCategorias() {
//        $categorias = App\Categoria::all();
        $categorias = App\Categoria::orderBy('updated_at','desc' )->paginate(8);
        return view('admin.categorias', compact('categorias'));
    }

    public function editarCategoria($idCategoria) {
        $categoria = App\Categoria::findOrFail($idCategoria);
        return view('admin.editar-categoria', compact('categoria'));
    }

    public function eliminarCategoria($idCategoria){
        $categoria = App\Categoria::findOrFail($idCategoria);
        $categoria->delete();
        return back()->with('mensaje', 'Categoría Eliminada Exitosamente!');
    }

    public function actualizarCategoria(Request $request){
        $request->validate([
            'nombre' => 'required'
        ]);

        //QUEDA EL ELIMINAR CATEGORIA

        $categoria = App\Categoria::findOrFail($request->id);
        $categoria->nombre = strtoupper($request->nombre);
        if($request->hasFile('foto')){
            $file = $request->file('foto');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'cat_'.date('Ymd_His').$extension;
            $file->move(public_path().'/categorias/',$name);
            $categoria->imagen = '/categorias/'.$name;
        }

        $categoria->save();
        return back()->with('mensaje', 'Categoria Actualizada Exitosamente!');
    }

    public function registrarCategoria(Request $request){
        $request->validate([
            'nombre' => 'required',
            'foto' => 'required'
        ]);

        $categoria = new App\Categoria();
        $categoria->nombre = strtoupper($request->nombre);
        $name = 'no-image-product.jpg';

        if($request->hasFile('foto')){
            $file = $request->file('foto');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $name = 'cat_'.date('Ymd_His').$extension;
            $file->move(public_path().'/categorias/',$name);
        }

        $categoria->imagen = '/categorias/'.$name;
        $categoria->save();
        return back()->with('mensaje', 'Categoría Registrada Exitosamente!');
    }

    //***** TIPOS DE ENVÍOS *******
    public function nuevoTipoEnvio(){
        return view('admin.nuevo-tipo-envio');
    }

    public function editarTipoEnvio($idTipoEnvio) {
        $tipoEnvio = App\Tipoenvio::findOrFail($idTipoEnvio);
        return view('admin.editar-tipoenvio', compact('tipoEnvio'));
    }

    public function listarTiposEnvio(){
        $tiposenvios = App\Tipoenvio::all();
        return view('admin.tiposenvio', compact('tiposenvios'));
    }

    public function eliminarTipoEnvio($idTipoEnvio){
        $tipoEnvio = App\Tipoenvio::findOrFail($idTipoEnvio);
        $tipoEnvio->delete();
        return back()->with('mensaje', 'Tipo de Envío Eliminado Exitosamente!');
    }

    public function registrarTipoEnvio(Request $request){
        $request->validate([
            'nombre' => 'required'
        ]);

        $tipoEnvio = new App\Tipoenvio();
        $tipoEnvio->nombre = $request->nombre;
        $tipoEnvio->precio = $request->precio;
        $tipoEnvio->save();

        return back()->with('mensaje', 'Tipo de Envío Registrado Exitosamente!');
    }

    //******* PRODUCTO *******
    public function nuevoProducto() {
        $categorias = App\Categoria::all();
        $tipoenvios = App\Tipoenvio::all();
        return view('admin.nuevo-producto', compact('categorias','tipoenvios'));
    }

    public function listarProductos(Request $request) {
        $num_pag = 15;
        // $productos = [];
        // $productos = App\Producto::where('nombre','LIKE','%'.$request->q.'%')->get();
        // $distritos = App\Distrito::where('departamento_id', $idDepartamento)->where('provincia_id', $idProvincia)->get();
        $categorias = App\Categoria::orderBy('nombre')->get();

        if ($request->nombre) {
            if ( $request->categoria && $request->categoria != '0' ) {
                $productos = App\Producto::where('nombre','LIKE','%'.$request->nombre.'%')->where('categoria_id', $request->categoria)->where('estado','1')->orderBy('updated_at','desc')->paginate($num_pag);
            } else {
                $productos = App\Producto::where('nombre','LIKE','%'.$request->nombre.'%')->where('estado','1')->orderBy('updated_at','desc')->paginate($num_pag);
            }
        } else {
            if ( $request->categoria && $request->categoria != '0' ) {
                $productos = App\Producto::where('categoria_id', $request->categoria)->where('estado','1')->orderBy('updated_at','desc')->paginate($num_pag);
            } else {
                $productos = App\Producto::where('estado','1')->orderBy('updated_at','desc')->paginate($num_pag);
            }
        }

//        return $productos;
        return view('admin.productos', compact('productos','categorias'));
    }

    public function registrarProducto(Request $request) {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required | max : 191',
                'stock' => 'required | numeric |gt:0',
                'descripcion' => '',
                'precio' => 'required | numeric',
                'precioRebaja' => ['bail','numeric', Rule::requiredIf($request->flagRebaja == true),'lt:precio'],
                'imagen1' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect(route('admin.nuevo.producto'))->withErrors($validator)->withInput();
            }

            $producto = new App\Producto;
            $producto->nombre = ucwords(strtolower($request->nombre));
            if($request->descripcion == ''){
                $producto->descripcion = 'No se ha registrado ninguna descripción';
            }else {
                $producto->descripcion = $request->descripcion;
            }

            $producto->stock = $request->stock;
            $producto->precio = $request->precio;
            if($request->flagRebaja){
                $producto->precioRebaja = $request->precioRebaja;
            }else{
                $producto->precioRebaja = $request->precio;
            }
            $producto->tipoenvio_id = 1;
            $producto->categoria_id = $request->categoria;

            foreach (range('1', '6') as $indice) {
                if( $request->hasFile('imagen'.$indice) ) {
                    $file = $request->file('imagen'.$indice);
                    $originalname = $file->getClientOriginalName();
                    $extension =  strrchr($originalname, ".");
                    $nombreImagen = 'pro-'.str_replace(' ', '', $producto->nombre).'-img-'.$indice.'-'.date('Ymd-His').$extension;
                    $folder = '/productos/cat-'.$request->categoria;
                    $file->move(public_path().$folder ,$nombreImagen);
                    $producto['imagen'.$indice] = $folder.'/'.$nombreImagen;
                }
            }

            $producto->save();
            //REGISTRAR UN MOVIMIENTO
            $movimiento = new App\Movimiento();
            $movimiento->cantidad = $request->stock;
            $movimiento->ingreso_salida = "1";//INGRESO
            $movimiento->producto_id = $producto->id;
            $movimiento->comentario = "Se ha creado el producto con Stock Inicial de ".$producto->stock;
            $movimiento->save();
            DB::commit();
            return back()->with('mensaje', 'Producto Registrado Exitosamente, adicionalmente se ha registrado un movimiento de INGRESO en su Kardex');
        } catch (\Exception $exception) {
            DB::rollback();
            return back()->with('mensaje', 'No se pudo registrar el Producto, '.$exception->getMessage());
        }
    }

    public function editarProducto($idProducto) {

        $producto = App\Producto::where('id', $idProducto)->where('estado','1')->first();
        $categorias = App\Categoria::all();
        $tipoenvios = App\Tipoenvio::all();
        return view('admin.editar-producto', compact('producto','categorias','tipoenvios'));
    }

    public function eliminarProducto($idProducto){
        try {
            $producto = App\Producto::where('id', $idProducto)->where('estado','1')->first();
            $producto->estado = 0;
            $producto->save();
            return back()->with('mensaje', 'Producto Eliminado Exitosamente!');
        } catch (Throwable  $e) {
            return back()->with('mensaje', 'No se pudo eliminar producto, antes, eliminar su comentarios asciados!');
        }

    }

    public function actualizarProducto(Request $request, $idProducto){

        $validator = Validator::make($request->all(), [
            'nombre' => 'required | max : 191',
//            'stock' => 'required | numeric |gt:0',
            'descripcion' => '',
            'precio' => 'required | numeric',
            'precioRebaja' => ['bail','numeric', Rule::requiredIf($request->flagRebaja == true),'lt:precio'],
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.editar.producto',$idProducto))->withErrors($validator)->withInput();
        }

        $producto = App\Producto::where('id', $idProducto)->where('estado','1')->first();
        $producto->nombre = ucwords(strtolower($request->nombre));
        if($request->descripcion == ''){
            $producto->descripcion = 'No se ha registrado ninguna descripcion';
        }else {
            $producto->descripcion = $request->descripcion;
        }

//        $producto->stock = $request->stock;
        $producto->precio = $request->precio;
        if($request->flagRebaja){
            $producto->precioRebaja = $request->precioRebaja;
        }else{
            $producto->precioRebaja = $request->precio;
        }
        $producto->categoria_id = $request->categoria;

        foreach (range('1', '6') as $indice) {
            if( $request->hasFile('imagen'.$indice) ) {
                $file = $request->file('imagen'.$indice);
                $originalname = $file->getClientOriginalName();
                $extension =  strrchr($originalname, ".");
                $nombreImagen = 'pro-'.str_replace(' ', '', $producto->nombre).'-img-'.$indice.'-'.date('Ymd-His').$extension;
                $folder = '/productos/cat-'.$request->categoria;
                $file->move(public_path().$folder ,$nombreImagen);
                $producto['imagen'.$indice] = $folder.'/'.$nombreImagen;
            }
        }

        $producto->save();
        return back()->with('mensaje', 'Producto Actualizado Exitosamente!');
    }


    //PEDIDOS PENDIENTE - PEDIDOS PENDIENTES
    public function getpedidosPendientes(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idPedido = str_replace("PED-", "", $request->num_pedido);
            //$idrequest = intval(substr($request->num_pedido,4));
            $idrequest = intval($idPedido);
            $recibos = App\Recibo::where('id', $idrequest)->where('estado','1')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '1')->orderBy('updated_at', 'desc')->paginate($num_pag);
        }

        return view('admin.ventas.pedidos-pendientes', compact('recibos'));
    }

    public function editarPedidoPendiente($idPedido) {
        try {
            $recibo = App\Recibo::where('id', $idPedido)->where('estado','1')->first();
            $detalle =  App\Detallerecibo::where('recibo_id', $idPedido)->get();
            return view('admin.ventas.pedido-pendiente', compact('recibo','detalle'));
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
//            if(isHttpException($exception)) {
//                $code = $exception->getStatusCode();
//                if( $code == '404' ) {
//                    return view('pagina.404');
//                }
//            }
        }
    }

    public function actualizarCostoEnvioDePedidoPendiente(Request $request, $idPedido, $costoenvio) {
        $mensaje = array();
        try {
            $recibo = App\Recibo::findOrFail($idPedido);
            $recibo->costoenvio = $costoenvio;
            $recibo->total_final = $recibo->total_final + $costoenvio;
            $recibo->estado = 2;
            $recibo->save();
            //enviando correo
            $venta = (object)[];
            $venta->recibo = $recibo;
            $venta->detalle = App\Detallerecibo::where('recibo_id',$recibo->id)->get();
            $venta->path = str_replace('/'.$request->path(), "", $request->url());
            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
            $venta->envio = $envio;
            $venta->id = $recibo->id;
            $info = App\Informacion::findOrFail(1);
            $venta->info = $info;
            Mail::to($envio->correo_electronico)->queue(new EnviarCosEnvActCorreoPedidos($venta));
            $mensaje['code'] = '1';
            $mensaje['correo'] = $envio->correo_electronico;
            $mensaje['nombres'] = $envio->nombres;
            $mensaje['celular'] = $envio->celular;
            $mensaje['costoenvio'] = $costoenvio;
            $mensaje['error'] = '';
            return json_encode($mensaje);
            return json_encode($mensaje);
            //return json \response($mensaje, 200);
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
            $mensaje['code'] = '-1';
            $mensaje['detalle'] = 'Ocurrio un error en actualizarCostoEnvioDePedidoPendiente, consulte al administrador';
            $mensaje['error'] = $exception;
            return \response($mensaje, 500);
        }
    }

//    public function enviarCorreoNoticandoleCostoDeEnvio($recibo , $request) {
//        try {
//            $venta = (object)[];
//            $venta->recibo = $recibo;
//            $venta->detalle = App\Detallerecibo::where('recibo_id',$recibo->id)->get();
//            $venta->path = str_replace('/'.$request->path(), "", $request->url());
//            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
//            $venta->envio = $envio;
//            $venta->id = $recibo->id;
//            // $destinoCorreoCliente =
//            Mail::to($envio->correo_electronico)->queue(new EnviarCosEnvActCorreoPedidos($venta));
//        } catch (\Exception $e) {
//            \Log::debug('Debug por exception ' . $e->getMessage());
//        }
//    }

    // PEDIDOS CON COSTO DE ENVIO
    public function getpedidosConCostoEnvio(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idPedido = str_replace("PED-", "", $request->num_pedido);
//            $idrequest = intval(substr($request->num_pedido,4));
            $idrequest = intval($idPedido);
            $recibos = App\Recibo::where('id', $idrequest)->where('estado', '2')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '2')->orderBy('updated_at',  'desc')->paginate($num_pag);
        }
        return view('admin.ventas.pedidos-concostoenvio', compact('recibos'));
    }


    ////////ADJUNTAR VOUCHER
    public function getPedidoParaAdjuntarVocher($idPedido) {
        try {
            //solo busca los tipos de envio Contra Entrega
            $recibo = App\Recibo::findOrFail($idPedido);
            //$recibo = App\Recibo::where('id', $idPedido)->where('tipoenvio','2')->where('estado','2')->first();
            $detalle =  App\Detallerecibo::where('recibo_id', $idPedido)->get();
            return view('admin.ventas.pedido-adjuntar-voucher', compact('recibo','detalle'));
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
        }
    }

    public function actualizarPedidoCostoAsigYDepositoEnCuenta(Request $request, $idPedido ){
        $request->validate([
            'imagen' => 'required'
        ]);
        $recibo = App\Recibo::findOrFail($idPedido);
        //6(depositado) y validado el voucher
//        if( $request->hasFile('imagen') ) {
            $file = $request->file('imagen');
            $originalname = $file->getClientOriginalName();
            $extension =  strrchr($originalname, ".");
            $nombreImagen = 'rec-'.$recibo->id.date('Ymd-His').$extension;
            $folder = '/voucher';
            $file->move(public_path().$folder ,$nombreImagen);
            $recibo->estado = 6;
            $recibo->imagen_voucher = $folder.'/'.$nombreImagen;
//        }
        $venta = (object)[];
        $venta->recibo = $recibo;
        $venta->detalle = App\Detallerecibo::where('recibo_id',$idPedido)->get();
        $venta->path = str_replace('/'.$request->path(), "", $request->url());
        $envio = App\Envio::where('recibo_id', $recibo->id)->first();
        $venta->envio = $envio;
        $venta->id = $idPedido;
        Mail::to($envio->correo_electronico)->queue(new EnviarCorreoPedidoDepositoConfirmado($venta));
//        CORREO INDICANDO QUE SU PEDIDO SE HA CONFIRMADO
        $recibo->save();
        return back()->with('mensaje', 'Se adjunto el Voucher Satisfactoriamente, se enviará un correo al cliente indicado que su Depósito se ha Confirmado!');
    }

    public function actualizarPedidoConCVaEnviado(Request $request,  $id) {

        $mensaje = array();
        try {
            $recibo = App\Recibo::findOrFail($id);
            $recibo->estado = 3;
            $recibo->save();
            //enviando correo
            $venta = (object)[];
            $venta->recibo = $recibo;
            $venta->detalle = App\Detallerecibo::where('recibo_id',$id)->get();
            $venta->path = str_replace('/'.$request->path(), "", $request->url());
            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
            $venta->envio = $envio;
            $venta->id = $id;
            Mail::to($envio->correo_electronico)->queue(new EnviarCorreoPedidoEnviado($venta));
            $mensaje['code'] = '1';
            $mensaje['correo'] = $envio->correo_electronico;
            $mensaje['nombres'] = $envio->nombres;
            $mensaje['celular'] = $envio->celular;
            $mensaje['error'] = '';
            return json_encode($mensaje);
            //return json \response($mensaje, 200);
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
            $mensaje['code'] = '-1';
            $mensaje['detalle'] = 'Ocurrio un error al registrar su pedido, consulte al administrador';
            $mensaje['error'] = $exception;
            return \response($mensaje, 500);
        }

    }



    // PEDIDOS ENVIADOS
    public function getPedidosEnviados(Request $request) {
        $num_pag = 15;

        if ($request->num_pedido) {
            $idPedido = str_replace("PED-", "", $request->num_pedido);
            // $idrequest = intval(substr($request->num_pedido,4))
            $idrequest = intval($idPedido);
            $recibos = App\Recibo::where('id', $idrequest)->where('estado', '3')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '3')->orderBy('updated_at', 'desc')->paginate($num_pag);
        }

        return view('admin.ventas.pedidos-enviados', compact('recibos'));
    }


    // PEDIDOS ENTREGAODS
    public function getPedidosEntregados(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idrequest = intval(substr($request->num_pedido,4));
            $recibos = App\Recibo::where('id', $idrequest)->where('estado', '4')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '4')->orderBy('updated_at',  'desc')->paginate($num_pag);
        }
        return view('admin.ventas.pedidos-entregados', compact('recibos'));

    }

    public function actualizarPedidoEnviadoaEntregado(Request $request, $id) {

        $mensaje = array();
        try {
            $recibo = App\Recibo::findOrFail($id);
            $recibo->estado = 4; //estado 4 es entregado
            $recibo->save();
            //enviando correo
            $venta = (object)[];
            $venta->recibo = $recibo;
            $venta->detalle = App\Detallerecibo::where('recibo_id',$id)->get();
            $venta->path = str_replace('/'.$request->path(), "", $request->url());
            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
            $venta->envio = $envio;
            $venta->id = $id;
            Mail::to($envio->correo_electronico)->queue(new EnviarCorreoPedidoEntregado($venta));
            $mensaje['code'] = '1';
            $mensaje['correo'] = $envio->correo_electronico;
            $mensaje['nombres'] = $envio->nombres;
            $mensaje['celular'] = $envio->celular;
            $mensaje['error'] = '';
            return json_encode($mensaje);
            //return json \response($mensaje, 200);
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
            $mensaje['code'] = '-1';
            $mensaje['detalle'] = 'Ocurrio un error en actualizarPedidoEnviadoaEntregado, consulte al administrador';
            $mensaje['error'] = $exception;
            return \response($mensaje, 500);
        }

    }

    //************************************************************************************
    // PEDIDOS DEPOSITADOS SIN CONFIRMAR  estado = 6
    //************************************************************************************
    public function getPedidosDepositados(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idrequest = intval(substr($request->num_pedido,4));
            $recibos = App\Recibo::where('id', $idrequest)->where('estado','6')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '6')->orderBy('updated_at','desc')->paginate($num_pag);
        }
        return view('admin.ventas.pedidos-depositados', compact('recibos'));

    }



    //************************************************************************************
    // PEDIDOS DEPOSITADOS CONFIRMADOS  estado = 7
    //************************************************************************************
    public function getPedidosDepositadosConfirmados(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idrequest = intval(substr($request->num_pedido,4));
            $recibos = App\Recibo::where('id', $idrequest)->where('estado','7')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '7')->orderBy('updated_at','desc')->paginate($num_pag);
        }
        return view('admin.ventas.pedidos-depositados-confirmados', compact('recibos'));
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getPedidosCancelados(Request $request) {
        $num_pag = 15;
        if($request->num_pedido) {
            $idPedido = str_replace("PED-", "", $request->num_pedido);
            //$idrequest = intval(substr($request->num_pedido,4));
            $idrequest = intval($idPedido);
            $recibos = App\Recibo::where('id', $idrequest)->where('estado','9')->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('estado', '9')->orderBy('updated_at', 'desc')->paginate($num_pag);
        }

        return view('admin.ventas.pedidos-cancelados', compact('recibos'));

    }

    //CANCELAR UN PEDIDO
    public function actualizarPedidoCancelado(Request $request, $idPedido, $motivo) {
        $mensaje = array();
        try {
            $recibo = App\Recibo::findOrFail($idPedido);
            $recibo->estado = 9;
            $recibo->motivo_cancelacion = $motivo;
            $recibo->save();

            $detallerecibo = App\Detallerecibo::where('recibo_id',$recibo->id)->get();

            //DEVOLVER LOS PRODUCTOS AL ALMACEN
            foreach ($detallerecibo as $detalle){
                //SE REGISTRA UN MOVIMIENTO DE SALIDA
                $movimiento = new App\Movimiento();
                $movimiento->cantidad =  $detalle->cantidad;
                $movimiento->ingreso_salida = "1";//INGRESO POR CANCELACION
                $movimiento->producto_id = $detalle->producto_id;
                $movimiento->comentario = "Se canceló el Pedido N° ".$recibo->id." - Se regresó ".$detalle->cantidad." uni. del producto ".$detalle->detalle;
                $movimiento->save();

                //EL STOCK AUMENTA AL CANCELAR EL PEDIDO
                $producto = App\Producto::findOrFail($detalle->producto_id);
                $producto->stock = $producto->stock + $detalle->cantidad;
                $producto->save();
            }

            // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
            // enviando correo
            $venta = (object)[];
            $venta->recibo = $recibo;
            $venta->detalle = App\Detallerecibo::where('recibo_id',$idPedido)->get();
            $venta->path = str_replace('/'.$request->path(), "", $request->url());
            $envio = App\Envio::where('recibo_id', $recibo->id)->first();
            $venta->envio = $envio;
            $venta->id = $idPedido;
            Mail::to($envio->correo_electronico)->queue(new EnviarCorreoPedidoCancelado($venta));
            $mensaje['code'] = '1';
            $mensaje['correo'] = $envio->correo_electronico;
            $mensaje['nombres'] = $envio->nombres;
            $mensaje['celular'] = $envio->celular;
            $mensaje['error'] = '';
            return json_encode($mensaje);
            //return json \response($mensaje, 200);

        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
            $mensaje['code'] = '-1';
            $mensaje['detalle'] = 'Ocurrio un error al registrar su pedido, consulte al administrador';
            $mensaje['error'] = $exception;
            return \response($mensaje, 500);
        }
    }


//    public function getVentasPorAnio(){
//
//        return view('admin.charts.ventas');
//
//    }


}
