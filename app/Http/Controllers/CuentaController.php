<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CuentaController extends Controller
{

    public function __construct() {
//        $this->middleware('auth');
    }

    public function index(Request $request){

        $num_pag = 15;
        if($request->num_pedido) {
            $idPedido = str_replace("PED-", "", $request->num_pedido);
            //$idrequest = intval(substr($request->num_pedido,4));
            $idrequest = intval($idPedido);
            $recibos = App\Recibo::where('id', $idrequest)->paginate($num_pag);
        } else {
            $recibos = App\Recibo::where('user_id',Auth::user()->id)->orderBy('created_at', 'desc')->paginate($num_pag);
        }
        $info = App\Informacion::findOrFail(1);
        return view('cuenta.mispedidos', compact('recibos','info'));
    }


    public function getPedidoCostoAsigYDepositoEnCuenta($idPedido) {
        try {
//            $recibo = App\Recibo::where('id', $idPedido)->where('user_id', Auth::user()->id)->where('tipoenvio', 2)->where('estado','2')->first();
            $recibo = App\Recibo::findOrFail($idPedido);
            $detalle =  App\Detallerecibo::where('recibo_id', $idPedido)->get();
            return view('cuenta.pedido', compact('recibo','detalle'));
        } catch (\Exception $exception) {
            \Log::debug('Debug por exception ' . $exception->getMessage());
        }
    }






    public function getPedidos(Request $request) {
        //->paginate(12);
        //Filtrarlos por usuario
        $categorias = App\Categoria::all();
        if ($request->busqueda){
            if ($request->busqueda=='0'){
                $recibos = App\Recibo::orderBy('created_at', 'desc')->paginate(10);
            }else{
                $recibos = App\Recibo::where('estado', $request->busqueda)->orderBy('created_at', 'desc')->paginate(10);
            }
        }else{
            if($request->num_pedido){
                $idrequest = intval(substr($request->num_pedido,4));
                $recibos = App\Recibo::where('id', $idrequest)->paginate(10);
            }else {
                $recibos = App\Recibo::orderBy('created_at', 'desc')->paginate(10);
            }
        }
        return $recibos;
    }

    public function verProfile (){
        $categorias = App\Categoria::all();
        $info = App\Informacion::findOrFail(1);
        return view('cuenta.perfil',compact('categorias','info'));
    }

    public function updateProfile (Request $request) {
        $user = Auth::user();
        $name = '/avatar/avatar-default.png';

        if( $request->hasFile('avatar') ) {
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $user->avatar = '/avatar/'.$name;
            $file->move(public_path().'/avatar/',$name);
        } else {
            $user->avatar = $name;
        }

        $user->save();
        return back()->with('mensaje', 'Perfil Actualizado Correctamente.!');
    }

}
