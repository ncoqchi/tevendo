<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;
use function GuzzleHttp\Psr7\str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * Return Productos en Oferta para la pantalla principal
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index() {
    //      $productos = App\Producto::all();
    //      $productos = App\Producto::where('nombre','LIKE','%'.$variable.'%')->get();
    //      $productos = App\Producto::where('nombre','LIKE','%ZAP%')->get();
            //$productos = App\Producto::where('precioRebaja', '100')->get();
    //        $productos = App\Producto::where('precioRebaja', '<','precio')->get();
    //        $productos = App\Producto::whereRaw('precioRebaja < precio')->paginate(12);
            $info = App\Informacion::findOrFail(1);
            $categorias = App\Categoria::orderBy('updated_at','desc')->take(5)->get();

            foreach ($categorias as $cat) {
                $productos = App\Producto::where('categoria_id', $cat->id)->orderBy('updated_at','desc')->take(5)->get();
                $cat->productos = $productos;
            }

            $sliders = App\Slider::all();
            $titulo = 'Nuestras Ofertas!';
    //        $productos = App\Producto::whereRaw('precioRebaja < precio')->paginate(12);
    //          $productos = App\Producto::cursor()->filter(function ($producto) {
    //              return $producto->precioRebaja < $producto->precio;
    //          });
            return view('home', compact('categorias','titulo','sliders','info'));
        }

    public function detalleProducto(Request $request, $idproducto) {

//        $categorias = App\Categoria::all();
        $info = App\Informacion::findOrFail(1);
        $producto = App\Producto::findOrFail($idproducto);
        $producto->categoria;
        // $comentarios = App\Comentario::all();
        if ($request->ordenar_comentarios) {
            if($request->ordenar_comentarios == 'mas_nueva'){
                $comentarios = App\Comentario::where('producto_id', $idproducto)->orderBY('created_at','desc')->get();

            } else if($request->ordenar_comentarios == 'mas_antigua'){
                $comentarios = App\Comentario::where('producto_id', $idproducto)->orderBY('created_at','asc')->get();

            } else if($request->ordenar_comentarios == 'alta_calificacion'){
                $comentarios = App\Comentario::where('producto_id', $idproducto)->orderBY('calificacion','desc')->get();

            } else {
                //baja calificacion
                $comentarios = App\Comentario::where('producto_id', $idproducto)->orderBY('calificacion','asc')->get();
            }
        } else {
            $comentarios = App\Comentario::where('producto_id', $idproducto)->orderBY('created_at','desc')->get();

        }
        //User::orderBy('created_at', 'desc')->orderBy('something', 'asc');
        //substr(string,start,length)
        $imagenesparse = substr($producto->imagen, 0 , strlen($producto->imagen) -1);
//        $imagenes = explode("," , $imagenesparse);
        // la magia de los comentarios
        $calificaciones = array();
        $calificacion_una = 0;
        $calificacion_dos = 0;
        $calificacion_tres = 0;
        $calificacion_cuatro = 0;
        $calificacion_cinco = 0;

        foreach ($comentarios as $comentario) {
            $comentario->liked='none';

            if(Auth::user()){
                $likedEncontrado = App\Liked::where('comentario_id', $comentario->id)->where('user_id', Auth::user()->id)->first();
                if($likedEncontrado){
                    $comentario->liked='unlike';
                    if( $likedEncontrado->like == 1 ) {
                        $comentario->liked='like';
                    }
                }
            }

            if ($comentario->calificacion == 1){
                $calificacion_una = $calificacion_una + 1;
            }else if ($comentario->calificacion == 2){
                $calificacion_dos = $calificacion_dos + 1;
            }else if ($comentario->calificacion == 3){
                $calificacion_tres = $calificacion_tres + 1;
            }else if ($comentario->calificacion == 4){
                $calificacion_cuatro = $calificacion_cuatro + 1;
            }else {
                $calificacion_cinco = $calificacion_cinco + 1;
            }
        }

        $total_calificaciones = sizeof($comentarios);
        $calificaciones_aprobadas = $calificacion_cuatro + $calificacion_cinco;
        $porcentaje_calificaciones_aprobadas = 0 ;
        $calificaciones['una'] = $calificacion_una;
        $calificaciones['por_una'] = 0;
        $calificaciones['dos'] = $calificacion_dos;
        $calificaciones['por_dos'] = 0;
        $calificaciones['tres'] = $calificacion_tres;
        $calificaciones['por_tres'] = 0;
        $calificaciones['cuatro'] = $calificacion_cuatro;
        $calificaciones['por_cuatro'] = 0;
        $calificaciones['cinco'] = $calificacion_cinco;
        $calificaciones['por_cinco'] = 0;

        if ($total_calificaciones > 0) {

            $porcentaje_calificaciones_aprobadas = round(($calificaciones_aprobadas / $total_calificaciones) * 100, 0);
            $calificaciones['por_una'] = round(($calificacion_una / $total_calificaciones) * 100, 0);
            $calificaciones['por_dos'] = round(($calificacion_dos / $total_calificaciones) * 100, 0);
            $calificaciones['por_tres'] = round(($calificacion_tres / $total_calificaciones) * 100, 0);
            $calificaciones['por_cuatro'] = round(($calificacion_cuatro / $total_calificaciones) * 100, 0);
            $calificaciones['por_cinco'] = round(($calificacion_cinco / $total_calificaciones) * 100, 0);

        }

        $calificaciones['total'] = $total_calificaciones;
        $calificaciones['aprobadas'] = $calificaciones_aprobadas;
        $calificaciones['porcentaje'] = $porcentaje_calificaciones_aprobadas;
//        $calificaciones['promedio_total']= round((1*$calificaciones['por_una'] + 2*$calificaciones['por_dos'] + 3*$calificaciones['por_tres'] + 4*$calificaciones['por_cuatro'] + 5*$calificaciones['por_cinco']) / 100,2);
        //print_r($producto);
        return view('producto-detalle', compact('producto','comentarios','calificaciones','info'));
    }


    public function listarTodosLosProductos(Request $request) {
//        $query_in = $request->categorias;
//        $categorias = App\Categoria::all();
        //AJAX
//        return $productos;
        $info = App\Informacion::findOrFail(1);
        return view('productos',compact('info'));
    }

    public function listarTodosLosProductosPorNombre(Request $request) {
        $productos = App\Producto::where('nombre','LIKE','%'.$request->q.'%')->OrderBy('updated_at','desc')->get();
//        $categorias = App\Categoria::all();
        return view('productos', compact('productos'));
    }

    public function listarTodasLasOfertas(Request $request) {
        //$query_in = $request->categorias;
//        $categorias = App\Categoria::all();
        //AJAX
        //return $productos;
        $info = App\Informacion::findOrFail(1);
        return view('productos-ofertas', compact('info'));
    }

    public function listarProductosPorCategoria($idCategoria) {
//        $productos = App\Producto::where('categoria_id', $idCategoria)->get();
        $categoria = App\Categoria::findOrFail($idCategoria);
        $productos = App\Producto::where('categoria_id', $idCategoria)->orderBy('updated_at','desc')->paginate(3);
//        $categorias = App\Categoria::all();
        $info = App\Informacion::findOrFail(1);
        return view('productos-por-categoria', compact('productos','categoria','info'));
    }

    public function carrito() {
        $categorias = App\Categoria::all();
        $info = App\Informacion::findOrFail(1);
        return view('carrito', compact('categorias','info'));
        // return view('home');
    }

    public function comprar() {
        $categorias = App\Categoria::all();
        $info = App\Informacion::findOrFail(1);
        return view('comprar', compact('categorias','info'));
        // return view('home');
    }

    public function getAllProductosOfertas() {
        //$productos = App\Producto::all();
        $productos = App\Producto::whereRaw('precioRebaja < precio')->paginate(6);
//        $productos = App\Producto::all()->with('categoria');
        foreach ($productos as $pro) {
            $pro->categoria;
        }
        return $productos;
    }

    public function getAllProductos() {
        //$productos = App\Producto::all();
        $productos = App\Producto::paginate(12);
//        $productos = App\Producto::all()->with('categoria');
        foreach ($productos as $pro) {
            $pro->categoria;
        }
        return $productos;
    }

    public function getAllCategorias() {
        $categorias = App\Categoria::all();
        return $categorias;
        // return view('home');
    }

    public function prueba(Request $request, $idProducto) {
        //return $request->header();
        $bodytag = str_replace('/'.$request->path(), "", $request->url());
        return $bodytag;
        //return view('prueba');
        // return view('home');
    }

    public function getUserInfo(){
        $user = Auth::user();
        return $user;
    }

}
