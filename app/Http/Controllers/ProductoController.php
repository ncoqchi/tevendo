<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $query_in = $request->categorias;
        $paginate = 12;

        if($request->enoferta == 'on') {
            if($request->orden == 'bprecio'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereRaw('precioRebaja < precio')->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('precioRebaja','asc')->paginate($paginate);
            }else if($request->orden == 'aprecio'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereRaw('precioRebaja < precio')->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('precioRebaja','desc')->paginate($paginate);
            }else if($request->orden == 'az'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereRaw('precioRebaja < precio')->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('nombre','asc')->paginate($paginate);
            }else if($request->orden == 'za'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereRaw('precioRebaja < precio')->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('nombre','desc')->paginate($paginate);
            }else if($request->orden == 'ultimo'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereRaw('precioRebaja < precio')->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('updated_at','desc')->paginate($paginate);
            }
        }else{
            if($request->orden == 'bprecio'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('precioRebaja','asc')->paginate($paginate);
            }else if($request->orden == 'aprecio'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('precioRebaja','desc')->paginate($paginate);
            }else if($request->orden == 'az'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('nombre','asc')->paginate($paginate);
            }else if($request->orden == 'za'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('nombre','desc')->paginate($paginate);
            }else if($request->orden == 'ultimo'){
                $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->where('estado','1')->orderBY('updated_at','desc')->paginate($paginate);
            }
        }

////        $categorias = App\Categoria::all();
//
////        foreach ($categorias as $categoria) {
////            if ($request['cat_'.$categoria->id]){
////                $query_in = $query_in.$categoria->id.',';
////            }
////        }
//        if($query_in != "") {
//            $query_in = substr($query_in, 0 , strlen($query_in) -1);
//            if($request->minPrecio){
//                // $productos = App\Producto::where('categoria_id', $idCategoria)->paginate(12);
//                if($request->orden) {
//                    if($request->orden == 'bprecio'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->orderBY('precioRebaja','asc')->paginate($paginate);
//                    }else if($request->orden == 'aprecio'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->orderBY('precioRebaja','desc')->paginate($paginate);
//                    }else if($request->orden == 'az'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->orderBY('nombre','asc')->paginate($paginate);
//                    }else if($request->orden == 'za'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->orderBY('nombre','desc')->paginate($paginate);
//                    }else if($request->orden == 'ultimo'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->orderBY('updated_at','desc')->paginate($paginate);
//                    }
//                }
////                else {
////                    //default de bajo a alto precio
////                    $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->whereBetween('precio', [$request->minPrecio, $request->maxPrecio])->paginate($paginate);
////                }
//            }else{
//                //solo filtrado por categorias
//                if($request->orden) {
//                    if($request->orden == 'bprecio'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->orderBY('precioRebaja','asc')->paginate($paginate);
//                    }else if($request->orden == 'aprecio'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->orderBY('precioRebaja','desc')->paginate($paginate);
//                    }else if($request->orden == 'az'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->orderBY('nombre','asc')->paginate($paginate);
//                    }else if($request->orden == 'za'){
//                        $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->orderBY('nombre','desc')->paginate($paginate);
//                    }
//                } else {
//                    //default de bajo a alto precio
//                    $productos = App\Producto::whereIn('categoria_id', explode(',', $query_in))->paginate($paginate);
//                }
//
//            }
//        } else {
//            if($request->orden) {
//                if($request->orden == 'bprecio'){
//                    $productos = App\Producto::orderBY('precioRebaja','asc')->paginate($paginate);
//                }else if($request->orden == 'aprecio'){
//                    $productos = App\Producto::orderBY('precioRebaja','desc')->paginate($paginate);
//                }else if($request->orden == 'az'){
//                    $productos = App\Producto::orderBY('nombre','asc')->paginate($paginate);
//                }else if($request->orden == 'za'){
//                    $productos = App\Producto::orderBY('nombre','desc')->paginate($paginate);
//                }
//            } else {
//                //default de bajo a alto precio
//                $productos = App\Producto::orderBY('updated_at','desc')->paginate($paginate);
//            }
//        }

        foreach ($productos as $pro) {
            $pro->categoria;
        }

        return $productos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

//        $pedido = array();
//        $recibo = App\Recibo::findOrFail($id);
//        $detallerecibo = App\Detallerecibo::where('recibo_id', $id)->get();
//        $envio = App\Envio::where('recibo_id', $id)->get();
//        $pedido['recibo'] = $recibo;
//        $pedido['detalle'] = $detallerecibo;
//        $pedido['envio'] = $envio;
//        return $pedido;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    public function getStockActual(Request $request , $idProducto) {
        $producto = App\Producto::findOrFail($idProducto);
        return $producto->stock;
    }

    public function validarStockdePedido(Request $request) {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
