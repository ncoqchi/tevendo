<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;

class ComentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'calificacion' => 'required | numeric',
            'comentario' => 'required | min:0 | max:190'
        ]);

        if ($validator->fails()) {
            return redirect(route('producto.detalle',$request->producto_id))->withErrors($validator)->withInput();
        }
        $mensaje = array();
        $comentario = new App\Comentario();
        $comentario->texto = $request->comentario;
        $comentario->calificacion = $request->calificacion;
        $comentario->user_id = Auth::user()->id;
        $comentario->producto_id = $request->producto_id;
        $comentario->save();
        //CALCULAR PORCENTAJE PROMEDIO DE COMENTARIOS
        $producto = App\Producto::find($request->producto_id);
        if( $producto ) {
            $producto->rating = $this->calcularRating($request->producto_id);
            $producto->save();
        }
        $mensaje['code'] = '1';
        $mensaje['detalle'] = 'Se registro su pedido satisfactoriamente, en breve le llegara un correo de su pedido';
        //$this->enviarCorreo($request);
        //return $mensaje;
        return back()->with('mensaje', 'Comentario Registrado Exitosamente!');
    }

    function calcularRating ($idproducto) {

        $comentarios = App\Comentario::where('producto_id', $idproducto)->get();
        $calificacion_una = 0;
        $calificacion_dos = 0;
        $calificacion_tres = 0;
        $calificacion_cuatro = 0;
        $calificacion_cinco = 0;

        foreach ($comentarios as $comentario) {
            if ($comentario->calificacion == 1){
                $calificacion_una = $calificacion_una + 1;
            }else if ($comentario->calificacion == 2){
                $calificacion_dos = $calificacion_dos + 1;
            }else if ($comentario->calificacion == 3){
                $calificacion_tres = $calificacion_tres + 1;
            }else if ($comentario->calificacion == 4){
                $calificacion_cuatro = $calificacion_cuatro + 1;
            }else {
                $calificacion_cinco = $calificacion_cinco + 1;
            }
        }
        $total_calificaciones = sizeof($comentarios);
        if($total_calificaciones > 0){
            $calificacion_por_una = round(($calificacion_una / $total_calificaciones) * 100, 0);
            $calificacion_por_dos = round(($calificacion_dos / $total_calificaciones) * 100, 0);
            $calificacion_por_tres = round(($calificacion_tres / $total_calificaciones) * 100, 0);
            $calificacion_por_cuatro = round(($calificacion_cuatro / $total_calificaciones) * 100, 0);
            $calificacion_por_cinco = round(($calificacion_cinco / $total_calificaciones) * 100, 0);
            $calificaciones_total = round((1*$calificacion_por_una + 2*$calificacion_por_dos + 3*$calificacion_por_tres + 4*$calificacion_por_cuatro + 5*$calificacion_por_cinco) / 100,0);
            return $calificaciones_total;
        } else {
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = array();
        $recibo = App\Recibo::findOrFail($id);
        $detallerecibo = App\Detallerecibo::where('recibo_id', $id)->get();
        $envio = App\Envio::where('recibo_id', $id)->get();

        $pedido['recibo'] = $recibo;
        $pedido['detalle'] = $detallerecibo;
        $pedido['envio'] = $envio;

        return $pedido;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
