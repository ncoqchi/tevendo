<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\EnviarCorreoVentas;
use Illuminate\Support\Facades\DB;
class LikedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //VERIFICAR SI EXISTE
        $respuesta = new class{};
//        $mensaje = object();
        $respuesta->code = '1';
        $respuesta->mensaje = 'Ya votaste por este comentario';
        $likedEncontrado = App\Liked::where('comentario_id', $request->comentario_id)->where('user_id', Auth::user()->id)->first();
        $liked = null;
        if( $likedEncontrado == null ) {
            $liked = new App\Liked;
            $liked->comentario_id = $request->comentario_id;
            $liked->user_id = Auth::user()->id;
            $comentario = App\Comentario::find($request->comentario_id);
            if($request->liked == 'like') {
                $liked->like = 1;
                $liked->unlike = 0;
                $comentario->like = $comentario->like + 1;
                $respuesta->code = '2';
                $respuesta->new_like = $comentario->like;
                $respuesta->mensaje = 'Se registro tu like para este comentario';
            } else {
                //Not like
                $liked->like = 0;
                $liked->unlike = 1;
                $comentario->notlike = $comentario->notlike + 1;
                $respuesta->code = '3';
                $respuesta->new_like = $comentario->notlike;
                $respuesta->mensaje = 'Se registro tu dislike para este comentario';
            }
            //Return View::make('profile')
            //->with('user', $user)
            //->with('workout', $workout);
            $liked->save();
            $comentario->save();
        }
            //return json_encode($mensaje);
        return response(json_encode($respuesta),200)->header('Content-Type', 'application/json');
            //App::abort(404);
            //return $liked;
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $pedido = array();
//        $recibo = App\Recibo::findOrFail($id);
//        $detallerecibo = App\Detallerecibo::where('recibo_id',$id)->get();
//        $envio = App\Envio::where('recibo_id',$id)->get();
//        $pedido['recibo'] = $recibo;
//        $pedido['detalle'] = $detallerecibo;
//        $pedido['envio'] = $envio;
//        return $pedido;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
