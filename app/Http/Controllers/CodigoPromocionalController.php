<?php
namespace App\Http\Controllers;

use App\Mail\EnviarCosEnvActCorreoPedidos;
use App\Mail\EnviarCorreoPedidoEntregado;
use App\Mail\EnviarCorreoPedidoEnviado;
use App\Mail\EnviarCorreoPedidoDepositoConfirmado;
use App\Mail\EnviarCorreoPedidoCancelado;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App;
use DB;
class CodigoPromocionalController extends Controller{


    public function getCodigos(Request $request){
        $codigos =  App\Promocion::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.codigosPromocionales', compact('codigos'));
    }

    public function registrarCodigo(Request $request){
        $promocion = new App\Promocion();
        $promocion->codigo = strtoupper(Str::random(12));
        $promocion->monto = $request->monto;
        $promocion->modo = $request->modo;
        $promocion->expiracion = $request->fechaExpiracion;
        $promocion->estado = 1;
        $promocion->save();
        return back()->with('mensaje', 'Código Promocional registrado exitosamente');
    }

    public function eliminarCodigo(Request $request, $idCodigo){
        $codigo = App\Promocion::findOrFail($idCodigo);
        $codigo->delete();
        return back()->with('mensaje', 'Código Promocional eliminado exitosamente');
    }

    public function getCodigo(Request $request, $codigo) {
        $promocion = App\Promocion::where('codigo', $codigo)->where('estado', true)->first();
        return $promocion;
    }
}
