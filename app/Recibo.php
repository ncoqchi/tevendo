<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recibo extends Model
{
    //
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function envio(){

        return $this->hasOne(Envio::class);

    }
}
